package com.mgf.psr.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mgf.psr.constants.UploadImageConstant;
import com.mgf.psr.elementIdentifier.GridActionsIdentifier;
import com.mgf.psr.elementIdentifier.UploadImagePageIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class UploadImagePage extends PageTemplate implements UploadImagePageIdentifier {
	
	/**
	 * This method is used to upload image
	 * @throws AWTException
	 */
	public void uploadImage() throws AWTException {
		MainUtilities.waitFor(5000);
		waitForElementToVisible(By.xpath(GridActionsIdentifier.LOADING_ICON));
		clickOnFirstRwUploadImageIcon();
		uploadImageFromFileExp();
		MainUtilities.waitFor(4000);
	}
	/**
	 * This method is used to check whether the image is uploaded or not.
	 * @return true if the image is uploaded otherwise return false.
	 */
	public boolean isImageUploaded(String successMsgXpath) { 
		MainUtilities.waitFor(4000);
		WebElement successMessage = driver.findElement(By.xpath(successMsgXpath));
		String message = successMessage.getText();
		log.info("message get att : " + successMessage.getAttribute("Value"));
		log.info("message get text : " + message);
		MainUtilities.waitFor(6000);
		if (message.equalsIgnoreCase(UploadImageConstant.SUCCESS_MESSAGE)) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * This method is used to click on First row Upload image icon button.
	 */
	public void clickOnFirstRwUploadImageIcon() {
		MainUtilities.waitFor(4000);
		String firstRwUPLOADIMAGEICON = MainUtilities.generateDynamicXpath(IMAGE_UPLOAD_ICON, "0");
		waitForElementToVisible(By.xpath(firstRwUPLOADIMAGEICON));
		clickOnElement(driver.findElement(By.xpath(firstRwUPLOADIMAGEICON)));
	}
	/**
	 * This method is used to attach image file from file explorer.
	 * @throws AWTException
	 */
	public void uploadImageFromFileExp() throws AWTException {
		String filePathToUpload = UploadImageConstant.BLUE_TSHIRT_PATH;
		StringSelection stringSelection = new StringSelection(filePathToUpload);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	    Robot robot = new Robot();
	    robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
	    robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
	    robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
	    robot.keyPress(java.awt.event.KeyEvent.VK_V);
	    robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
	    robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
	}
}
