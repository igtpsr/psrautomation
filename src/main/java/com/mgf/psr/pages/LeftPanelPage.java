package com.mgf.psr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mgf.psr.elementIdentifier.LeftPanelPageIdentifier;
import com.retelzy.core.selenium.PageTemplate;

public class LeftPanelPage extends PageTemplate implements LeftPanelPageIdentifier {

	private String baseUrl;

	@FindBy(xpath = DASHBOARD_LINK)
	private WebElement dashboardLink;

	@FindBy(xpath = PSR_LINK)
	private WebElement psrLink;

	@FindBy(xpath = EVENT_MODEL_LINK)
	private WebElement eventModelLink;
	
	@FindBy(xpath = EVENT_CODE_LINK)
	private WebElement eventCodeLink;

	/**
	 * This method used to click on 'Dash board' link.
	 */
	public void clickOnDashboardLink() {
		waitForElementToVisible(By.xpath(DASHBOARD_LINK));
		clickOnElement(dashboardLink);
	}

	/**
	 * This method used to click on 'PSR' link.
	 */
	public void clickOnPsrLink() {
		waitForElementToVisible(By.xpath(PSR_LINK));
		clickOnElement(psrLink);
	}

	/**
	 * This method used to click on 'Event Model' link.
	 */
	public void clickOnEventModelLink() {
		waitForElementToVisible(By.xpath(EVENT_MODEL_LINK));
		clickOnElement(eventModelLink);
	}
	
	/**
	 * This method used to click on 'Event Code' link.
	 */
	public void clickOnEventCodeLink() {
	waitForElementToVisible(By.xpath(EVENT_CODE_LINK));
	clickOnElement(eventCodeLink);
	}

}
