package com.mgf.psr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mgf.psr.elementIdentifier.SavedSearchIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class SavedSearchPage extends PageTemplate implements SavedSearchIdentifier{
	
	@FindBy(xpath = SAVE_SEARCH_BUTTON)
	private WebElement saveSearchButton;
	
	@FindBy(xpath = SAVED_SEARCH_BUTTON)
	private WebElement savedSearchButton;

	@FindBy(xpath = REFRESH_BUTTON)
	private WebElement refreshButton;
	
	@FindBy(xpath = SEARCH_FIELDS_DROP_DOWN)
	private WebElement searchFieldsDropDown;
	
	/**
	 * This method is used to click on Search fields drop down.
	 */
	public void clickOnSearchFieldsDropDown() {
		MainUtilities.waitFor(20000);
		waitForElementToVisible(By.xpath(SEARCH_FIELDS_DROP_DOWN));
		clickOnElement(searchFieldsDropDown);
	}
	/**
	 * This method is used to click on Save Search button.
	 */
	public void clickOnSaveSearchButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(SAVE_SEARCH_BUTTON));
		clickOnElement(saveSearchButton);
	}
	/**
	 * This method is used to click on Saved Search button.
	 */
	public void clickOnSavedSearchButton() {
		MainUtilities.waitFor(8000);
		waitForElementToVisible(By.xpath(SAVED_SEARCH_BUTTON));
		clickOnElement(savedSearchButton);
	}
	/**
	 * This method is used to click on Refresh button.
	 */
	public void clickOnRefreshButton() {
		MainUtilities.waitFor(6000);
		waitForElementToVisible(By.xpath(REFRESH_BUTTON));
		clickOnElement(refreshButton);
		MainUtilities.waitFor(6000);
	}
	/**
	 * This method is used to check whether the Search is saved or not.
	 * @param toasterMsg toasterMsg
	 * @return true if the Search is saved otherwise false.
	 */
	public boolean isSearchSaved(String toasterMsg) {
		log.info("toasterMsg : " + toasterMsg);
		if (toasterMsg.equalsIgnoreCase("Success!")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * This method is used to check whether the Saved Search is correct or not.
	 * @param vpo vpo
	 * @param style style
	 * @param vpoInputXpath vpoInputXpath
	 * @param styleInputXpath styleInputXpath
	 * @return true if the saved search is correct otherwise return false.
	 */
	public boolean isSavedSearchCorrect(String vpo, String style ,String vpoInputXpath, String styleInputXpath) { 
		String vpoValue = getText(driver.findElement(By.xpath(vpoInputXpath)));
		String styleValue = getText(driver.findElement(By.xpath(styleInputXpath)));
		if(vpo.equalsIgnoreCase(vpoValue) && style.equalsIgnoreCase(styleValue)) {
			return true;
		} else {
			return false;
		}
	}
	
}