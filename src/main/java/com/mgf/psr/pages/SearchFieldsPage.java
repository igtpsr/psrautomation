package com.mgf.psr.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.mgf.psr.elementIdentifier.GridActionsIdentifier;
import com.mgf.psr.elementIdentifier.SearchFieldsPageIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class SearchFieldsPage extends PageTemplate implements SearchFieldsPageIdentifier {

	@FindBy(xpath = VPO_DROPDOWN)
	private WebElement vpoDropdown;

	@FindBy(xpath = CATEGORY_DROPDOWN)
	private WebElement categoryDropdown;

	@FindBy(xpath = BRAND_DROPDOWN)
	private WebElement brandDropdown;

	@FindBy(xpath = STYLE_DROPDOWN)
	private WebElement styleDropdown;

	@FindBy(xpath = ORDER_COMMIT_DROPDOWN)
	private WebElement orderCommitDropdown;

	@FindBy(xpath = PRODUCTION_OFFICE_DROPDOWN)
	private WebElement productionOfficeDropdown;

	@FindBy(xpath = VPO_INPUT)
	private WebElement vpoInut;

	@FindBy(xpath = CATEGORY_INPUT)
	private WebElement categoryInput;

	@FindBy(xpath = BRAND_INPUT)
	private WebElement brandInput;

	@FindBy(xpath = STYLE_INPUT)
	private WebElement styleInput;

	@FindBy(xpath = ORDER_COMMIT_INPUT)
	private WebElement orderCommitInput;

	@FindBy(xpath = PRODUCTION_OFFICE_INPUT)
	private WebElement productionOfficeInput;

	@FindBy(xpath = NDC_DATE_CALENDAR_ICON)
	private WebElement ndcDateCalendarIcon;

	@FindBy(xpath = SELECT_DATE)
	private WebElement selectDate;

	@FindBy(xpath = SEARCH_BUTTON)
	private WebElement searchBtn;

	@FindBy(xpath = SEARCHED_VPO)
	private List<WebElement> searchedVpo;

	@FindBy(xpath = SEARCHED_STYLE)
	private List<WebElement> searchedStyle;

	@FindBy(xpath = SEARCHED_OC_COMMIT)
	private List<WebElement> searchedOcCommit;

	@FindBy(xpath = SEARCHED_PRODUCTION_OFFICE)
	private List<WebElement> searchedProductionOffice;

	@FindBy(xpath = SELECT_COLUMN_DROPDOWN)
	private WebElement selectColumnDropdown;

	@FindBy(xpath = COLUMN_OPTION)
	private WebElement columnOption;

	/**
	 * This method used to select 'VPO' operator from the drop down.
	 * 
	 * @param operator
	 *            operator
	 */
	public void selectVpoOpeartor(String operator) {
		waitForElementToVisible(By.xpath(VPO_DROPDOWN));
		Select selectOperator = new Select(driver.findElement(By.xpath(VPO_DROPDOWN)));
		clickOnElement(vpoDropdown);
		selectOperator.selectByValue(operator);
	}

	/**
	 * This method is used to enter vpo id.
	 * 
	 * @param vpo
	 *            vpo
	 */
	public void enterVPO(final String vpo) {
		waitForElementToVisible(By.xpath(VPO_INPUT));
		setText(vpoInut, vpo);
	}

	/**
	 * This method used to select 'Category' operator from the drop down.
	 * 
	 * @param operator
	 *            operator
	 */
	public void selectCategoryOpeartor(String operator) {
		waitForElementToVisible(By.xpath(CATEGORY_DROPDOWN));
		Select selectOperator = new Select(driver.findElement(By.xpath(CATEGORY_DROPDOWN)));
		clickOnElement(categoryDropdown);
		selectOperator.selectByValue(operator);
	}

	/**
	 * This method is used to enter category.
	 * 
	 * @param category
	 *            category
	 */
	public void enterCategory(final String category) {
		waitForElementToVisible(By.xpath(CATEGORY_INPUT));
		setText(categoryInput, category);
	}

	/**
	 * This method used to select 'Brand' operator from the drop down.
	 * 
	 * @param operator
	 *            operator
	 */
	public void selectBrandOpeartor(String operator) {
		waitForElementToVisible(By.xpath(BRAND_DROPDOWN));
		Select selectOperator = new Select(driver.findElement(By.xpath(BRAND_DROPDOWN)));
		clickOnElement(brandDropdown);
		selectOperator.selectByValue(operator);
	}

	/**
	 * This method is used to enter brand.
	 * 
	 * @param brand
	 *            brand
	 */
	public void enterBrand(final String brand) {
		waitForElementToVisible(By.xpath(BRAND_INPUT));
		setText(brandInput, brand);
	}

	/**
	 * This method used to select 'Style' operator from the drop down.
	 * 
	 * @param operator
	 *            operator
	 */
	public void selectStyleOpeartor(String operator) {
		waitForElementToVisible(By.xpath(STYLE_DROPDOWN));
		Select selectOperator = new Select(driver.findElement(By.xpath(STYLE_DROPDOWN)));
		clickOnElement(styleDropdown);
		selectOperator.selectByValue(operator);
	}

	/**
	 * This method is used to enter style.
	 * 
	 * @param brand
	 *            brand
	 */
	public void enterStyle(final String brand) {
		waitForElementToVisible(By.xpath(STYLE_INPUT));
		setText(styleInput, brand);
	}

	/**
	 * This method used to select 'Order Commit' operator from the drop down.
	 * 
	 * @param operator
	 *            operator
	 */
	public void selectOrderCommitOpeartor(String operator) {
		waitForElementToVisible(By.xpath(ORDER_COMMIT_DROPDOWN));
		Select selectOperator = new Select(driver.findElement(By.xpath(ORDER_COMMIT_DROPDOWN)));
		clickOnElement(orderCommitDropdown);
		selectOperator.selectByValue(operator);
	}

	/**
	 * This method is used to enter order commit.
	 * 
	 * @param orderCommit
	 *            orderCommit
	 */
	public void enterOrderCommit(final String orderCommit) {
		waitForElementToVisible(By.xpath(ORDER_COMMIT_INPUT));
		setText(orderCommitInput, orderCommit);
	}

	/**
	 * This method used to select 'Production Office' operator from the drop down.
	 * 
	 * @param operator
	 *            operator
	 */
	public void selectProductionOfficeOpeartor(String operator) {
		waitForElementToVisible(By.xpath(PRODUCTION_OFFICE_DROPDOWN));
		Select selectOperator = new Select(driver.findElement(By.xpath(PRODUCTION_OFFICE_DROPDOWN)));
		clickOnElement(productionOfficeDropdown);
		selectOperator.selectByValue(operator);
	}

	/**
	 * This method is used to enter production office.
	 * 
	 * @param productionOffice
	 *            productionOffice
	 */
	public void enterProductionOffice(final String productionOffice) {
		waitForElementToVisible(By.xpath(PRODUCTION_OFFICE_INPUT));
		setText(productionOfficeInput, productionOffice);
	}

	/**
	 * This method used to select Ndc Date.
	 * 
	 * @param date
	 *            date
	 */
	public void selectNdcDate(final String date) {
		waitForElementToVisible(By.xpath(NDC_DATE_CALENDAR_ICON));
		clickOnElement(ndcDateCalendarIcon);
		String ndcDate = MainUtilities.generateDynamicXpath(SELECT_DATE, date);
		waitForElementToVisible(By.xpath(ndcDate));
		clickOnElement(driver.findElement(By.xpath(ndcDate)));
	}

	/**
	 * This method used to click on search button.
	 */
	public void clickOnSearchButton() {
		waitForElementToVisible(By.xpath(SEARCH_BUTTON));
		clickOnElement(searchBtn);
		MainUtilities.waitFor(15000);
		waitForElementToVisible(By.xpath(GridActionsIdentifier.LOADING_ICON));
	}

	/**
	 * This method used to get searched VPO list.
	 * 
	 * @return vpo list
	 */
	public List<String> getSearchedVPO() {
		waitForElementToVisible(By.xpath(SEARCHED_VPO));
		List<String> searchedVpoList = new ArrayList<>();
		for (WebElement element : searchedVpo) {
			if (StringUtils.isNotBlank(getText(element)))
				searchedVpoList.add(getText(element));
		}
		return searchedVpoList;
	}

	/**
	 * This method used to verify searched VPO.
	 * 
	 * @param vpoId
	 *            vpoId
	 * @return true if vpo search successfully else false
	 */
	public boolean isVpoSearched(final String vpoId) {
		boolean isVpoSearched = false;
		final List<String> searchedVpoList = getSearchedVPO();
		for (String searchedVpo : searchedVpoList) {
			if (searchedVpo.equals(vpoId)) {
				isVpoSearched = true;
			} else {
				isVpoSearched = false;
				break;
			}
		}
		return isVpoSearched;
	}

	/**
	 * This method used to verify searched VPO.
	 * 
	 * @param vpoId
	 *            vpoId
	 * @return true if vpo is not search else false
	 */
	public boolean isVpoNotSearched(final String vpoId) {
		boolean isVpoNotSearched = false;
		final List<String> searchedVpoList = getSearchedVPO();
		for (String searchedVpo : searchedVpoList) {
			if (!searchedVpo.equals(vpoId)) {
				isVpoNotSearched = true;
			} else {
				isVpoNotSearched = false;
				break;
			}
		}
		return isVpoNotSearched;
	}

	/**
	 * This method used to verify searched Style.
	 * 
	 * @param styleId
	 *            styleId
	 * @return true if Style search successfully else false
	 */
	public boolean isStyleSearched(final String styleId) {
		boolean isStyleSearched = false;
		final List<String> searchedStyleList = getSearchedVPO();
		for (String searchedStyle : searchedStyleList) {
			if (searchedStyle.equals(styleId)) {
				isStyleSearched = true;
			} else {
				isStyleSearched = false;
				break;
			}
		}
		return isStyleSearched;
	}

	/**
	 * This method used to verify searched Style.
	 * 
	 * @param styleId
	 *            styleId
	 * @return true if style is not search else false
	 */
	public boolean isStyleNotSearched(final String styleId) {
		boolean isStyleNotSearched = false;
		final List<String> searchedStyleList = getSearchedVPO();
		for (String searchedStyle : searchedStyleList) {
			if (!searchedStyle.equals(styleId)) {
				isStyleNotSearched = true;
			} else {
				isStyleNotSearched = false;
				break;
			}
		}
		return isStyleNotSearched;
	}

	/**
	 * This method used to get searched style list.
	 * 
	 * @return style list
	 */
	public List<String> getSearchedStyle() {
		waitForElementToVisible(By.xpath(SEARCHED_STYLE));
		List<String> searchedStyleList = new ArrayList<>();
		for (WebElement element : searchedStyle) {
			if (StringUtils.isNotBlank(getText(element)))
			searchedStyleList.add(getText(element));
		}
		return searchedStyleList;
	}

	/**
	 * This method used to get searched order commit list.
	 * 
	 * @return order commit list
	 */
	public List<String> getSearchedOrderCommit() {
		waitForElementToVisible(By.xpath(SEARCHED_OC_COMMIT));
		List<String> searchedOcCommitList = new ArrayList<>();
		for (WebElement element : searchedOcCommit) {
			if (StringUtils.isNotBlank(getText(element)))
			searchedOcCommitList.add(getText(element));
		}
		return searchedOcCommitList;
	}

	/**
	 * This method used to verify searched order commit #.
	 * 
	 * @param orderCommitId
	 *            orderCommitId
	 * @return true if order commit search successfully else false
	 */
	public boolean isOrderCommitSearched(final String orderCommitId) {
		boolean isOrderCommitSearched = false;
		final List<String> searchedOrderCommitList = getSearchedOrderCommit();
		for (String searchedOrderCommit : searchedOrderCommitList) {
			if (searchedOrderCommit.equals(orderCommitId)) {
				isOrderCommitSearched = true;
			} else {
				isOrderCommitSearched = false;
				break;
			}
		}
		return isOrderCommitSearched;
	}

	/**
	 * This method used to verify searched order commit #.
	 * 
	 * @param orderCommitId
	 *            orderCommitId
	 * @return true if style is not search else false
	 */
	public boolean isOrderCommitNotSearched(final String orderCommitId) {
		boolean isOrderCommitNotSearched = false;
		final List<String> searchedOrderCommitList = getSearchedOrderCommit();
		for (String searchedOrderCommit : searchedOrderCommitList) {
			if (!searchedOrderCommit.equals(orderCommitId)) {
				isOrderCommitNotSearched = true;
			} else {
				isOrderCommitNotSearched = false;
				break;
			}
		}
		return isOrderCommitNotSearched;
	}

	/**
	 * This method used to get searched production office list.
	 * 
	 * @return production office list
	 */
	public List<String> getSearchedProductionOffice() {
		waitForElementToVisible(By.xpath(SEARCHED_PRODUCTION_OFFICE));
		List<String> searchedProdutionOfficeList = new ArrayList<>();
		for (WebElement element : searchedProductionOffice) {
			if (StringUtils.isNotBlank(getText(element)))
			searchedProdutionOfficeList.add(getText(element));
		}
		return searchedProdutionOfficeList;
	}

	/**
	 * This method used to verify searched production office.
	 * 
	 * @param productionOffice
	 *            productionOffice
	 * @return true if production office search successfully else false
	 */
	public boolean isProductionOfficeSearched(final String productionOffice) {
		boolean isProductionOfficeSearched = false;
		final List<String> searchedProductionOfficeList = getSearchedProductionOffice();
		for (String searchedProductionOffice : searchedProductionOfficeList) {
			if (searchedProductionOffice.equals(productionOffice)) {
				isProductionOfficeSearched = true;
			} else {
				isProductionOfficeSearched = false;
				break;
			}
		}
		return isProductionOfficeSearched;
	}

	/**
	 * This method used to verify searched production office.
	 * 
	 * @param productionOffice
	 *            productionOffice
	 * @return true if production Office is not search else false
	 */
	public boolean isProductionOfficeNotSearched(final String productionOffice) {
		boolean isProductionOfficeNotSearched = false;
		final List<String> searchedProductionOfficeList = getSearchedProductionOffice();
		for (String searchedProductionOffice : searchedProductionOfficeList) {
			if (!searchedProductionOffice.equals(productionOffice)) {
				isProductionOfficeNotSearched = true;
			} else {
				isProductionOfficeNotSearched = false;
				break;
			}
		}
		return isProductionOfficeNotSearched;
	}

	/**
	 * This method used to select column.
	 * 
	 * @param columnName
	 *            columnName
	 */
	public void selectColumn(final String columnName) {
		waitForElementToVisible(By.xpath(SELECT_COLUMN_DROPDOWN));
		clickOnElement(selectColumnDropdown);
		final String column = MainUtilities.generateDynamicXpath(COLUMN_OPTION, columnName);
		waitForElementToVisible(By.xpath(column));
		clickOnElement(driver.findElement(By.xpath(column)));
		MainUtilities.waitFor(3000);
	}

	/**
	 * This method used to verify searched brand.
	 * 
	 * @param brand
	 *            brand
	 * @return true if brand search successfully else false
	 */
	public boolean isBrandSearched(final String brand) {
		boolean isVpoSearched = false;
		final List<String> searchedVpoList = getSearchedVPO();
		for (String searchedVpo : searchedVpoList) {
			if (searchedVpo.contains(brand)) {
				isVpoSearched = true;
			} else {
				isVpoSearched = false;
				break;
			}
		}
		return isVpoSearched;
	}

	/**
	 * This method used to verify searched brand.
	 * 
	 * @param brand
	 *            brand
	 * @return true if brand is not search else false
	 */
	public boolean isBrandNotSearched(final String brand) {
		boolean isVpoNotSearched = false;
		final List<String> searchedVpoList = getSearchedVPO();
		for (String searchedVpo : searchedVpoList) {
			if (!searchedVpo.contains(brand)) {
				isVpoNotSearched = true;
			} else {
				isVpoNotSearched = false;
				break;
			}
		}
		return isVpoNotSearched;
	}
	
	/**
	 * This method used to verify searched category.
	 * 
	 * @param category
	 *            category
	 * @return true if category search successfully else false
	 */
	public boolean isCategorySearched(final String category) {
		boolean isVpoSearched = false;
		final List<String> searchedVpoList = getSearchedVPO();
		for (String searchedVpo : searchedVpoList) {
			if (searchedVpo.contains(category)) {
				isVpoSearched = true;
			} else {
				isVpoSearched = false;
				break;
			}
		}
		return isVpoSearched;
	}

	/**
	 * This method used to verify searched category.
	 * 
	 * @param category
	 *            category
	 * @return true if category is not search else false
	 */
	public boolean isCategoryNotSearched(final String category) {
		boolean isVpoNotSearched = false;
		final List<String> searchedVpoList = getSearchedVPO();
		for (String searchedVpo : searchedVpoList) {
			if (!searchedVpo.contains(category)) {
				isVpoNotSearched = true;
			} else {
				isVpoNotSearched = false;
				break;
			}
		}
		return isVpoNotSearched;
	}
}
