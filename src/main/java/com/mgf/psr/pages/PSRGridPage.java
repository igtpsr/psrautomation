package com.mgf.psr.pages;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mgf.psr.elementIdentifier.PSRGridIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class PSRGridPage  extends PageTemplate implements  PSRGridIdentifier {
	
	@FindBy(xpath = SELECT_COLUMN_DROPDOWN)
	private WebElement selectColumnDropDown;
	
	/**
	 * This method is used to get cell values with the help of their Xpaths.
	 * @param cellXpath cellXpath
	 * @return cellValue cellValue
	 */
	public String getCellValue(String cellXpath) {
		MainUtilities.waitFor(4000);
		String cellValue = driver.findElement(By.xpath(cellXpath)).getText(); 
		return cellValue;
	}
	/**
	 * This method used to select column.
	 * 
	 * @param columnName columnName
	 */
	public void selectColumn(final String columnName) {
		waitForElementToVisible(By.xpath(SELECT_COLUMN_DROPDOWN));
		clickOnElement(selectColumnDropDown);
		final String column = MainUtilities.generateDynamicXpath(COLUMN_OPTION, columnName);
		waitForElementToVisible(By.xpath(column));
		clickOnElement(driver.findElement(By.xpath(column)));
		MainUtilities.waitFor(3000);
	}
	/**
	 * This method is used to get list of column value with their names from grid.
	 * @return listOfColumnNamesWithValues listOfColumnNamesWithValues
	 */
	protected LinkedHashMap<String, String> getCoumnNamesAndValuesFromGrid() throws IOException {
		log.info("inside getCoumnNamesAndValuesFromGrid ");
		LinkedHashMap<String, String> listOfColumnNamesWithValues = new LinkedHashMap<String, String>();
		LinkedHashMap<String, String> listOfColumnNamesWithXpaths = new LinkedHashMap<String, String>();
		
		listOfColumnNamesWithXpaths.put("Style #",STYLE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Color Wash Name",COLOR_WASH_NAME_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Order Commit #",ORDER_COMMIT_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Vpo #",VPO_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Ndc Wk",NDC_WK_VALUE_IN_GRID);
		
		listOfColumnNamesWithXpaths.put("Cust Fty Id",CUSTY_FTY_ID_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Fabric Code",FABRIC_CODE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Fiber Content",FIBER_CONTENT_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Fabric Mill",FABRIC_MILL_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Comment",COMMENT_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Tech Pack Received Date",TECH_PACK_REC_DATE_STR_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("# of Knitting Machines",KNITTING_MACHINES_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Garment Treatment Pcs",GARMENT_TREATMENT_PCS_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Packaging Ready Date",PACKAGING_READY_DATE_STR_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Fabric Pp Date",FABRIC_PP_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Cpo Acc Date By Vendor",CPO_ACC_DATE_BY_VENDOR_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Dist Channel",DIST_CHANNEL_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Sample Merch Eta",SAMPLE_MERCH_ETA_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Sample Floor Set Eta",SAMPLE_FLOOR_SET_ETA_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Sample Dcom Eta",SAMPLE_DCOM_ETA_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Sample Mailer Eta",SAMPLE_MAILER_ETA_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Photo/Merchant Sample Send date",PHOTO_MERCHANT_SAMPLE_SEND_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Photo/Merchant Sample ETA date",PHOTO_MERCHANT_SAMPLE_ETA_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Marketing Sample Send date",MARKETING_SAMPLE_SEND_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Marketing Sample ETA date",MARKETING_SAMPLE_ETA_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Marketing Sample Comment",MARKETING_SAMPLE_COMMENT_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Visual Sample Send date",VISUAL_SAMPLE_SEND_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Visual Sample ETA date",VISUAL_SAMPLE_ETA_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Visual Sample Comment",VISUAL_SAMPLE_COMMENT_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Copyright Sample Send date",COPYRIGHT_SAMPLE_SEND_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Copyright Sample ETA date",COPYRIGHT_SAMPLE_ETA_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Additional Bulk Lot Approve",ADDITIONAL_BULK_LOT_APPROVE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Fabric Comment",FABRIC_COMMENT_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("TOP sample ETA Date",TOP_SAMPLE_ETA_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("GAC by Vendor",VENDOR_REVISED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("OC Date / Commit Date",OC_DATE_REVISED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("OC Date / Commit Date",OC_DATE_ACTUAL_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("CPO Date",CPO_DATE_PLANNED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("CPO Date",CPO_DATE_REVISED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("CPO Date",CPO_DATE_ACTUAL_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Size Breakdown Approval",SIZE_BREAKDOWN_APPROVAL_REVISED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Size Breakdown Approval",SIZE_BREAKDOWN_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Trim Approval",TRIM_APPROVAL_REVISED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Trim Approval",TRIM_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Lab Dip/ Strike Off / Knitdown Submit Date",LAB_DIP_SUBMIT_REVISED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Lab Dip/ Strike Off / Knitdown Submit Date",LAB_DIP_SUBMIT_ACTUAL_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("1st Bulk Lot Approval",BULKLOT_APPROVAL_REVISED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("1st Bulk Lot Approval",BULKLOT_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Color Shade Band Approval",COLORSHADE_BAND_APPROVAL_REVISED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Color Shade Band Approval",COLORSHADE_BAND_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Washing End Date",WASHING_ENDDATE_REVISED_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Washing End Date",WASHING_ENDDATE_ACTUAL_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Bulk Fabric Submit Date",BULK_FABRIC_SUBMIT_REVISED_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Bulk Fabric Submit Date",BULK_FABRIC_SUBMIT_ACTUAL_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Fabric / Yarn Ex-Mill Date",FABRICYARN_XMILL_REVISED_DATE_VALUE_IN_GRID);
		listOfColumnNamesWithXpaths.put("Fabric / Yarn Ex-Mill Date",FABRICYARN_XMILL_ACTUAL_DATE_VALUE_IN_GRID);
		
		listOfColumnNamesWithXpaths.entrySet().forEach( entry -> {
			log.info("Column Name : " + entry.getKey());
			selectColumn(entry.getKey());
			MainUtilities.waitFor(2000);
			String colValue = getCellValue(entry.getValue());
			log.info("col value : " + colValue);
			listOfColumnNamesWithValues.put(entry.getKey(), colValue);
		});
		
		return listOfColumnNamesWithValues;
	}
}
