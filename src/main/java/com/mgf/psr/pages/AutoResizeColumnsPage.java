package com.mgf.psr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mgf.psr.elementIdentifier.AutoReSizeColumnsIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class AutoResizeColumnsPage extends PageTemplate implements AutoReSizeColumnsIdentifier {

	@FindBy(xpath = AUTO_RESIZE_COLUMNS_BUTTON)
	private WebElement autoResizeColumnsButton;
	
	/**
	 * This method is used to click on Auto Resize columns button.
	 */
	public void clickOnAutoResizeColumnsButton() {
		MainUtilities.waitFor(12000);
		waitForElementToVisible(By.xpath(AUTO_RESIZE_COLUMNS_BUTTON));
		clickOnElement(autoResizeColumnsButton);
	}
	/**
	 * This method is used to check whether the Auto resize columns is successful or not.
	 * @param toasterMsg toasterMsg
	 * @return true if the toaster message is success.
	 */
	public boolean isAutoResizeColumnsDone(String toasterMsg) {
		log.info("toasterMsg : " + toasterMsg);
		if (toasterMsg.equalsIgnoreCase("Success!")) {
			return true;
		} else {
			return false;
		}
	}	
}
