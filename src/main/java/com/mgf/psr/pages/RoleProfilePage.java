package com.mgf.psr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.mgf.psr.constants.OperatorConstant;
import com.mgf.psr.elementIdentifier.RoleProfileIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class RoleProfilePage extends PageTemplate implements RoleProfileIdentifier {

	@FindBy(xpath = DASHBOARD_LINK)
	private WebElement dashboardLink;
	
	@FindBy(xpath = ROLE_PROFILE_LINK)
	private WebElement roleProfileLink;
	
	@FindBy(xpath = NEW_BUTTON_IN_ROLEPROFILE)
	private WebElement newButtonInRoleProfile;
	
	@FindBy(xpath = ROLE_NAME_INPUT)
	private WebElement roleNameInput;
	
	@FindBy(xpath = ROLE_DESC_INPUT)
	private WebElement roleDescInput;
	
	@FindBy(xpath = ROLE_APP_INPUT_SELECTION)
	private WebElement roleAppInputSelection;
	
	@FindBy(xpath = ROLE_PROFILE_SAVE_BUTTON)
	private WebElement roleProfileSaveButton;
	
	@FindBy(xpath = UPDATE_ROLE_BUTTON_APP)
	private WebElement updateRoleButtonApp;
	
	@FindBy(xpath = UPDATE_ROLE_BUTTON)
	private WebElement updateRoleButton;
	
	@FindBy(xpath = ROLE_NAME_SEARCH_FIELD_INPUT)
	private WebElement roleNameSearchFieldInput;
	
	@FindBy(xpath = ROLE_SEARCH_BUTTON)
	private WebElement roleSearchButton;
	
	@FindBy(xpath = ROLE_DETAILS_ID)
	private WebElement roleDetailsID;
	
	@FindBy(xpath = ROLENAME_SELECT_OPERATOR)
	private WebElement roleNameSelectOperator;
	
	@FindBy(xpath = DELETE_ROLE_BUTTON)
	private WebElement deleteRoleButton;
	
	@FindBy(xpath = DISABLE_ROLE)
	private WebElement disableRole;
	 
	@FindBy(xpath = ROLE_NO_DATA_TO_DISPLAY)
	private WebElement roleNoDataToDisplay;
	
	private String updated_Role_Desc = "";
	/**
	 * This method used to click on 'DASHBOARD_LINK'.
	 */
	public void clickOnDashboardLink() {
		MainUtilities.waitFor(10000);
		waitForElementToVisible(By.xpath(DASHBOARD_LINK));
		clickOnObjectWithJavaScript(dashboardLink);
	}
	/**
	 * This method used to click on 'ROLE_PROFILE_LINK'.
	 */
	public void clickOnRoleProfileLink() {
		MainUtilities.waitFor(10000);
		waitForElementToVisible(By.xpath(ROLE_PROFILE_LINK));
		clickOnElement(roleProfileLink);
	}
	/**
	 * This method used to click on 'NEW_BUTTON_IN_ROLEPOFILE' button.
	 */
	public void clickOnNewUserProfileLink() {
		waitForElementToVisible(By.xpath(NEW_BUTTON_IN_ROLEPROFILE));
		clickOnElement(newButtonInRoleProfile);
	}
	/**
	 * This method is used to enter value into role input field.
	 * @param role role
	 */
	public void enterRole(String role) {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(ROLE_NAME_INPUT));
		setText(roleNameInput,role);
	}
	/**
	 * This method is used to enter value into role description field.
	 * @param roleDesc roleDesc
	 */
	public void enterRoleDesc(String roleDesc) {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(ROLE_NAME_INPUT));
		setText(roleDescInput,roleDesc);
	}
	/**
	 * This method is used to select App input.
	 * @param app app
	 */
	public void selectApp(final String app) {
		waitForElementToVisible(By.xpath(ROLE_APP_INPUT_SELECTION));
		MainUtilities.waitFor(2000);
		clickOnObjectWithJavaScript(roleAppInputSelection);
		String selectRole = MainUtilities.generateDynamicXpath(ROLE_APP_INPUT_VALUE, app);
		waitForElementToVisible(By.xpath(selectRole));
		clickOnObjectWithJavaScript(driver.findElement(By.xpath(selectRole)));
	}
	/**
	 * This method is used to click on role profile save button.
	 */
	public void clickonSaveRoleProfileButton() {
		MainUtilities.waitFor(8000);
		waitForElementToVisible(By.xpath(ROLE_PROFILE_SAVE_BUTTON));
		clickOnElement(roleProfileSaveButton);
		MainUtilities.waitFor(8000);
	}
	/**
	 * This method is used to create new role profile.
	 * @param role role
	 * @param roleDesc roleDesc
	 * @param app app
	 */
	public void createRoleProfile(String role, String roleDesc, String app) {
		MainUtilities.waitFor(4000);
		clickOnNewUserProfileLink();
		MainUtilities .waitFor(4000);
		enterRole(role);
		enterRoleDesc(roleDesc);
		selectApp(app);
		clickonSaveRoleProfileButton();
	}
	/**
	 * This method is used to select the Role Name operator.
	 * @param Operator Operator
	 */
	public void selectRoleNameOpeartor(String Operator) {
		waitForElementToVisible(By.xpath(ROLENAME_SELECT_OPERATOR));
		Select operator = new Select(driver.findElement(By.xpath(ROLENAME_SELECT_OPERATOR)));
		clickOnElement(roleNameSelectOperator);
		operator.selectByValue(Operator); 
	}
	/**
	 * This method is used to enter the role name in Role Name search field.
	 * @param role role
	 */
	public void enterSearchRoleName(String role) {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(ROLE_NAME_SEARCH_FIELD_INPUT));
		setText(roleNameSearchFieldInput, role);
	}
	/**
	 * This method is used to click on Search role button.
	 */
	public void clickOnRoleSearchButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(ROLE_SEARCH_BUTTON));
		clickOnElement(roleSearchButton);
		MainUtilities.waitFor(4000);
	}
	/**
	 * This method is used to role in the grid
	 * @param role role
	 */
	public void selectRoleNameInGrid(String role) {
		waitForElementToVisible(By.xpath(ROLE_DETAILS_ID));		
		String jqxid = getTextByAttribute(roleDetailsID, "id");
		String roleDetailsIdInGrid = MainUtilities.generateDynamicXpath(SELECT_ROLE, jqxid);
		log.info("User details id: " + roleDetailsIdInGrid);
		waitForElementToVisible(By.xpath(roleDetailsIdInGrid));
		clickOnElement(driver.findElement(By.xpath(roleDetailsIdInGrid)));
		MainUtilities.waitFor(4000);
	}
	/**
	 * This method is used to click on App Role Update button.
	 */
	public void clickOnAppRoleUpdateButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(UPDATE_ROLE_BUTTON_APP));
		clickOnElement(updateRoleButtonApp);
	}
	/**
	 * This method is used to click on Role Update Button.
	 */
	public void clickOnRoleUpdateButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(UPDATE_ROLE_BUTTON));
		clickOnElement(updateRoleButton);
	}
	/**
	 * This method is used to Update the user profile.
	 * @param role role
	 */
	public void updateRoleProfile(String role) {
		MainUtilities.waitFor(5000);
		selectRoleNameOpeartor(OperatorConstant.EQUAL_TO);
		enterSearchRoleName(role);
		clickOnRoleSearchButton();
		selectRoleNameInGrid(role);
		clickOnAppRoleUpdateButton();
		updated_Role_Desc = MainUtilities.generateRandomString(8);
		enterRoleDesc(updated_Role_Desc);
		MainUtilities.waitFor(4000);
		clickOnRoleUpdateButton();
	}
	/**
	 * This method is used to click on Role Delete Button.
	 */
	public void clickOnDeleteButton() {
		MainUtilities.waitFor(4000);
		waitForElementToVisible(By.xpath(DELETE_ROLE_BUTTON));
		clickOnElement(deleteRoleButton);
		MainUtilities.waitFor(6000);
	}
	/**
	 * This method is used to delete the role profile.
	 * @param role role
	 */
	public void deleteRoleProfile(String role) {
		MainUtilities.waitFor(4000);
		selectRoleNameOpeartor(OperatorConstant.EQUAL_TO);
		enterSearchRoleName(role);
		clickOnRoleSearchButton();
		selectRoleNameInGrid(role);
		clickOnDeleteButton();
	}
	/**
	 * This method is used to click on Disable role check box.
	 */
	public void clickOnDisableRoleCheckBox() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(DISABLE_ROLE));
		clickOnElement(disableRole);
	}
	/**
	 * This method is used to Disable the role profile.
	 * @param role role
	 */
	public void disableRoleProfile(String role) {
		MainUtilities.waitFor(4000);
		selectRoleNameOpeartor(OperatorConstant.EQUAL_TO);
		enterSearchRoleName(role);
		clickOnRoleSearchButton();
		selectRoleNameInGrid(role);
		clickOnAppRoleUpdateButton();
		clickOnDisableRoleCheckBox();
		clickOnRoleUpdateButton();
	}
	/**
	 * This method is used to verify whether the Role is created or not.
	 * @param roleName roleName
	 * @return true if role is Created else false
	 */
	public boolean isRoleCreated(String roleName) {
		MainUtilities.waitFor(1000);
		selectRoleNameOpeartor(OperatorConstant.EQUAL_TO);
		enterSearchRoleName(roleName);
		MainUtilities.waitFor(1000);
		clickOnRoleSearchButton();
		MainUtilities.waitFor(1000);
		String actualModelName = MainUtilities.generateDynamicXpath(SELECT_ROLE, roleName);
		waitForElementToVisible(By.xpath(actualModelName));

		if (isElementDisplayed(driver.findElement(By.xpath(actualModelName)))) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * This method is used to verify whether the Role is updated or not.
	 * @param roleName roleName
	 * @return true if role is Updated else false
	 */
	public boolean isRoleUpdated(String roleName) {
		selectRoleNameOpeartor(OperatorConstant.EQUAL_TO);
		enterSearchRoleName(roleName);
		MainUtilities.waitFor(1000);
		clickOnRoleSearchButton();
		MainUtilities.waitFor(1000);
		String actualModelName = MainUtilities.generateDynamicXpath(SELECT_ROLE, roleName);
		waitForElementToVisible(By.xpath(actualModelName));
		selectRoleNameInGrid(roleName);
		clickOnAppRoleUpdateButton();
		MainUtilities.waitFor(4000);
		
		waitForElementToVisible(By.xpath(ROLE_DESC_INPUT));
		String roleDesc = roleDescInput.getAttribute("value");
		log.info("text in Role Desc field : " + roleDesc);
		log.info("Updated Role Desc value : " + updated_Role_Desc);
		if (roleDesc.equalsIgnoreCase(updated_Role_Desc)) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * This method is used to verify whether the Role is deleted or not.
	 * @param roleName roleName
	 * @return true if role is Deleted else false
	 */
	public boolean isRoleDeleted(String roleName) {
		selectRoleNameOpeartor(OperatorConstant.EQUAL_TO);
		enterSearchRoleName(roleName);
		MainUtilities.waitFor(1000);
		clickOnRoleSearchButton();
		MainUtilities.waitFor(1000);
		waitForElementToVisible(By.xpath(ROLE_NO_DATA_TO_DISPLAY));
		if (isElementDisplayed(roleNoDataToDisplay)) {
			return true;
		} else {
			return false;
		}
	}	
}
