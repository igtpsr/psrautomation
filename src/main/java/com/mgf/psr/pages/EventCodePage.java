package com.mgf.psr.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.mgf.psr.constants.OperatorConstant;
import com.mgf.psr.elementIdentifier.EventCodePageIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class EventCodePage extends PageTemplate implements EventCodePageIdentifier{
	
	@FindBy(xpath = EVENT_CODE_NAME_TXTFIELD)
	private WebElement eventCodeNameTxtField;
	
	@FindBy(xpath = NEW_EVENT_CODE_TXTFIELD)
	private WebElement newEventCodeTxtField;
	
	@FindBy(xpath = NEW_EVENT_CODE_DESCRIPTION)
	private WebElement newEventCodeDescription;
	
	@FindBy(xpath = NEW_EVENT_CODE_CATEGORY)
	private WebElement newEventCodeCategory;
	
	@FindBy(xpath = EVENT_CODE_NEW_BTN)
	private WebElement eventCodeNewBtn;
	
	@FindBy(xpath = EVENT_CODE_SAVE)
	private WebElement eventCodeSaveBtn;
	
	@FindBy(xpath = EVENT_CODE_SEARCH_INPUT)
	private WebElement searchCode;
	
	@FindBy(xpath = EVENT_CODE_SEARCH_BTN)
	private WebElement searchBtn;
	
	@FindBy(xpath = SELECT_OPERATOR)
	private WebElement selectOperator;
	
	@FindBy(xpath = CREATED_EVENT_CODES)
	private WebElement createdEvntCode;
	
	@FindBy(xpath = DELETE_EVENT_CODE)
	private WebElement deleteEventCode;
	
	@FindBy(xpath = UPDATE_CODE_DESCRIPTION_JQXID)
	private WebElement updateCodeDescriptionJqxid;
	
	@FindBy(xpath = TOASTER_MSG)
	private WebElement toasterMsg; 
	
	/**
	 * This method used to select 'operators' from the drop down.
	 * 
	 * @param operator 
	 *            operator
	 */
	public void SelectOpeartor(String operator) {
		waitForElementToVisible(By.xpath(SELECT_OPERATOR));
		Select select_operator = new Select(driver.findElement(By.xpath(SELECT_OPERATOR)));
		clickOnElement(selectOperator);
		select_operator.selectByValue(operator); 

	}
	
	/**
	 * This method used to click on 'New' Button.
	 */
	public void clickOnNewBtn() {
		waitForElementToVisible(By.xpath(EVENT_CODE_NEW_BTN));
		clickOnElement(eventCodeNewBtn);
	}
	
	/**
	 * This method used to click on 'Delete' Button.
	 */
	public void clickOnEventCodeDeleteBtn() {
		MainUtilities.waitFor(5000);
		waitForElementToVisible(By.xpath(DELETE_EVENT_CODE));
		clickOnElement(deleteEventCode);
	}
	
   /**
    * This method is used to handle Delete Pop Up Alert
    * @return 
    */
	public void confirmDelete() {
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
	}
	
	/**
	 * This method used to read 'Toaster' Message.
	 */
	public String getoasterMsg() {
		waitForElementToVisible(By.xpath(TOASTER_MSG));
		return getText(toasterMsg);
	}
	
	/**
	 * This method is used to enter 'Event Code Name' in Code name text field
	 * 
	 * @param eventCodeName 
	 *             eventCodeName
	 */
	public void enterCodeName(final String eventCodeName) {
		waitForElementToVisible(By.xpath(EVENT_CODE_NAME_TXTFIELD));
		setText(eventCodeNameTxtField, eventCodeName);
	}

	/**
	 * This method is used to enter 'Event Code Description' in description text field
	 * 
	 * @param enterEventCodeDescription 
                    enterEventCodeDescription
	 */
	public void enterEventCodeDescription(final String eventCodeDescription) {
		waitForElementToVisible(By.xpath(NEW_EVENT_CODE_DESCRIPTION));
		setText(newEventCodeDescription, eventCodeDescription);		
	}
		
	/**
	 * This method is used to enter 'Event Code Category' in category text field
	 * 
	 * @param enterEventCodeCategory 
	 *              enterEventCodeCategory
	 */
	public void enterEventCodeCategory(final String eventCodeCategory) {
	    waitForElementToVisible(By.xpath(NEW_EVENT_CODE_CATEGORY));
		setText(newEventCodeCategory, eventCodeCategory);
	}

	/**
	 * This method used to click on 'Save' Button.
	 */
	public void clickOnSave() {
		waitForElementToVisible(By.xpath(EVENT_CODE_SAVE));
		clickOnElement(eventCodeSaveBtn);
	}
	
	/**
	 * This method is used to search 'Event Code Name'
	 * 
	 * @param eventCodeName 
	 *            eventCodeName
	 */
	public void searchEventCode(final String eventCodeName) {
		waitForElementToVisible(By.xpath(EVENT_CODE_SEARCH_INPUT));
		setText(searchCode, eventCodeName);
	}
	
	/**
	 * This method used to click on ' event code search Button'.
	 */
	public void clickEventCodeSearchBtn() {
		waitForElementToVisible(By.xpath(EVENT_CODE_SEARCH_BTN));
		clickOnElement(searchBtn);
	}
	
	/**
	 * This method used to verify whether ' event code is created'.
	 * 
	 * @param eventCodeName 
	 *            eventCodeName
	 */
	public boolean isEventCodeCreated(String eventCodeName) {
		SelectOpeartor(OperatorConstant.EQUAL_TO);
		searchEventCode(eventCodeName);
		MainUtilities.waitFor(5000);
		clickEventCodeSearchBtn();
		MainUtilities.waitFor(5000);
		String actualEventCodeName = MainUtilities.generateDynamicXpath(CREATED_EVENT_CODES, eventCodeName);
		waitForElementToVisible(By.xpath(actualEventCodeName));
		if (isElementDisplayed(driver.findElement(By.xpath(actualEventCodeName)))) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method used to click on ' created event code'.
	 */
	public void clickOnCreatedEventCode(String eventCode) {
		String updatedEventCodeName = MainUtilities.generateDynamicXpath(CREATED_EVENT_CODES, eventCode);
		waitForElementToVisible(By.xpath(updatedEventCodeName));
		clickOnElement(driver.findElement(By.xpath(updatedEventCodeName)));
	}
	
	/**
	 * This method used to verify whether ' event code is updated or not'.
	 * 
	 * @param CodeName  
	 *            CodeName
	 */
	public boolean isEventCodeUpdated(String CodeName, String eventCodeDescription) {
		SelectOpeartor(OperatorConstant.EQUAL_TO);
		searchEventCode(CodeName);
		MainUtilities.waitFor(5000);
		clickEventCodeSearchBtn();
		MainUtilities.waitFor(50000);
		String updatedEventCodeDescription = MainUtilities.generateDynamicXpath(UPDATE_CODE_DESCRIPTION_JQXID, eventCodeDescription);
		waitForElementToVisible(By.xpath(updatedEventCodeDescription));

		if (isElementDisplayed(driver.findElement(By.xpath(updatedEventCodeDescription)))) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method used to verify whether ' event code is deleted or not'.
	 * 
	 * @param eventCodeName 
	 *            eventCodeName
	 */
	public boolean isEventCodeDeleted(String eventCodeName) {
		SelectOpeartor(OperatorConstant.EQUAL_TO);
		searchEventCode(eventCodeName);
		clickEventCodeSearchBtn();
		MainUtilities.waitFor(1000);
		String actualEventCodeName = MainUtilities.generateDynamicXpath(CREATED_EVENT_CODES);
		waitForElementToVisible(By.xpath(actualEventCodeName));

		if (isElementDisplayed(driver.findElement(By.xpath(actualEventCodeName)))) {
			return false;
		} else {
			return true;
		}
	}
}
