package com.mgf.psr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mgf.psr.elementIdentifier.ShowHideColumnViewIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class ShowHideColumnViewPage extends PageTemplate implements ShowHideColumnViewIdentifier {

	@FindBy(xpath = SHOW_HIDE_COLUMN_BUTTON)
	private WebElement showHideColumnButton;
	
	@FindBy(xpath = LOOKING_FOR_COLUMN_INPUT)
	private WebElement lookingForColumnWebElementInput;
	
	@FindBy(xpath = VIEW_NAME_INPUT)
	private WebElement viewNameWebElementInput;
	
	@FindBy(xpath = SAVE_AS_VIEW_BUTTON)
	private WebElement viewSaveAsButton;
	
	@FindBy(xpath = UPDATE_VIEW_BUTTON)
	private WebElement viewUpdateButton;
	
	@FindBy(xpath = DELETE_VIEW_BUTTON)
	private WebElement viewDeleteButton;
	
	@FindBy(xpath = AVAILABLE_VIEW_DROP_DOWN)
	private WebElement availableViewDropDown;
	
	/**
	 * This method is used to click on Show Hide Column button.
	 */
	public void clickOnShowHideColumnButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(SHOW_HIDE_COLUMN_BUTTON));
		clickOnElement(showHideColumnButton);		
	}
	/**
	 * This method is used to select grid view name in available view drop down.
	 * @param viewName viewNameInp
	 */
	public void selectGridView(final String viewName) {
		MainUtilities.waitFor(4000);
		waitForElementToVisible(By.xpath(AVAILABLE_VIEW_DROP_DOWN));
		clickOnElement(availableViewDropDown);
		final String viewNameXpath = MainUtilities.generateDynamicXpath(VIEW_NAME_IN_DROP_DOWN, viewName);
		waitForElementToVisible(By.xpath(viewNameXpath));
		clickOnElement(driver.findElement(By.xpath(viewNameXpath)));
		MainUtilities.waitFor(3000);
	}
	/**
	 * This method is used to check whether the grid view is present or not.
	 * @param viewName viewNameInp
	 * @return true if the grid view is not present otherwise false.
	 */
	public boolean isGridViewPresent(String viewName) {
		MainUtilities.waitFor(4000);
		waitForElementToVisible(By.xpath(AVAILABLE_VIEW_DROP_DOWN));
		clickOnElement(availableViewDropDown);
		final String viewNameXpath = MainUtilities.generateDynamicXpath(VIEW_NAME_IN_DROP_DOWN, viewName);
		waitForElementToVisible(By.xpath(viewNameXpath));
		try {
			if(isElementDisplayed(driver.findElement(By.xpath(viewNameXpath)))) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	/**
	 * This method is used to enter column name in Looking for input box.
	 * @param columnName columnName
	 */
	public void enterColumnName(String columnName) {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(LOOKING_FOR_COLUMN_INPUT));	
		setText(lookingForColumnWebElementInput, columnName);
	}
	/**
	 * This method is used to select column name check box.
	 * @param columnNameToSelect columnNameToSelect
	 */
	public void selectColumnNameCheckBox(String columnNameToSelect) {
		final String columnNameXpath = MainUtilities.generateDynamicXpath(COLUMN_NAMES_LIST_IN_VIEW_FORM, columnNameToSelect);
		waitForElementToVisible(By.xpath(columnNameXpath));
		clickOnElement(driver.findElement(By.xpath(columnNameXpath)));
	}
	/**
	 * This method is used to enter View name in View name input text box.
	 * @param viewName viewName
	 */
	public void enterViewName(String viewName) {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(VIEW_NAME_INPUT));
		setText(viewNameWebElementInput, viewName);
	}
	/**
	 * This method is used to click on View Save As button.
	 */
	public void clickOnViewSaveAsButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(SAVE_AS_VIEW_BUTTON));
		clickOnElement(viewSaveAsButton);		
	}
	/**
	 * This method is used to click on View Update button.
	 */
	public void clickOnViewUpdateButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(UPDATE_VIEW_BUTTON));
		clickOnElement(viewUpdateButton);		
	}
	/**
	 * This method is used to click on View Delete button.
	 */
	public void clickOnViewDeleteButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(DELETE_VIEW_BUTTON));
		clickOnElement(viewDeleteButton);		
	}
	/**
	 * This method is used to check the toaster message is success or not.
	 * @param toasterMsg toasterMsg
	 * @return true if the toaster message is success otherwise false.
	 */
	public boolean checkSuccessMsg(String toasterMsg) {
		if (toasterMsg.equalsIgnoreCase("Success!")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * This method is used to save New Grid View.
	 * @param newViewName newViewName
	 * @param columnNameToHide columnNameToHide
	 */
	public void saveNewView(String newViewName, String columnNameToHide) {
		MainUtilities.waitFor(4000);
		clickOnShowHideColumnButton();
		MainUtilities.waitFor(4000);
		enterColumnName(columnNameToHide);
		MainUtilities.waitFor(4000);
		selectColumnNameCheckBox(columnNameToHide);
		MainUtilities.waitFor(4000);
		enterViewName(newViewName);
		MainUtilities.waitFor(4000);
		clickOnViewSaveAsButton();
	}
	/**
	 * This method is used to check whether the New grid view is saved or not.
	 * @param toasterMsg toasterMsg
	 * @return true if the new grid view is saved otherwise false.
	 */
	public boolean isGridViewSaved(String toasterMsg) {
		return checkSuccessMsg(toasterMsg);
	}
	/**
	 * This method is used to update the existing grid view.
	 * @param columnNameToHide columnNameToHide
	 * @param viewNameToUpdate viewNameToUpdate
	 */
	public void updateView(String columnNameToHide, String viewNameToUpdate) {
		MainUtilities.waitFor(4000);
		clickOnShowHideColumnButton();
		MainUtilities.waitFor(4000);
		MainUtilities.waitFor(4000);
		enterColumnName(columnNameToHide);
		MainUtilities.waitFor(4000);
		selectColumnNameCheckBox(columnNameToHide);
		MainUtilities.waitFor(4000);
		clickOnViewUpdateButton();
		MainUtilities.waitFor(4000);
	}
	/**
	 * This method is used to check whether the grid view is updated or not.
	 * @param toasterMsg toasterMsg
	 * @return true if the grid is updated successfully otherwise false.
	 */
	public boolean isGirdViewUpdated(String toasterMsg) {
		return checkSuccessMsg(toasterMsg);
	}
	/**
	 * This method is used to delete the existing grid view.
	 * @param viewNameToDelete viewNameToDelete
	 */
	public void deleteGridView() {
		MainUtilities.waitFor(4000);
		clickOnShowHideColumnButton();
		MainUtilities.waitFor(4000);
		clickOnViewDeleteButton();
		MainUtilities.waitFor(4000);
	}
	/**
	 * This method is used to check whether the grid view is deleted or not.
	 * @param toasterMsg toasterMsg
	 * @return true if the grid view is deleted otherwise false.
	 */
	public boolean isGridViewDeleted(String viewName) {
		MainUtilities.waitFor(4000);
		return isGridViewPresent(viewName);
	}
}