package com.mgf.psr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mgf.psr.constants.ApplicationConstant;
import com.mgf.psr.elementIdentifier.ChangeTrakPageIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class ChangeTrackPage extends PageTemplate implements ChangeTrakPageIdentifier {
	
	@FindBy(xpath = CHANGE_TRACK_BUTTON)
	private WebElement changeTrackButton;
	
	@FindBy(xpath = SINCE_DROP_DOWN)
	private WebElement sinceDropDown;
	
	@FindBy(xpath = SEARCH_BUTTON_IN_CHANGE_TRACK)
	private WebElement searchButtonInChangeTrack;
	
	@FindBy(xpath = CHANGE_TRACK_DETAILS)
	private WebElement changeTrackDetails;
	
	/**
	 * This method is used to click on Change Track button.
	 */
	public void clickOnChangeTrackButton() {
		MainUtilities.waitFor(8000);
		waitForElementToVisible(By.xpath(CHANGE_TRACK_BUTTON));
		clickOnElement(changeTrackButton);
	}
	/**
	 * This method is used to select value in Since drop down.
	 * @param sinceValue sinceValue
	 */
	public void selectSinceDropDown(String sinceValue) {
		MainUtilities.waitFor(12000);
		waitForElementToVisible(By.xpath(SINCE_DROP_DOWN));
		clickOnElement(sinceDropDown);
		final String sinceValueXpath = MainUtilities.generateDynamicXpath(SINCE_VALUE_IN_DROP_DOWN, sinceValue);
		waitForElementToVisible(By.xpath(sinceValueXpath));
		clickOnElement(driver.findElement(By.xpath(sinceValueXpath)));
		MainUtilities.waitFor(4000);
	}	
	/**
	 * This method is used to click on search button in Change Track form.
	 */
	public void clickOnSearchButtonInChangeTrack() {
		MainUtilities.waitFor(12000);
		waitForElementToVisible(By.xpath(SEARCH_BUTTON_IN_CHANGE_TRACK));
		clickOnElement(searchButtonInChangeTrack);
		MainUtilities.waitFor(4000);
	}
	/**
	 * This method is used to get change track details.
	 * @return changeTrackDetails changeTrackDetails
	 */
	public String getChangeTrackDetails() {
		waitForElementToVisible(By.xpath(CHANGE_TRACK_DETAILS));
	    return getText(changeTrackDetails);
	}
	/**
	 * This method is used to navigate to Change Track details.
	 */
	public void navigateChangeTrack() {
		clickOnChangeTrackButton();
		selectSinceDropDown(ApplicationConstant.TODAY);
		clickOnSearchButtonInChangeTrack();
	}
	/**
	 * This method is used to check whether the Change track is updated or not.
	 * @param columnName columnName
	 * @param updateColumnValue newValue
	 * @return true the change track is update correctly otherwise false.
	 */
	public boolean isChangeTrackUpdated(String columnName, String updateColumnValue) {
		String changeTrackDetails = getChangeTrackDetails();
		Boolean columnNameMatched = changeTrackDetails.contains(columnName);
		Boolean updatedValueMatched = changeTrackDetails.contains(updateColumnValue);
		if(columnNameMatched && updatedValueMatched) {
			return true;
		} else {
			return false;
		}
	}
}
