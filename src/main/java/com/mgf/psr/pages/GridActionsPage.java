package com.mgf.psr.pages;

import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.sikuli.script.FindFailed;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import com.mgf.psr.elementIdentifier.GridActionsIdentifier;
import com.mgf.psr.elementIdentifier.PSRGridIdentifier;
import com.mgf.psr.elementIdentifier.SearchFieldsPageIdentifier;
import com.mgf.psr.utilities.Utilities;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;
import org.apache.commons.io.FileUtils;

public class GridActionsPage extends PSRGridPage implements  GridActionsIdentifier{
	
	@FindBy(xpath = SearchFieldsPageIdentifier.VPO_INPUT)
	private WebElement vpoNumberInput;
	
	@FindBy(xpath = SearchFieldsPageIdentifier.SEARCH_BUTTON)
	private WebElement gridSearchButton;
	
	@FindBy(xpath = GET_VIEW_NAME)
	private WebElement getViewName;
	
	@FindBy(xpath = EXCEL_OPERATION)
	private WebElement excelOperation;
	
	@FindBy(xpath = EXPORT_EXCEL_BUTTON)
	private WebElement exportExcel;
	
	@FindBy(xpath = IMPORT_EXCEL_BUTTON)
	private WebElement importExcel;
	
	@FindBy(xpath = FILE_TRACK)
	private WebElement fileTrack;
	
	@FindBy(xpath = FILE_TRACK_VISIBILITY)
	private WebElement fileTrackVisibility;
	
	@FindBy(xpath = FILE_NAME_EXPAND)
	private WebElement fileNameExpand;

	@FindBy(xpath = FILE_TRACK_USER_NAME)
	private WebElement fileTrackUserName;
	
	@FindBy(xpath = IMPORT_CHOOSE_FILE_BUTTON)
	private WebElement imprtChooseFileButton;
	
	@FindBy(xpath = IMPORT_UPLOAD_EXCEL_BUTTON)
	private WebElement importUploadExcelButton;
	
	@FindBy(xpath = GridActionsIdentifier.SAVE_CHANGES_BUTTON)
	private WebElement saveChangesButton;
	
	@FindBy(xpath = PSRGridIdentifier.NDC_WK_VALUE_IN_GRID)
	private WebElement ndcWkValueInGrid;
	
	@FindBy(xpath = PSRGridIdentifier.NDC_WK_VALUE_INPUT_IN_GRID)
	private WebElement ndcWkValueInputInGrid;
	
	@FindBy(xpath = ACKNOWLEDGEMENT_BUTTON)
	private WebElement acknowledgementButton;
	
	@FindBy(xpath = SUCCESS_MESSAGE)
	private WebElement successMessage;
	
	@FindBy(xpath = PSRGridIdentifier.SELECT_CHECK_BOX)
	private WebElement selectCheckBox;
	
	private String downloadsFolderPath = System.getProperty("user.dir") + File.separator + "downloads" + File.separator;

	private String excelOperationFileName = "PSR_2021-8-11_19_3_29_683.xlsm";
	
	private String newNdcWkValue = "test";
	
	/**
	 * This method is used to do Export excel operation.
	 * @throws IOException
	 */
	public void exportExcel() throws IOException {
		String viewName = getViewName();
		deleteAllFilesinThisFolder(downloadsFolderPath);
		MainUtilities.waitFor(4000);
		clickOnExcelOperation();
		clickOnExportExcel();
	}
	/**
	 * This method is used to check whether the excel is Exported or not.
	 * @throws IOException 
	 * @throws FindFailed 
	 */
	public boolean isExcelExported() throws IOException, FindFailed {
		log.info("inside isExcelExported ");
		LinkedHashMap<String, String> gridData = getGridData();
		MainUtilities.waitFor(10000);
		log.info("after getting grid data");
		File folder = new File(downloadsFolderPath);
		File[] listOfFiles = folder.listFiles();
		String exportedFileName = listOfFiles[0].getName();
		excelOperationFileName = exportedFileName;
		String sheetName = exportedFileName.replace(".xlsm", "");
		
		log.info("downloadsFolderPath : " + downloadsFolderPath);
		log.info("exportedFileName : " + exportedFileName);
		log.info("sheetName : " + sheetName);
		
		//********************************************
		Desktop.getDesktop().open(new File(downloadsFolderPath + exportedFileName));
		MainUtilities.waitFor(60000);
		log.info("After opening excel file");
		Screen S = new Screen();
		
		Pattern content1 = new Pattern(System.getProperty("user.dir") + File.separator + "Sikuli_Images" + File.separator + "Enable_Editing.png");
		log.info("After clicking on Enable Editing button in excel file");
		S.wait(content1);
		S.click(content1);
		
		Pattern content2 = new Pattern(System.getProperty("user.dir") + File.separator + "Sikuli_Images" + File.separator + "Enable_Content.png");
		log.info("After clicking on Enable content button in excel file");
		S.wait(content2);
		S.click(content2);
		//********************************************
		
		log.info("Before reading excel file");
		LinkedHashMap<String, String> excelData = Utilities.readExcel(downloadsFolderPath, exportedFileName, sheetName);
		log.info("After reading excel file");
		
		log.info("before comparing excel data and grid data");
		boolean isExcelExported = compareGridandExcelData(excelData, gridData);
		log.info("After comparing excel and grid data");
		return isExcelExported;
	}
	/**
	 * This method is used to do Import excel operation.
	 * @throws AWTException 
	 * @throws FindFailed 
	 */
	public void importExcel() throws IOException, AWTException, FindFailed {
		exportExcel();
		MainUtilities.waitFor(2000);
		log.info("After export excel");
		log.info("downloadsFolderPath : " + downloadsFolderPath);
		File folder = new File(downloadsFolderPath);
		File[] listOfFiles = folder.listFiles();
		String importedFileName = listOfFiles[0].getName();
		log.info("importedFileName :" + importedFileName);
		String sheetName = importedFileName.replace(".xlsm", "");
		log.info("sheetname :" + sheetName);
		MainUtilities.waitFor(4000);
		
		//********************************************
		Desktop.getDesktop().open(new File(downloadsFolderPath + importedFileName));
		MainUtilities.waitFor(60000);
		log.info("After opening excel file");
		Screen S = new Screen();
		
		Pattern contentEnableEditing = new Pattern(System.getProperty("user.dir") + File.separator + "Sikuli_Images" + File.separator + "Enable_Editing.png");
		log.info("After clicking on Enable Editing button in excel file");
		S.wait(contentEnableEditing);
		S.click(contentEnableEditing);
		
		Pattern contentEnableContent = new Pattern(System.getProperty("user.dir") + File.separator + "Sikuli_Images" + File.separator + "Enable_Content.png");
		log.info("After clicking on Enable content button in excel file");
		S.wait(contentEnableContent);
		S.click(contentEnableContent);
		//********************************************
		
		Utilities.writeToExcel(downloadsFolderPath, importedFileName, sheetName);
		
		//********************************************		
		Pattern contentCancelBtn = new Pattern(System.getProperty("user.dir") + File.separator + "Sikuli_Images" + File.separator + "Cancel_BTN.png");
		log.info("After clicking on Enable Editing button in excel file");
		S.wait(contentCancelBtn);
		S.click(contentCancelBtn);
		
		Pattern contentSave = new Pattern(System.getProperty("user.dir") + File.separator + "Sikuli_Images" + File.separator + "Save.png");
		log.info("After clicking on Enable content button in excel file");
		S.wait(contentSave);
		S.click(contentSave);
		//********************************************
		
		log.info("before import excel");
		clickOnImportExcel();
		log.info("before clicking import choose file button");
		clickOnImportChooseFileButton();
		
		StringSelection s = new StringSelection(downloadsFolderPath + importedFileName);
		log.info("S : " + s);
		log.info("starting robot class");
  	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(s, null);
  	    Robot robot = new Robot();
  	    robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
  	    robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
  	    robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
  	    robot.keyPress(java.awt.event.KeyEvent.VK_V);
  	    robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
  	    MainUtilities.waitFor(3000);
  	    robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
  	  
  	    log.info("before clicking import upload excel button");
		clickOnImportUploadExcelButton();
	}
	/**
	 * This method is used to check whether the excel is Imported or not.
	 * @return isExcelImported isExcelImported
	 * @throws IOException
	 */
	public boolean isExcelImported() throws IOException {
		LinkedHashMap<String, String> gridData = getGridData();
		File folder = new File(downloadsFolderPath);
		File[] listOfFiles = folder.listFiles();
		String importedFileName = listOfFiles[0].getName();
		excelOperationFileName = importedFileName;
		String filePath = downloadsFolderPath + File.separator + importedFileName;
		LinkedHashMap<String, String> importedExcelData = Utilities.readExcel(filePath, importedFileName, importedFileName);
		boolean isExcelImported = compareGridandExcelData(importedExcelData, gridData);
		return isExcelImported;
	}
	/**
	 * This method is used to click on Import choose file button.
	 */
	public void clickOnImportChooseFileButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(IMPORT_CHOOSE_FILE_BUTTON));
		clickOnElement(imprtChooseFileButton);
	}
	/**
	 * This method is used to click on Import upload excel button.
	 */
	public void clickOnImportUploadExcelButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(IMPORT_UPLOAD_EXCEL_BUTTON));
		clickOnElement(importUploadExcelButton);
	}
	/**
	 * This method is used to get file track file name.
	 * @return fileTrackFileName fileTrackFileName
	 */
	public String getFileTrackFileName() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(FILE_NAME_EXPAND));
		String fileTrackFileName = fileNameExpand.getText();
		fileTrackFileName = fileTrackFileName.substring(13, fileTrackFileName.length()-1);
		return fileTrackFileName;
	}
	/**
	 * This method is used to get file track user name.
	 * @return fileTrackUser fileTrackUser
	 */
	public String getFileTrackUserName() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(FILE_TRACK_USER_NAME));
		String fileTrackUser = fileTrackUserName.getText();
		fileTrackUser = fileTrackUser.substring(10, fileTrackUser.length()-1);
		return fileTrackUser;
	}
	/**
	 * This method is used to delete all files in specified folder.
	 * @param filePath
	 * @throws IOException
	 */
	public void deleteAllFilesinThisFolder(String filePath) throws IOException {
		File folder = new File(filePath);
		FileUtils.cleanDirectory(folder); 
	}
	/**
	 * This method is used to get View Name.
	 * @return viewName viewName
	 */
	public String getViewName() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(GET_VIEW_NAME));
		String viewName = getViewName.getAttribute("value");
		return viewName;
	}
	/**
	 * This method is used to click on Excel operation button.
	 */
	public void clickOnExcelOperation() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(EXCEL_OPERATION));
		clickOnElement(excelOperation);
	}
	/**
	 * This method is used to click on Export Excel Option button.
	 */
	public void clickOnExportExcel() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(EXPORT_EXCEL_BUTTON));
		clickOnElement(exportExcel);
		MainUtilities.waitFor(15000);
	}
	/**
	 * This method is used to click on Import Excel option button.
	 */
	public void clickOnImportExcel() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(IMPORT_EXCEL_BUTTON));
		clickOnElement(importExcel);
		MainUtilities.waitFor(5000);
	}
	/**
	 * This method is used to click on File Track option button.
	 */
	public void clickonFileTrack() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(FILE_TRACK));
		clickOnElement(fileTrack);
		MainUtilities.waitFor(5000);
	}
	/**
	 * This method is used to click on File name drop down.
	 */
	public void clickOnFileName() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(FILE_NAME_EXPAND));
		clickOnElement(fileNameExpand);
	}
	/**
	 * This method is used to get Grid data (i.e., values along with column names) into linked hash map.
	 * @return listOfColumnNamesWithValuesFromGrid listOfColumnNamesWithValuesFromGrid
	 * @throws IOException 
	 */
	public LinkedHashMap<String, String> getGridData() {
		log.info("inside getGridData");
		LinkedHashMap<String, String> listOfColumnNamesWithValuesFromGrid = new LinkedHashMap<String, String>();
		try {
			listOfColumnNamesWithValuesFromGrid	= getCoumnNamesAndValuesFromGrid();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listOfColumnNamesWithValuesFromGrid;
	}
	/**
	 * This method is used to compare Excel data with Grid data.
	 * @param excel_Data
	 * @param grid_Data
	 * @return result If both the grid data and excel data are same then it will return true otherwise false.
	 */
	public boolean compareGridandExcelData(LinkedHashMap<String, String> excel_Data,LinkedHashMap<String, String> grid_Data) {
		boolean result = new ArrayList<String>(excel_Data.values()).equals(new ArrayList<String>(grid_Data.values()));
		return result;
	}
	/**
	 * This method is used to do File Track operation.
	 */
	public void checkFileTrack() {
		clickOnExcelOperation();
		clickonFileTrack();
		MainUtilities.waitFor(10000);
		waitForElementToVisible(By.xpath(FILE_TRACK_VISIBILITY));		
	}
	/**
	 * This method is used to verify File track functionality.
	 * @return true if file track is correct otherwise false.
	 */
	public boolean isFileTrackCorrect() {
		String fileTrackFileName = getFileTrackFileName();
		String fileTrackUserName = getFileTrackUserName();	
		String loginUserName = ParameterHelper.getParameterDefaultValue("userName");
		if (excelOperationFileName.equalsIgnoreCase(fileTrackFileName.trim()) && loginUserName.equalsIgnoreCase(fileTrackUserName.trim())) {
			return true;
		} else {
			return false;
		}
		
	}
	
	
	/**
	 * This method is used to click on Save Grid Changes button.
	 */
	public void clickOnSaveGridChangesButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(GridActionsIdentifier.SAVE_CHANGES_BUTTON));
		clickOnElement(saveChangesButton);
		MainUtilities.waitFor(4000);
	}
	
	/**
	 * This method is used to enter Ndc Wk Value in 1st row of grid.
	 * @param ndcWkValue 
	 */
	public void enterNdcWkNewValue(String ndcWkValue) {
		MainUtilities.waitFor(2000);
		log.info("waiting for ndc wk value");
		waitForElementToVisible(By.xpath(PSRGridIdentifier.NDC_WK_VALUE_IN_GRID));
		log.info("before double clicking");
		MainUtilities.waitFor(4000);
		clickOnElement(ndcWkValueInGrid);
		log.info("after one click");
		clickOnElement(ndcWkValueInGrid);
		MainUtilities.waitFor(6000);
		log.info("after double clicking");
		log.info("ndc value : " + ndcWkValue);
		setText(ndcWkValueInputInGrid, ndcWkValue);
		MainUtilities.waitFor(2000);
	}
	/**
	 * This method is used to get Ndc Wk Value from 1st row of Grid.
	 * @return ndcWkValue ndcWkValue
	 */
	public String getNdcWkValue() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(PSRGridIdentifier.NDC_WK_VALUE_IN_GRID));
		String ndcWkValue = getText(ndcWkValueInGrid);
		MainUtilities.waitFor(2000);
		return ndcWkValue;
	}
	/**
	 * This method is used to save grid changes.
	 */
	public void saveGridChanges() {
		enterNdcWkNewValue(newNdcWkValue);
		MainUtilities.waitFor(4000);
		clickOnSaveGridChangesButton();
	}
	/**
	 * This method is used to check whether the grid saved or not.
	 * @return true if the grid is saved correctly otherwise false.
	 */
	public boolean isGridSaved() {
		MainUtilities.waitFor(2000);
		String ndcWkValueFromGrid = getNdcWkValue();
		MainUtilities.waitFor(2000);
		if (ndcWkValueFromGrid.equalsIgnoreCase(newNdcWkValue)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method is used to click on Acknowledgement button.
	 */
	public void clickOnAcknowledgementButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(ACKNOWLEDGEMENT_BUTTON));
		clickOnElement(acknowledgementButton);
		MainUtilities.waitFor(15000);
	}
	/**
	 * This method is used to click on Select check box.
	 */
	public void clickOnSelectCheckBox() {
		MainUtilities.waitFor(4000);
		waitForElementToVisible(By.xpath(PSRGridIdentifier.SELECT_CHECK_BOX));
		clickOnElement(selectCheckBox);
		MainUtilities.waitFor(2000);
	}
	/**
	 * This method is used to Acknowledge the row.
	 */
	public void acknowledgeRow() {
		clickOnSelectCheckBox();
		log.info("After selecting check box");
		clickOnAcknowledgementButton();
	}
	/**
	 * This method is used to check the whether the row is acknowledged or not.
	 * @return
	 */
	public boolean isRowAcknowledged() {
		MainUtilities.waitFor(2000);
		String message = getMessageFromToaster();
		log.info("message get att : " + successMessage.getAttribute("value"));
		log.info("message get text : " + message);
		if (message.equalsIgnoreCase("Success!")) {
			return true;
		} else {
			return false;
		}
	}	
	/**
	 * This method is used to get message from the toaster
	 * @return message message
	 */
	public String getMessageFromToaster() {
		return successMessage.getText();
	}
}