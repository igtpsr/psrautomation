package com.mgf.psr.pages;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.mgf.psr.constants.EventModelConstant;
import com.mgf.psr.constants.OperatorConstant;
import com.mgf.psr.elementIdentifier.EventModelPageIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;

public class EventModelPage extends PageTemplate implements EventModelPageIdentifier {

	@FindBy(xpath = NEW_BTN)
	private WebElement newBtn;

	@FindBy(xpath = MODELNAME_TXTFIELD)
	private WebElement modelNameTxtField;

	@FindBy(xpath = ADD_NEW_ROW_FIRST_BTN)
	private WebElement addNewRowFirstBtn;

	@FindBy(xpath = EVENTMODEL_UNIQUEID)
	private WebElement eventModelUniqueId;

	@FindBy(xpath = SELECT_MATCH_CASE)
	private WebElement selectMatchCase;

	@FindBy(xpath = PLEASECHOOSE_DRPDOWN)
	private WebElement pleaseChooseDrpDown;

	@FindBy(xpath = MATCH_CASE_ROW)
	private WebElement matchCaseRow;

	@FindBy(xpath = MATCHFIELD_VALUE)
	private WebElement matchFieldTextValue;

	@FindBy(xpath = MATCH_FIELD_ROW)
	private WebElement matchFieldRow;

	@FindBy(xpath = ADD_NEW_ROW_SECOND_BTN)
	private WebElement addNewRowSecondBtn;

	@FindBy(xpath = EVENTCODE_POPUP)
	private WebElement eventCodePopup;

	@FindBy(xpath = EVENTCODE_SEARCHFIELD)
	private WebElement eventCodeSearchField;

	@FindBy(xpath = SELECT_EVENTCODE)
	private WebElement selectEventCode;

	@FindBy(xpath = EVENT_DETAILS_JQXID)
	private WebElement eventDetailsJqxid;

	@FindBy(xpath = TRIGGER_DAYS_ROW)
	private WebElement triggerDaysRow;

	@FindBy(xpath = TRIGGER_EVENT_ROW)
	private WebElement triggerEventRow;

	@FindBy(xpath = TRIGGER_TXTFIELD)
	private WebElement triggerTxtField;

	@FindBy(xpath = SAVE_BTN)
	private WebElement saveBtn;

	@FindBy(xpath = SELECT_OPERATOR)
	private WebElement selectOperator;

	@FindBy(xpath = SEARCH_MODEL_NAME)
	private WebElement searchModelName;

	@FindBy(xpath = MODEL_SEARCH_BTN)
	private WebElement modelSearchBtn;

	@FindBy(xpath = CREATED_MODEL_NAMES)
	private WebElement createdModelNames;

	@FindBy(xpath = UPDATE_MODEL_ICON)
	private WebElement updateModelIcon;

	@FindBy(xpath = UPDATE_MODEL_JQXID)
	private WebElement updateModelJqxid;

	@FindBy(xpath = TOTAL_ROWS_UPDATE_MODEL_DETAILS_GRID)
	private List<WebElement> totalRowsUpdateModelDetailsGrid;

	@FindBy(xpath = ECODE_DISPLAYED)
	private WebElement eCodeDisplayed;

	@FindBy(xpath = COPY_MODEL_ICON)
	private WebElement copyModelIcon;

	@FindBy(xpath = DISABLE_MODEL_CHECK_BOX)
	private WebElement disableModelCheckBox;

	@FindBy(xpath = DISABLE_MODEL_CHECK_BOX_STATUS)
	private WebElement disableModelCheckBoxStatus;

	@FindBy(xpath = DELETE_EVENT_MODEL_BTN)
	private WebElement deleteEventModelBtn;

	@FindBy(xpath = TOASTER_MSG)
	private WebElement toasterMsg;

	@FindBy(xpath = DISABLE_EVENT_CHECK_BOX)
	private WebElement disableEventCheckBox;

	@FindBy(xpath = DISABLE_EVENT_CHECK_BOX_STATUS)
	private WebElement disableEventCheckBoxStatus;

	@FindBy(xpath = REMOVE_EVENT_ROW)
	private WebElement removeEventRow;

	@FindBy(xpath = EMPTY_GRID)
	private WebElement emptyGrid;

	@FindBy(xpath = EVENT_MODEL_DETAILS_JQXID)
	private WebElement eventModelDetailsJqxid;

	/**
	 * This method used to click on 'New' Button.
	 */
	public void clickOnNewBtn() {
		waitForElementToVisible(By.xpath(NEW_BTN));
		clickOnElement(newBtn);
	}

	/**
	 * This method is used to enter 'Model Name' in model name text field
	 * 
	 * @param modelName
	 *            modelName
	 */
	public void enterModelName(final String modelName) {
		waitForElementToVisible(By.xpath(MODELNAME_TXTFIELD));
		setText(modelNameTxtField, modelName);
	}

	/**
	 * This method used to click on 'Add New Row Button'.
	 */
	public void clickOnAddNewRowFirstBtn() {
		waitForElementToVisible(By.xpath(ADD_NEW_ROW_FIRST_BTN));
		clickOnElement(addNewRowFirstBtn);
	}

	/**
	 * This method used to click on 'Select match case and match filed value'.
	 * 
	 * @param matchCaseList
	 *            matchCaseList
	 * @param fieldValueList
	 *            fieldValueList
	 * @throws InterruptedException
	 */
	public void selectMatchCase(List<String> matchCaseList, List<String> fieldValueList) throws InterruptedException {
		Actions actions = new Actions(driver);
		String uniqueID = getTextByAttribute(eventModelUniqueId, "id");
		log.info(uniqueID);

		int i = 0;
		for (String matchCasevalue : matchCaseList) {
			clickOnAddNewRowFirstBtn();
			String matchCaseRowNumber = MainUtilities.generateDynamicXpath(MATCH_CASE_ROW, String.valueOf(i), uniqueID);
			waitForElementToVisible(By.xpath(matchCaseRowNumber));
			actions.doubleClick(driver.findElement(By.xpath(matchCaseRowNumber))).perform();
			clickOnElement(pleaseChooseDrpDown);

			String selectMatchCase = MainUtilities.generateDynamicXpath(SELECT_MATCH_CASE, matchCasevalue);
			waitForElementToVisible(By.xpath(selectMatchCase));

			clickOnElement(driver.findElement(By.xpath(selectMatchCase)));

			String matchFieldRowNumber = MainUtilities.generateDynamicXpath(MATCH_FIELD_ROW, String.valueOf(i),
					uniqueID);
			waitForElementToVisible(By.xpath(matchCaseRowNumber));

			actions.doubleClick(driver.findElement(By.xpath(matchFieldRowNumber))).perform();

			setText(matchFieldTextValue, fieldValueList.get(i) + Keys.TAB);

			MainUtilities.waitFor(500);
			i++;

		}
		clickOnAddNewRowFirstBtn();

	}

	/**
	 * This method used to click on 'Add New Row Button second button'.
	 */
	public void clickOnAddNewRowSecondBtn() {
		waitForElementToVisible(By.xpath(ADD_NEW_ROW_SECOND_BTN));
		clickOnElement(addNewRowSecondBtn);
		MainUtilities.waitFor(1000);
	}

	/**
	 * This method is used to search 'Event Codes'
	 * 
	 * * @param eventCode eventCode
	 */
	public void searchEventCode(final String eventCode) {
		waitForElementToVisible(By.xpath(EVENTCODE_SEARCHFIELD));
		setText(eventCodeSearchField, eventCode);
		MainUtilities.waitFor(2000);
	}

	/**
	 * This method used to select on Event Code.
	 * 
	 * @param eCode
	 *            eCode
	 */
	public void selectEventCode(String eCode) {
		String selectECode = MainUtilities.generateDynamicXpath(SELECT_EVENTCODE, String.valueOf(eCode));
		waitForElementToVisible(By.xpath(selectECode));
		clickOnObjectWithJavaScript(driver.findElement(By.xpath(selectECode)));
		MainUtilities.waitFor(1000);
	}

	/**
	 * This method used to select on TRIGGER_DAYS_ROW.
	 */
	public void clickOnTriggerDays() {
		waitForElementToVisible(By.xpath(TRIGGER_DAYS_ROW));
		clickOnElement(triggerDaysRow);
	}

	/**
	 * This method used to select on TRIGGER_EVENT_ROW.
	 */
	public void clickOnTriggerEvent() {
		waitForElementToVisible(By.xpath(TRIGGER_EVENT_ROW));
		clickOnElement(triggerEventRow);
	}

	/**
	 * This method used to enter triggerDaysValue.
	 * 
	 * @param triggerDaysValue
	 *            triggerDaysValue
	 */
	public void enterTriggerDays(final String triggerDaysValue) {
		waitForElementToVisible(By.xpath(TRIGGER_TXTFIELD));
		setText(triggerTxtField, triggerDaysValue);
		MainUtilities.waitFor(1000);
	}

	/**
	 * This method used to enter triggerEventValue.
	 * 
	 * @param triggerEventValue
	 *            triggerEventValue
	 */
	public void enterTriggerEvent(final String triggerEventValue) {
		waitForElementToVisible(By.xpath(TRIGGER_TXTFIELD));
		setText(triggerTxtField, triggerEventValue);
		MainUtilities.waitFor(1000);
	}

	/**
	 * This method used to click on 'Save' Button.
	 */
	public void clickOnSave() {
		waitForElementToVisible(By.xpath(SAVE_BTN));
		clickOnElement(saveBtn);
	}

	/**
	 * This method used to enter 'Event model details'.
	 * 
	 * @param eCodeList
	 *            eCodeList
	 */
	public void enterModelDetails(List<String> eCodeList) {
		clickOnAddNewRowSecondBtn();
		int rowCount = 0;
		String modelDetailsRow = MainUtilities.generateDynamicXpath(EVENTCODE_POPUP, String.valueOf(rowCount));
		waitForElementToVisible(By.xpath(modelDetailsRow));
		clickOnElement(driver.findElement(By.xpath(modelDetailsRow)));
		String eCodeGacByVendor = EventModelConstant.E000_CODE;
		searchEventCode(eCodeGacByVendor);
		selectEventCode(eCodeGacByVendor);

		int i = 1;
		for (String eCode : eCodeList) {
			Actions actions = new Actions(driver);

			clickOnAddNewRowSecondBtn();
			String modelDetailsRowNumber = MainUtilities.generateDynamicXpath(EVENTCODE_POPUP, String.valueOf(i));
			waitForElementToVisible(By.xpath(modelDetailsRowNumber));
			clickOnElement(driver.findElement(By.xpath(modelDetailsRowNumber)));
			MainUtilities.waitFor(2000);
			searchEventCode(eCode);
			selectEventCode(eCode);
			String jqxid = getTextByAttribute(eventDetailsJqxid, "id");
			long triggerDays = MainUtilities.generateRandomDigits(2);

			String triggerEvent_Row = MainUtilities.generateDynamicXpath(TRIGGER_EVENT_ROW, String.valueOf(i), jqxid);
			waitForElementToVisible(By.xpath(triggerEvent_Row));
			actions.doubleClick(driver.findElement(By.xpath(triggerEvent_Row))).perform();
			MainUtilities.waitFor(2000);
			enterTriggerEvent(EventModelConstant.E000_CODE);

			String triggerDays_Row = MainUtilities.generateDynamicXpath(TRIGGER_DAYS_ROW, String.valueOf(i), jqxid);
			waitForElementToVisible(By.xpath(triggerDays_Row));
			actions.doubleClick(driver.findElement(By.xpath(triggerDays_Row))).perform();
			MainUtilities.waitFor(2000);
			enterTriggerEvent(String.valueOf(triggerDays));

			i++;
		}
		clickOnSave();

	}

	/**
	 * This method used to select 'operators' from the drop down.
	 * 
	 * @param Operator
	 *            Operator
	 */
	public void selectOpeartor(String Operator) {
		waitForElementToVisible(By.xpath(SELECT_OPERATOR));
		Select operator = new Select(driver.findElement(By.xpath(SELECT_OPERATOR)));
		clickOnElement(selectOperator);
		operator.selectByValue(Operator);

	}

	/**
	 * This method is used to search 'Model Name'
	 * 
	 * @param modelName
	 *            modelName
	 */
	public void searchModelName(final String modelName) {
		waitForElementToVisible(By.xpath(SEARCH_MODEL_NAME));
		setText(searchModelName, modelName);
	}

	/**
	 * This method used to click on ' model search Button'.
	 */
	public void clickOnModelSearchBtn() {
		waitForElementToVisible(By.xpath(MODEL_SEARCH_BTN));
		clickOnElement(modelSearchBtn);
		MainUtilities.waitFor(2000);
	}

	/**
	 * This method used to verify whether ' model is created'.
	 * 
	 * @param modelName
	 *            modelName
	 */
	public boolean isModelCreated(String modelName) {
		selectOpeartor(OperatorConstant.EQUAL_TO);
		searchModelName(modelName);
		MainUtilities.waitFor(2000);
		clickOnModelSearchBtn();
		MainUtilities.waitFor(2000);
		String actualModelName = MainUtilities.generateDynamicXpath(CREATED_MODEL_NAMES, modelName);
		waitForElementToVisible(By.xpath(actualModelName));

		if (isElementDisplayed(driver.findElement(By.xpath(actualModelName)))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method used to click on 'Update Model' icon.
	 */
	public void clicOnkUpdateModelIcon() {
		waitForElementToVisible(By.xpath(UPDATE_MODEL_ICON));
		clickOnElement(updateModelIcon);
	}

	/**
	 * This method used to get 'Update Model jqxid'.
	 */
	public String getUpdateModelJqxid() {
		waitForElementToVisible(By.xpath(UPDATE_MODEL_JQXID));
		return getTextByAttribute(updateModelJqxid, "id");

	}

	/**
	 * This method used to get total number of rows of 'Update Model details grid'.
	 */
	public int getTotalRowsOfUpdateModelGrid() {
		String rowCount = MainUtilities.generateDynamicXpath(TOTAL_ROWS_UPDATE_MODEL_DETAILS_GRID,
				getUpdateModelJqxid());
		waitForElementToVisible(By.xpath(rowCount));
		return driver.findElements(By.xpath(rowCount)).size();
	}

	/**
	 * This method used to update 'Model details grid'.
	 * 
	 * @param modelName
	 *            modelName
	 * @param eCodeUpdateList
	 *            eCodeUpdateList
	 */
	public void updateModelDetails(String modelName, List<String> eCodeUpdateList) {
		clicOnkUpdateModelIcon();

		int i = getTotalRowsOfUpdateModelGrid();
		System.out.println("Value of i :" + i);

		for (String eCode : eCodeUpdateList) {
			Actions actions = new Actions(driver);

			clickOnAddNewRowSecondBtn();

			String modelDetailsRowNumber = MainUtilities.generateDynamicXpath(EVENTCODE_POPUP, String.valueOf(i));
			waitForElementToVisible(By.xpath(modelDetailsRowNumber));
			clickOnElement(driver.findElement(By.xpath(modelDetailsRowNumber)));
			MainUtilities.waitFor(1000);
			searchEventCode(eCode);
			selectEventCode(eCode);
			String jqxid = getTextByAttribute(updateModelJqxid, "id");
			long triggerDays = MainUtilities.generateRandomDigits(2);

			String triggerEvent_Row = MainUtilities.generateDynamicXpath(TRIGGER_EVENT_ROW, String.valueOf(i), jqxid);
			waitForElementToVisible(By.xpath(triggerEvent_Row));
			actions.doubleClick(driver.findElement(By.xpath(triggerEvent_Row))).perform();
			MainUtilities.waitFor(1000);
			enterTriggerEvent(EventModelConstant.E000_CODE);

			String triggerDays_Row = MainUtilities.generateDynamicXpath(TRIGGER_DAYS_ROW, String.valueOf(i), jqxid);
			waitForElementToVisible(By.xpath(triggerDays_Row));
			actions.doubleClick(driver.findElement(By.xpath(triggerDays_Row))).perform();
			MainUtilities.waitFor(1000);
			enterTriggerEvent(String.valueOf(triggerDays));

			i++;
		}
		clickOnSave();

	}

	/**
	 * This method used to verify 'Model is updated '.
	 */
	public boolean isModelUpdated() {
		boolean ModelUpdated = false;
		int rowCountActual = getTotalRowsOfUpdateModelGrid();
		int rowCountExpected = EventModelConstant.EVENT_CODE_LIST.size()
				+ EventModelConstant.EVENT_CODE_UPDATE_LIST.size() + 1;

		for (String eCode : EventModelConstant.EVENT_CODE_UPDATE_LIST) {

			String EventCode = MainUtilities.generateDynamicXpath(ECODE_DISPLAYED, eCode);
			if (isElementDisplayed(driver.findElement(By.xpath(EventCode))) && rowCountActual == rowCountExpected) {
				ModelUpdated = true;
			}
		}
		return ModelUpdated;

	}

	/**
	 * This method used to click on 'Copy Model' icon.
	 */
	public void clicOnkCopyModelIcon() {
		waitForElementToVisible(By.xpath(COPY_MODEL_ICON));
		clickOnElement(copyModelIcon);
	}

	/**
	 * This method used to enter 'Model new model name '.
	 * 
	 * @param modelName
	 *            modelName
	 */
	public void enterModelNameCopy(String modelName) {
		isModelCreated(modelName);
		clicOnkCopyModelIcon();
		enterModelName(modelName + "Copy");
		MainUtilities.waitFor(2000);
		clickOnSave();
		MainUtilities.waitFor(2000);

	}

	/**
	 * This method used to verify 'Model Copy is created '.
	 * 
	 * @param modelName
	 *            modelName
	 */
	public boolean isModelNameCopyCreated(String modelName) {
		return isModelCreated(modelName);

	}

	/**
	 * This method used to disable 'Model'.
	 * 
	 * @param modelName
	 *            modelName
	 */
	public boolean disableModel(String modelName) {

		selectOpeartor(OperatorConstant.EQUAL_TO);
		searchModelName(modelName);
		clickOnModelSearchBtn();
		MainUtilities.waitFor(2000);
		String actualModelName = MainUtilities.generateDynamicXpath(CREATED_MODEL_NAMES, modelName);
		waitForElementToVisible(By.xpath(actualModelName));

		String checkBoxStatus = getTextByAttribute(disableModelCheckBoxStatus, "class");

		if (StringUtils.isBlank(checkBoxStatus)) {
			clickOnElement(disableModelCheckBox);
			MainUtilities.waitFor(1000);
			Alert alert = driver.switchTo().alert();
			MainUtilities.waitFor(1000);
			alert.accept();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method used to verify 'Model is disabled'.
	 * 
	 * @param modelName
	 *            modelName
	 */
	public boolean isModelDisabled(String modelName) {
		clickOnModelSearchBtn();
		MainUtilities.waitFor(2000);
		String actualModelName = MainUtilities.generateDynamicXpath(CREATED_MODEL_NAMES, modelName);
		waitForElementToVisible(By.xpath(actualModelName));

		String checkBoxStatus = getTextByAttribute(disableModelCheckBoxStatus, "class");
		if (isElementDisplayed(driver.findElement(By.xpath(actualModelName))) && checkBoxStatus.contains("checked")) {
			System.out.println("model disabled");
			return true;
		} else {
			return false;

		}

	}

	/**
	 * This method used to click on 'Save' Button.
	 */
	public void clickOnDeleteBtn() {
		waitForElementToVisible(By.xpath(DELETE_EVENT_MODEL_BTN));
		clickOnElement(deleteEventModelBtn);
	}

	/**
	 * This method used to read 'Toaster' Message.
	 */
	public String getoasterMsg() {
		waitForElementToVisible(By.xpath(TOASTER_MSG));
		return getText(toasterMsg);
	}

	/**
	 * This method used to verify 'Event Model is associated in auto attachment'.
	 * 
	 * @param modelName
	 *            modelName
	 */
	public boolean isEventModelAssociatedInAutoAttachment(String modelName) {
		clicOnkUpdateModelIcon();
		clickOnDeleteBtn();
		Alert alert = driver.switchTo().alert();
		MainUtilities.waitFor(1000);
		alert.accept();
		boolean isModelAssociatedInAutoAttachment = false;
		if (getoasterMsg().contains("cannot be deleted")) {
			isModelAssociatedInAutoAttachment = true;
		} else {
			isModelAssociatedInAutoAttachment = false;
		}
		return isModelAssociatedInAutoAttachment;
	}

	public boolean isModelDeleted(String modelName) {

		boolean isModelDeleted = false;

		if (isEventModelAssociatedInAutoAttachment(modelName)) {
			isModelDeleted = false;
		} else {
			isModelDeleted = true;
		}
		return isModelDeleted;
	}

	/**
	 * This method used to disable 'Event' .
	 */
	public void disableEvent() {

		waitForElementToVisible(By.xpath(DISABLE_EVENT_CHECK_BOX));
		clickOnElement(disableEventCheckBox);
	}

	/**
	 * This method used to verify 'Event is disabled or no'.
	 */
	public boolean isEventDisabled() {
		clicOnkUpdateModelIcon();
		disableEvent();
		clickOnSave();
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(DISABLE_EVENT_CHECK_BOX_STATUS));
		String checkBoxStatus = getTextByAttribute(disableEventCheckBoxStatus, "class");
		if (checkBoxStatus.contains("checked")) {
			return true;
		} else {
			return false;

		}

	}

	/**
	 * This method used to verify 'Event is removed or no'.
	 * 
	 * 
	 */
	public boolean isEventRowRemoved(String modelName) {
		clicOnkUpdateModelIcon();
		String jqxid = getTextByAttribute(eventModelDetailsJqxid, "id");
		String removeEventRow = MainUtilities.generateDynamicXpath(REMOVE_EVENT_ROW, String.valueOf(2), jqxid);
		waitForElementToVisible(By.xpath(removeEventRow));
		clickOnElement(driver.findElement(By.xpath(removeEventRow)));
		MainUtilities.waitFor(1000);
		Alert alert = driver.switchTo().alert();
		MainUtilities.waitFor(1000);
		alert.accept();

		boolean isEventAssociatedInAutoAttachment = false;
		if (getoasterMsg().contains("cannot be removed") || (getoasterMsg().contains("Successfully"))) {
			isEventAssociatedInAutoAttachment = true;
		} else {
			isEventAssociatedInAutoAttachment = false;
		}
		return isEventAssociatedInAutoAttachment;

	}
	
	/**
	 * This method is used to verify event is removed or not
	 * @param  modelName modelName
	 * @return true if event is removed else false
	 */
	public boolean isEventRemoved(String modelName) {

		boolean isEventRemoved = false;

		if (isEventRowRemoved(modelName)) {
			isEventRemoved = false;
		} else {
			isEventRemoved = true;
		}
		return isEventRemoved;
	}

	/**
	 * This method used to verify 'empty grid '.
	 * 
	 */
	public boolean isEmptyGridDisplayed() {
		waitForElementToVisible(By.xpath(EMPTY_GRID));
		if (isElementDisplayed(emptyGrid)) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * This method used to verify whether 'event code is visible or no'.
	 * @param event event
	 * @return true if isEventCodeVisible else false
	 */
	public boolean isEventCodeVisible(String event) {

		String eventCode = MainUtilities.generateDynamicXpath(ECODE_DISPLAYED, event);

		if (isElementDisplayed(driver.findElement(By.xpath(eventCode)))) {
			return true;
		} else {
			return false;
		}
		
	}

}