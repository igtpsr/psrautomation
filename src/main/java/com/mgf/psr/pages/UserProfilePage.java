package com.mgf.psr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import com.mgf.psr.constants.OperatorConstant;
import com.mgf.psr.elementIdentifier.LeftPanelPageUserProfileIdentifier;
import com.retelzy.core.selenium.PageTemplate;
import com.retelzy.utilities.MainUtilities;


public class UserProfilePage extends PageTemplate implements LeftPanelPageUserProfileIdentifier {

	@FindBy(xpath = DASHBOARD_LINK)
	private WebElement dashboardLink;

	@FindBy(xpath = USERPROFILE_LINK)
	private WebElement userProfileLink;

	@FindBy(xpath = CREATE_NEW_USERPROFILE_LINK)
	private WebElement creatNewUserProfileLink;

	@FindBy(xpath = USER_ID_INPUT)
	private WebElement userIdInput;

	@FindBy(xpath = USER_NAME_INPUT)
	private WebElement userNameInput;

	@FindBy(xpath = PASSWORD_INPUT)
	private WebElement passwordInput;

	@FindBy(xpath = CFMPASSWORD_INPUT)
	private WebElement cfmPasswordInput;

	@FindBy(xpath = EMAIL_INPUT)
	private WebElement eMailInput;

	@FindBy(xpath = ROLE_DROPDOWN)
	private WebElement roleDropdownInput;

	@FindBy(xpath = COUNTRY_DROPDOWN)
	private WebElement countryDropdownInput;

	@FindBy(xpath = QUERYNAME_DROPDOWN)
	private WebElement queryNameDropdownInput;

	@FindBy(xpath = BRUSERID_INPUT)
	private WebElement brUserIDInput;

	@FindBy(xpath = USER_PROFILE_SAVE)
	private WebElement userProfileSaveButton;

	@FindBy(xpath = PASSWORD_NEVER_EXPIRES)
	private WebElement pswdNeverExpInput;

	@FindBy(xpath = SEARCH_USER_NAME_INPUT)
	private WebElement searchUserNameInput;

	@FindBy(xpath = SEARCH_USER_INPUT)
	private WebElement clickSearchUserButton;
	
	@FindBy(xpath = DELETE_USER_BUTTON)
	private WebElement deleteUserButton;
	
	@FindBy(xpath = UPDATE_USER_APP_BUTTON)
	private WebElement updateUserAppButton;
	
	@FindBy(xpath = UPDATE_USER_FORM_BUTTON)
	private WebElement updateUserformButton;
	
	@FindBy(xpath =  USER_DETAILS_ID)
	private WebElement userDetailsId;
	
	@FindBy(xpath = USERNAME_SELECT_OPERATOR)
	private WebElement userNameSelectOperator;
	
	@FindBy(xpath = USER_NO_DATA_TO_DISPLAY)
	private WebElement userNoDataToDisplay;
	
	private String updated_Email = ""; 
	
	/**
	 * This is method used to create User profile.
	 * @param userID
	 * @param userName
	 * @param password
	 * @param brUserID
	 * @param role
	 * @param country
	 * @param queryName
	 */
	public void createUserProfile(String userID,String userName,String password,String brUserID,String role,String country,String queryName) {
		MainUtilities.waitFor(12000);
		clickOnNewUserProfileLink();
		enterUserid(userID);
		enterUsername(userName);
		enterPassword(password);
		enterCfmPassword(password);
		enterEmail(MainUtilities.generateRandomEmail());
		selectRole(role);
		selectCountry(country);
		selectQuery(queryName);
		enterBRUserid(brUserID);
		clickOnPswdNeverExpires();
		MainUtilities.waitFor(2000);
		clickOnDashboardLink();
		MainUtilities.waitFor(2000);
		clickOnSaveUserProfileButton();
		MainUtilities.waitFor(4000);
	}
	
	/**
	 * This method is used to select user name operator.
	 * @param Operator
	 */
	public void selectUserNameOpeartor(String Operator) {
		waitForElementToVisible(By.xpath(USERNAME_SELECT_OPERATOR));
		Select operator = new Select(driver.findElement(By.xpath(USERNAME_SELECT_OPERATOR)));
		clickOnElement(userNameSelectOperator);
		operator.selectByValue(Operator); 
	}
	
	/**
	 * This method is used to check whether the user is created or not
	 * @param userID
	 * @return boolean
	 */
	public boolean isUserCreated(String userID) {
		MainUtilities.waitFor(1000);
		selectUserNameOpeartor(OperatorConstant.EQUAL_TO);
		enterSearchUserID(userID);
		MainUtilities.waitFor(1000);
		clickOnSearchButton();
		MainUtilities.waitFor(1000);
		String actualModelName = MainUtilities.generateDynamicXpath(USER_DETAIL_ID_IN_GRID, userID);
		waitForElementToVisible(By.xpath(actualModelName));

		if (isElementDisplayed(driver.findElement(By.xpath(actualModelName)))) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method is used to check whether the user is updated or not
	 * @param userID
	 * @return boolean
	 */
	public boolean isUserUpdated(String userID) {
		
		selectUserNameOpeartor(OperatorConstant.EQUAL_TO);
		enterSearchUserID(userID);
		MainUtilities.waitFor(1000);
		clickOnSearchButton();
		MainUtilities.waitFor(1000);
		String actualModelName = MainUtilities.generateDynamicXpath(USER_DETAIL_ID_IN_GRID, userID);
		waitForElementToVisible(By.xpath(actualModelName));
		selectUserInGrid(userID);
		clickonUpdateUserAppButton();
		MainUtilities.waitFor(4000);
		waitForElementToVisible(By.xpath(EMAIL_INPUT));
		String eMail = eMailInput.getAttribute("value");
		log.info("text in email field : " + eMail);
		log.info("Updated email value : " + updated_Email);
		if (eMail.equalsIgnoreCase(updated_Email)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method is used to check whether the user is deleted or not
	 * @param userID
	 * @return boolean
	 */
	public boolean isUserDeleted(String userID) {
		selectUserNameOpeartor(OperatorConstant.EQUAL_TO);
		enterSearchUserID(userID);
		MainUtilities.waitFor(1000);
		clickOnSearchButton();
		MainUtilities.waitFor(1000);
		waitForElementToVisible(By.xpath(USER_NO_DATA_TO_DISPLAY));
		if (isElementDisplayed(userNoDataToDisplay)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method used to Delete User profile.
	 * @param userID
	 */
	public void deleteUserProfile(String userID) {
		enterSearchUserID(userID);
		clickOnSearchButton();	
		selectUserInGrid(userID);
		clickdeleteUserProfileButton();
	}
	
	/**
	 * This method used to Update User profile.
	 * @param userID
	 */
	public void updateUserProfile(String userID) {
		enterSearchUserID(userID);
		clickOnSearchButton();		
		selectUserInGrid(userID);
		clickonUpdateUserAppButton();
		updated_Email = MainUtilities.generateRandomEmail();
		enterEmail(updated_Email);
		clickupdateUserProfileButton();
	}
	
	/**
	 * This method used to click on password never expires checkbox.
	 */
	public void clickOnPswdNeverExpires() {
		waitForElementToVisible(By.xpath(PASSWORD_NEVER_EXPIRES));
		clickOnElement(pswdNeverExpInput);
	}	

	/**
	 * This method used to click on 'DASHBOARD_LINK' button.
	 */
	public void clickOnDashboardLink() {
		MainUtilities.waitFor(10000);
		waitForElementToVisible(By.xpath(DASHBOARD_LINK));
		clickOnObjectWithJavaScript(dashboardLink);
	}

	/**
	 * This method used to click on 'USERPROFILE_LINK' button.
	 */
	public void clickOnUserProfileLink() {
		MainUtilities.waitFor(10000);
		waitForElementToVisible(By.xpath(USERPROFILE_LINK));
		clickOnElement(userProfileLink);
	}

	/**
	 * This method used to click on 'CREATE_NEW_USERPROFILE_LINK' button.
	 */
	public void clickOnNewUserProfileLink() {
		waitForElementToVisible(By.xpath(CREATE_NEW_USERPROFILE_LINK));
		clickOnElement(creatNewUserProfileLink);
	}

	/**
	 * This method used to enter 'UserID' input.
	 * @param userID
	 */
	public void enterUserid(final String userID) {
		waitForElementToVisible(By.xpath(USER_ID_INPUT));
		setText(userIdInput, userID);
	}

	/**
	 * This method used to enter 'UserName' input.
	 * @param username
	 */
	public void enterUsername(final String username) {
		waitForElementToVisible(By.xpath(USER_NAME_INPUT));
		setKey(userNameInput, username);
	}

	/**
	 * This method used to enter 'password' input.
	 * @param password
	 */
	public void enterPassword(final String password) {
		waitForElementToVisible(By.xpath(PASSWORD_INPUT));
		setKey(passwordInput, password);
	}

	/**
	 * This method used to enter 'confirm password' input.
	 * @param cfmpassword
	 */
	public void enterCfmPassword(final String cfmpassword) {
		waitForElementToVisible(By.xpath(CFMPASSWORD_INPUT));
		setKey(cfmPasswordInput, cfmpassword);
	}

	/**
	 * This method used to enter 'email' input.
	 * @param email
	 */
	public void enterEmail(final String email) {
		MainUtilities.waitFor(5000);
		waitForElementToVisible(By.xpath(EMAIL_INPUT));
		setText(eMailInput, email);
	}

	/**
	 * This method used to select 'Role' dropdown input.
	 * @param role
	 */
	public void selectRole(final String role) {
		waitForElementToVisible(By.xpath(ROLE_DROPDOWN));
		MainUtilities.waitFor(2000);
		clickOnObjectWithJavaScript(roleDropdownInput);
		String selectRole = MainUtilities.generateDynamicXpath(ROLE_INPUT, role);
		waitForElementToVisible(By.xpath(selectRole));
		clickOnObjectWithJavaScript(driver.findElement(By.xpath(selectRole)));

	}

	/**
	 * This method used to select 'Country' dropdown input.
	 * @param country
	 */
	public void selectCountry(final String country) {
		waitForElementToVisible(By.xpath(COUNTRY_DROPDOWN));
		MainUtilities.waitFor(2000);
		clickOnElement(countryDropdownInput);
		String selectCountry = MainUtilities.generateDynamicXpath(COUNTRY, country);
		waitForElementToVisible(By.xpath(selectCountry));
		MainUtilities.waitFor(2000);
		clickOnElement(driver.findElement(By.xpath(selectCountry)));
	}

	/**
	 * This method used to select 'Query' dropdown input.
	 * @param query
	 */
	public void selectQuery(final String query) {
		waitForElementToVisible(By.xpath(QUERYNAME_DROPDOWN));
		clickOnElement(queryNameDropdownInput);
		String selectQueryName = MainUtilities.generateDynamicXpath(QUERYNAME, query);
		waitForElementToVisible(By.xpath(selectQueryName));
		clickOnElement(driver.findElement(By.xpath(selectQueryName)));
	}

	/**
	 * This method used to enter 'BR User ID'.
	 * @param brUserID
	 */
	public void enterBRUserid(final String brUserID) {
		waitForElementToVisible(By.xpath(BRUSERID_INPUT));
		setKey(brUserIDInput, brUserID);
	}

	/**
	 * This method used to click on user profile save button.
	 */
	public void clickOnSaveUserProfileButton() {
		MainUtilities.waitFor(10000);
		waitForElementToVisible(By.xpath(USER_PROFILE_SAVE));
		clickOnElement(userProfileSaveButton);
		MainUtilities.waitFor(10000);
	}

	/**
	 * This method used to enter Search user name input.
	 * @param searchUserName
	 */
	public void enterSearchUserID(String searchUserName) {
		MainUtilities.waitFor(4000);
		waitForElementToVisible(By.xpath(SEARCH_USER_NAME_INPUT));
		setText(searchUserNameInput, searchUserName);
	}

	/**
	 * This method is used to click on search user name button.
	 */
	public void clickOnSearchButton() {
		MainUtilities.waitFor(2000);
		waitForElementToVisible(By.xpath(SEARCH_USER_INPUT));
		clickOnElement(clickSearchUserButton);
		MainUtilities.waitFor(12000);
	}
	
	/**
	 * This method is used to select check box of user profile.
	 * @param userName
	 */
	public void selectUserInGrid(String userName) {
		waitForElementToVisible(By.xpath(USER_DETAILS_ID));		
		String jqxid = getTextByAttribute(userDetailsId, "id");
		String userDetailsIdInGrid = MainUtilities.generateDynamicXpath(USER_DETAIL_ID_IN_GRID, jqxid);
		log.info("User details id: " + userDetailsIdInGrid);
		waitForElementToVisible(By.xpath(userDetailsIdInGrid));
		clickOnElement(driver.findElement(By.xpath(userDetailsIdInGrid)));
		MainUtilities.waitFor(4000);
	}
	
	/**
	 * This method is used to delete user profile
	 */
	public void clickdeleteUserProfileButton() {
		waitForElementToVisible(By.xpath(DELETE_USER_BUTTON));
		clickOnElement(deleteUserButton);
		driver.switchTo().alert().accept();
		MainUtilities.waitFor(6000);
	}
	
	/**
	 * This method is used to click on Update button in APP
	 */
	public void clickonUpdateUserAppButton() {
		MainUtilities.waitFor(4000);
		waitForElementToVisible(By.xpath(UPDATE_USER_APP_BUTTON));
		clickOnElement(updateUserAppButton);
	}
	
	/**
	 * This method is used to click on Update button in saving user profile
	 */
	public void clickupdateUserProfileButton() {
		MainUtilities.waitFor(4000);
		waitForElementToVisible(By.xpath(UPDATE_USER_FORM_BUTTON));
		clickOnElement(updateUserformButton);
		MainUtilities.waitFor(4000);
	}
	
}
