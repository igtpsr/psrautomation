package com.mgf.psr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mgf.psr.elementIdentifier.TopMenuPageIdentifier;
import com.retelzy.core.selenium.PageTemplate;

public class TopMenuPage extends PageTemplate implements TopMenuPageIdentifier{

	@FindBy(xpath = LOGOUT_LINK)
	private WebElement logoutLink;
	/**
	 * This method used to wait for user to logged in.
	 */
	public void waitForUserToLoggedIn() {
		waitForElementToVisible(By.xpath(LOGOUT_LINK));
	}
	
	/**
	 * This method used to verify logout link.
	 * 
	 * @return true if logout link displayed else false
	 */
	public boolean isLogoutLinkDisplayed() {
		waitForUserToLoggedIn();
		boolean isUserLoggedIn = false;
		try {
			if (isElementDisplayed(logoutLink)) {
				isUserLoggedIn = true;
			} else {
				isUserLoggedIn = false;
			}
		} catch (Exception exception) {
			isUserLoggedIn = false;
		}
		return isUserLoggedIn;
	}
}
