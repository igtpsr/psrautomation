package com.mgf.psr.pages;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mgf.psr.elementIdentifier.LoginPageIdentifier;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.core.selenium.PageTemplate;

public class LoginPage extends PageTemplate implements LoginPageIdentifier {

	private String baseUrl;

	@FindBy(xpath = USERNAME_TXTFIELD)
	private WebElement userNameInput;

	@FindBy(xpath = PASSWORD_TXTFIELD)
	private WebElement passwordInput;

	@FindBy(xpath = SUBMIT_BTN)
	private WebElement submitBtn;

	public LoginPage(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public void openApplication() {
		log.info("Opening PSR Application.." + baseUrl);
		launchApp(baseUrl);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		final String browser;
		if (StringUtils.isBlank(System.getProperty("browser"))) {
			browser = ParameterHelper.getParameterDefaultValue("browser");
		} else {
			browser = System.getProperty("browser");
		}
	}

	/**
	 * This method used to click on 'Login' button.
	 */
	public void clickOnLoginBtn() {
		clickOnElement(submitBtn);
	}

	/**
	 * This method used to enter 'Username'.
	 */
	public void enterUsername(final String userName) {
		waitForElementToVisible(By.xpath(USERNAME_TXTFIELD));
		setText(userNameInput, userName);
	}

	/**
	 * This method used to enter 'password'.
	 */
	public void enterPassword(final String password) {
		waitForElementToVisible(By.xpath(PASSWORD_TXTFIELD));
		setText(passwordInput, password);
	}

	/**
	 * This method used to login to application.
	 * 
	 * @param userName
	 * @param password
	 */
	public void loginToApplication(final String userName, final String password) {
		log.info("Entering login credentials...");
		waitForElementToVisible(By.xpath(SUBMIT_BTN));
		enterUsername(userName);
		enterPassword(password);
		clickOnLoginBtn();
	}

	/**
	 * This method used to wait for login page to displayed.
	 */
	public void waitForLoginPageDisplayed() {
		waitForElementToVisible(By.xpath(SUBMIT_BTN));
	}

	/**
	 * This method used to verify login page displayed or not.
	 * 
	 * @return true if displayed else false
	 */
	public boolean isLoginPageDisplayed() {
		waitForLoginPageDisplayed();
		boolean isLoginPageDisplayed = false;
		try {
			if (isElementDisplayed(userNameInput) && isElementDisplayed(passwordInput)) {
				isLoginPageDisplayed = true;
			}
		} catch (Exception exception) {
			isLoginPageDisplayed = false;
		}
		return isLoginPageDisplayed;
	}
}
