package com.mgf.psr.elementIdentifier;

public interface LeftPanelPageUserProfileIdentifier {
	/**
	 * DASHBOARD_LINK.
	 */
	public static final String DASHBOARD_LINK = "//li[@class='icon-dv']";
	/**
	 * USERPROFILE_LINK
	 */
	public static final String USERPROFILE_LINK = "//a[@routerlink='/user/user-profile']";
	/**
	 * CREATE_NEW_USERPROFILE_LINK
	 */
	public static final String CREATE_NEW_USERPROFILE_LINK = "//button[@routerlink='/user/create-user']";
	/**
	 * USER_ID_INPUT
	 */
	public static final String USER_ID_INPUT = "//input[@name='userid']";
	/**
	 * USER_NAME_INPUT
	 */
	public static final String USER_NAME_INPUT = "//input[@name='username']";
	/**
	 * PASSWORD_INPUT
	 */
	public static final String PASSWORD_INPUT = "//input[@name='password']";
	/**
	 * CFMPASSWORD_INPUT
	 */
	public static final String CFMPASSWORD_INPUT = "//input[@name='confirmPassword']";
	/**
	 * EMAIL_INPUT
	 */
	public static final String EMAIL_INPUT = "//input[@name='useremail']";
	/**
	 * ROLE_INPUT
	 */
	public static final String ROLE_INPUT = "//div[text()='{0}']";
	/**
	 * ROLE_DROPDOWN
	 */
	public static final String ROLE_DROPDOWN = "//input[@placeholder='Select Role']";											
	/**
	 * COUNTRY
	 */
	public static final String COUNTRY = "//div[text()='{0}']";
	/**
	 * COUNTRY
	 */
	public static final String COUNTRY_DROPDOWN = "//ng-select[@placeholder='Select Country']/div";
	/**
	 * QUERYNAME
	 */
	public static final String QUERYNAME = "//div[contains (text(), '{0}')]";
	/**										
	 * QUERYNAME
	 */
	public static final String QUERYNAME_DROPDOWN = "//ng-select[@placeholder='Select Query Name']/div";
													
	/**
	 * BRUSERID_INPUT
	 */
	public static final String BRUSERID_INPUT = "//input[@name='brUserID']";
	/**
	 * PASSWORD_NEVER_EXPIRES
	 */
	public static final String PASSWORD_NEVER_EXPIRES = "//label[@for='passwordNeverExpire']";
	/**
	 * USER_PROFILE_SAVE
	 */
	public static final String USER_PROFILE_SAVE = "//button[text()='SAVE']";
	/**
	 * SEARCH_USER_NAME_INPUT
	 */
	public static final String SEARCH_USER_NAME_INPUT = "//select[@name='Useroperator']/following-sibling::input";
	/**
	 * SEARCH_USER_INPUT
	 */
	public static final String SEARCH_USER_INPUT = "//button[contains(text(),'Search')]";
	/**
	 * DELETE_USER_BUTTON
	 */
	public static final String DELETE_USER_BUTTON = "//button[contains(text(),' DELETE')]";
	/**
	 * UPDATE_USER_APP_BUTTON
	 */
	public static final String UPDATE_USER_APP_BUTTON = "//button[contains(text(),' UPDATE')]";
	/**
	 * UPDATE_USER_FORM_BUTTON
	 */									
	public static final String UPDATE_USER_FORM_BUTTON = "//*[@id=\"container\"]/div/app-create-user/div/form/div[1]/button[2]";
	/**
	 * USER_DETAILS_ID
	 */
	public static final String USER_DETAILS_ID = "//div[@class='jqx-grid-main-container']/jqxgrid/div";
	/**
	 * USER_DETAIL_ID_IN_GRID
	 */
	public static final String USER_DETAIL_ID_IN_GRID = "//*[@id='row0{0}']/div[1]/div";
	/**
	 * USERNAME_SELECT_OPERATOR.
	 */
	public static String USERNAME_SELECT_OPERATOR = "//select[@Id='Useroperator']";
	/**
	 * USER_NO_DATA_TO_DISPLAY.
	 */
	public static String USER_NO_DATA_TO_DISPLAY = "//span[text()='No data to display']";
}
