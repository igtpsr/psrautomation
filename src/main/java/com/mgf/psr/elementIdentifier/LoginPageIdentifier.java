package com.mgf.psr.elementIdentifier;

public interface LoginPageIdentifier {

	/**
	 * USERNAME_TXTFIELD.
	 */
	public static final String USERNAME_TXTFIELD = "//input[@name='username']";
	/**
	 * PASSWORD_TXTFIELD
	 */
	public static final String PASSWORD_TXTFIELD = "//input[@name='password']";
	/**
	 * SUBMIT_BTN
	 */
	public static final String SUBMIT_BTN = "//button[@id='login']";
}
