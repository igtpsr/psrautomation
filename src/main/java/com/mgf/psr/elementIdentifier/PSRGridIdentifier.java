package com.mgf.psr.elementIdentifier;

public interface PSRGridIdentifier {
	/**
	 * SELECT_COLUMN_DROPDOWN.
	 */
	public static String SELECT_COLUMN_DROPDOWN = "//ng-select[@id='dropdownscroll']";
	
	/**
	 * COLUMN_OPTION.
	 */
	public static String COLUMN_OPTION = "//div[text()='{0}']";
	/**
	 * LOADING_ICON
	 */
	public static final String LOADING_ICON ="//span[text()='Loading...']";
	/**
	 * SELECT_CHECK_BOX
	 */
	public static final String SELECT_CHECK_BOX = "(//div[contains(@class, 'checkbox')]/div)[1]";
	/**
	 * STYLE_VALUE_IN_GRID
	 */
	public static final String STYLE_VALUE_IN_GRID = "//div[contains (@class , 'style0')]";
	/**
	 * COLOR_WASH_NAME_VALUE_IN_GRID
	 */
	public static final String COLOR_WASH_NAME_VALUE_IN_GRID = "//div[contains (@class , 'colorWashName0')]";
	/**
	 * ORDER_COMMIT_VALUE_IN_GRID
	 */
	public static final String ORDER_COMMIT_VALUE_IN_GRID = "//div[contains (@class , 'ocCommit0')]";
	/**
	 * VPO_VALUE_IN_GRID
	 */
	public static final String VPO_VALUE_IN_GRID = "//div[contains (@class , 'vpo0')]";
	/**
	 * NDC_WK_VALUE_IN_GRID
	 */
	public static final String NDC_WK_VALUE_IN_GRID = "//div[contains (@class , 'ndcWk0')]";
	/**
	 * NDC_WK_VALUE_INPUT_IN_GRID
	 */
	public static final String NDC_WK_VALUE_INPUT_IN_GRID = "//div[contains (@class , 'ndcWk0')]/input";
	/**
	 * CUSTY_FTY_ID_VALUE_IN_GRID
	 */
	public static final String CUSTY_FTY_ID_VALUE_IN_GRID = "//div[contains (@class , 'custFtyId0')]";
	/**
	 * FABRIC_CODE_VALUE_IN_GRID
	 */
	public static final String FABRIC_CODE_VALUE_IN_GRID = "//div[contains (@class , 'fabricCode0')]";
	/**
	 * FIBER_CONTENT_VALUE_IN_GRID
	 */
	public static final String FIBER_CONTENT_VALUE_IN_GRID = "//div[contains (@class , 'fiberContent0')]";
	/**
	 * FABRIC_MILL_VALUE_IN_GRID
	 */
	public static final String FABRIC_MILL_VALUE_IN_GRID = "//div[contains (@class , 'fabricMill0')] ";
	/**
	 * COMMENT_VALUE_IN_GRID
	 */
	public static final String COMMENT_VALUE_IN_GRID = "//div[contains (@class , 'comment0')]";
	/**
	 * TECH_PACK_REC_DATE_STR_VALUE_IN_GRID
	 */
	public static final String TECH_PACK_REC_DATE_STR_VALUE_IN_GRID = "//div[contains (@class , 'techPackReceivedDateStr0')]";
	/**
	 * KNITTING_MACHINES_VALUE_IN_GRID
	 */
	public static final String KNITTING_MACHINES_VALUE_IN_GRID = "//div[contains (@class , 'knittingMachines0')]";
	/**
	 * GARMENT_TREATMENT_PCS_VALUE_IN_GRID
	 */
	public static final String GARMENT_TREATMENT_PCS_VALUE_IN_GRID = "//div[contains (@class , 'garmentTreatmentPcs0')]";
	/**
	 * PACKAGING_READY_DATE_STR_VALUE_IN_GRID
	 */
	public static final String PACKAGING_READY_DATE_STR_VALUE_IN_GRID = "//div[contains (@class , 'packagingReadyDateStr0')]";
	/**
	 * FABRIC_PP_DATE_VALUE_IN_GRID
	 */
	public static final String FABRIC_PP_DATE_VALUE_IN_GRID = "//div[contains (@class , 'fabricPpDateStr0')]";
	/**
	 * CPO_ACC_DATE_BY_VENDOR_VALUE_IN_GRID
	 */
	public static final String CPO_ACC_DATE_BY_VENDOR_VALUE_IN_GRID = "//div[contains (@class , 'cpoAccDateByVendorStr0')]";
	/**
	 * DIST_CHANNEL_VALUE_IN_GRID
	 */
	public static final String DIST_CHANNEL_VALUE_IN_GRID = "//div[contains (@class , 'distChannel0')]";
	/**
	 * SAMPLE_MERCH_ETA_VALUE_IN_GRID
	 */
	public static final String SAMPLE_MERCH_ETA_VALUE_IN_GRID = "//div[contains (@class , 'sampleMerchEtaStr0')]";
	/**
	 * SAMPLE_FLOOR_SET_ETA_VALUE_IN_GRID
	 */
	public static final String SAMPLE_FLOOR_SET_ETA_VALUE_IN_GRID = "//div[contains (@class , 'sampleFloorSetEtaStr0')]";
	/**
	 * SAMPLE_DCOM_ETA_VALUE_IN_GRID
	 */
	public static final String SAMPLE_DCOM_ETA_VALUE_IN_GRID = "//div[contains (@class , 'sampleDcomEtaStr0')]";
	/**
	 * SAMPLE_MAILER_ETA_VALUE_IN_GRID
	 */
	public static final String SAMPLE_MAILER_ETA_VALUE_IN_GRID = "//div[contains (@class , 'sampleMailerEtaStr0')]";
	/**
	 * PHOTO_MERCHANT_SAMPLE_SEND_DATE_VALUE_IN_GRID
	 */
	public static final String PHOTO_MERCHANT_SAMPLE_SEND_DATE_VALUE_IN_GRID = "//div[contains (@class , 'photoMerchantSampleSendDateStr0')]";
	/**
	 * PHOTO_MERCHANT_SAMPLE_ETA_DATE_VALUE_IN_GRID
	 */
	public static final String PHOTO_MERCHANT_SAMPLE_ETA_DATE_VALUE_IN_GRID = "//div[contains (@class , 'photoMerchantSampleEtaDateStr0')]";
	/**
	 * MARKETING_SAMPLE_SEND_DATE_VALUE_IN_GRID
	 */
	public static final String MARKETING_SAMPLE_SEND_DATE_VALUE_IN_GRID = "//div[contains (@class , 'marketingSampleSendDateStr0')]";
	/**
	 * MARKETING_SAMPLE_ETA_DATE_VALUE_IN_GRID
	 */
	public static final String MARKETING_SAMPLE_ETA_DATE_VALUE_IN_GRID = "//div[contains (@class , 'marketingSampleEtaDateStr0')]";
	/**
	 * MARKETING_SAMPLE_COMMENT_VALUE_IN_GRID
	 */
	public static final String MARKETING_SAMPLE_COMMENT_VALUE_IN_GRID = "//div[contains (@class , 'marketingSampleComment0')]";
	/**
	 * VISUAL_SAMPLE_SEND_DATE_VALUE_IN_GRID
	 */
	public static final String VISUAL_SAMPLE_SEND_DATE_VALUE_IN_GRID = "//div[contains (@class , 'visualSampleSendDateStr0')]";
	/**
	 * VISUAL_SAMPLE_ETA_DATE_VALUE_IN_GRID
	 */
	public static final String VISUAL_SAMPLE_ETA_DATE_VALUE_IN_GRID = "//div[contains (@class , 'visualSampleEtaDateStr0')]";
	/**
	 * VISUAL_SAMPLE_COMMENT_VALUE_IN_GRID
	 */
	public static final String VISUAL_SAMPLE_COMMENT_VALUE_IN_GRID = "//div[contains (@class , 'visualSampleComment0')]";
	/**
	 * COPYRIGHT_SAMPLE_SEND_DATE_VALUE_IN_GRID
	 */
	public static final String COPYRIGHT_SAMPLE_SEND_DATE_VALUE_IN_GRID = "//div[contains (@class , 'copyrightSampleSendDateStr0')]";
	/**
	 * COPYRIGHT_SAMPLE_ETA_DATE_VALUE_IN_GRID
	 */
	public static final String COPYRIGHT_SAMPLE_ETA_DATE_VALUE_IN_GRID = "//div[contains (@class , 'copyrightSampleEtaDateStr0')]";
	/**
	 * ADDITIONAL_BULK_LOT_APPROVE_VALUE_IN_GRID
	 */
	public static final String ADDITIONAL_BULK_LOT_APPROVE_VALUE_IN_GRID = "//div[contains (@class , 'additionalBulkLotApproveStr0')]";
	/**
	 * FABRIC_COMMENT_VALUE_IN_GRID
	 */
	public static final String FABRIC_COMMENT_VALUE_IN_GRID = "//div[contains (@class , 'fabricComment0')]";
	/**
	 * TOP_SAMPLE_ETA_DATE_VALUE_IN_GRID
	 */
	public static final String TOP_SAMPLE_ETA_DATE_VALUE_IN_GRID = "//div[contains (@class , 'topSampleEtaDateStr0')]";
	/**
	 * VENDOR_REVISED_DATE_VALUE_IN_GRID
	 */
	public static final String VENDOR_REVISED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'VendorRevisedDate0')]";
	/**
	 * OC_DATE_REVISED_DATE_VALUE_IN_GRID
	 */
	public static final String OC_DATE_REVISED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'DateRevisedDate0')]";
	/**
	 * OC_DATE_ACTUAL_DATE_VALUE_IN_GRID
	 */
	public static final String OC_DATE_ACTUAL_DATE_VALUE_IN_GRID = "//div[contains (@class , 'OC Date / Commit DateActualDate0')]";
	/**
	 * CPO_DATE_PLANNED_DATE_VALUE_IN_GRID
	 */
	public static final String CPO_DATE_PLANNED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'CPO DatePlannedDate0')]";
	/**
	 * CPO_DATE_REVISED_DATE_VALUE_IN_GRID
	 */
	public static final String CPO_DATE_REVISED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'CPO DateRevisedDate0')]";
	/**
	 * CPO_DATE_ACTUAL_DATE_VALUE_IN_GRID
	 */
	public static final String CPO_DATE_ACTUAL_DATE_VALUE_IN_GRID = "//div[contains (@class , 'CPO DateActualDate0')]";
	/**
	 * SIZE_BREAKDOWN_APPROVAL_REVISED_DATE_VALUE_IN_GRID
	 */
	public static final String SIZE_BREAKDOWN_APPROVAL_REVISED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Size Breakdown ApprovalRevisedDate0')]";
	/**
	 * SIZE_BREAKDOWN_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID
	 */
	public static final String SIZE_BREAKDOWN_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Size Breakdown ApprovalActualDate0')]";
	/**
	 * TRIM_APPROVAL_REVISED_DATE_VALUE_IN_GRID
	 */
	public static final String TRIM_APPROVAL_REVISED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Trim ApprovalRevisedDate0')]";
	/**
	 * TRIM_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID
	 */
	public static final String TRIM_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Trim ApprovalActualDate0')]";
	/**
	 * LAB_DIP_SUBMIT_REVISED_DATE_VALUE_IN_GRID
	 */
	public static final String LAB_DIP_SUBMIT_REVISED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Lab Dip/ Strike Off / Knitdown Submit DateRevisedDate0')]";
	/**
	 * LAB_DIP_SUBMIT_ACTUAL_DATE_VALUE_IN_GRID
	 */
	public static final String LAB_DIP_SUBMIT_ACTUAL_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Lab Dip/ Strike Off / Knitdown Submit DateActualDate0')]";
	/**
	 * BULKLOT_APPROVAL_REVISED_DATE_VALUE_IN_GRID
	 */
	public static final String BULKLOT_APPROVAL_REVISED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Bulk Lot ApprovalRevisedDate0')]";
	/**
	 * BULKLOT_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID
	 */
	public static final String BULKLOT_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Bulk Lot ApprovalActualDate0')]";
	/**
	 * COLORSHADE_BAND_APPROVAL_REVISED_DATE_VALUE_IN_GRID
	 */
	public static final String COLORSHADE_BAND_APPROVAL_REVISED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Color Shade Band ApprovalRevisedDate0')]";
	/**
	 * COLORSHADE_BAND_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID
	 */
	public static final String COLORSHADE_BAND_APPROVAL_ACTUAL_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Color Shade Band ApprovalActualDate0')]";
	/**
	 * WASHING_ENDDATE_REVISED_VALUE_IN_GRID
	 */
	public static final String WASHING_ENDDATE_REVISED_VALUE_IN_GRID = "//div[contains (@class , 'Washing End DateRevisedDate0')]";
	/**
	 * WASHING_ENDDATE_ACTUAL_VALUE_IN_GRID
	 */
	public static final String WASHING_ENDDATE_ACTUAL_VALUE_IN_GRID = "//div[contains (@class , 'Washing End DateActualDate0')]";
	/**
	 * BULK_FABRIC_SUBMIT_REVISED_VALUE_IN_GRID
	 */
	public static final String BULK_FABRIC_SUBMIT_REVISED_VALUE_IN_GRID = "//div[contains (@class , 'Bulk Fabric Submit DateRevisedDate0')]";
	/**
	 * BULK_FABRIC_SUBMIT_ACTUAL_VALUE_IN_GRID
	 */
	public static final String BULK_FABRIC_SUBMIT_ACTUAL_VALUE_IN_GRID = "//div[contains (@class , 'Bulk Fabric Submit DateActualDate0')]";
	/**
	 * FABRICYARN_REVISED_DATE_VALUE_IN_GRID
	 */
	public static final String FABRICYARN_XMILL_REVISED_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Fabric / Yarn Ex-Mill DateRevisedDate0')]";
	/**
	 * FABRICYARN_ACTUAL_DATE_VALUE_IN_GRID
	 */
	public static final String FABRICYARN_XMILL_ACTUAL_DATE_VALUE_IN_GRID = "//div[contains (@class , 'Fabric / Yarn Ex-Mill DateActualDate0')]";
}
