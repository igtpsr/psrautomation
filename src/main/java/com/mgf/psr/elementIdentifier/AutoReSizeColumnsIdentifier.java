package com.mgf.psr.elementIdentifier;

public interface AutoReSizeColumnsIdentifier {

	/**
	 * AUTO_RESIZE_COLUMNS_BUTTON
	 */
	public static final String AUTO_RESIZE_COLUMNS_BUTTON = "//button[text()='Auto Resize Columns']";
}
