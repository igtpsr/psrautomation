package com.mgf.psr.elementIdentifier;

public interface EventModelPageIdentifier {

	/**
	 * NEW_BTN.
	 */
	public static final String NEW_BTN = "//button[contains(text(),'New')]";

	/**
	 * MODELNAME_TXTFIELD.
	 */
	public static final String MODELNAME_TXTFIELD = "//input[@name='modelName']";

	/**
	 * ADD_NEW_ROW_FIRST_BTN.
	 */
	public static final String ADD_NEW_ROW_FIRST_BTN = "//div[@id='addNewRowBtnMatch']";

	/**
	 * EVENTMODEL_UNIQUEID.
	 */
	public static final String EVENTMODEL_UNIQUEID = "//jqxgrid[@class= 'ng-star-inserted']/div";

	/**
	 * MATCH_CASE_ROW.
	 */
	public static final String MATCH_CASE_ROW = "//*[@id='row{0}{1}']/div[1]";

	/**
	 * PLEASECHOOSE_DRPDOWN.
	 */
	public static final String PLEASECHOOSE_DRPDOWN = "//div[@role='combobox']";

	/**
	 * SELECTMATCHCASE.
	 */
	public static final String SELECT_MATCH_CASE = "//span[contains(text(), '{0}')]";

	/**
	 * MATCH_FIELD_ROW.
	 */
	public static final String MATCH_FIELD_ROW = "//*[@id='row{0}{1}']/div[2]";

	/**
	 * MATCHFIELD_VALUE.
	 */
	public static final String MATCHFIELD_VALUE = "//input[@type='textarea']";

	/**
	 * MATCHFIELD_VALUE_TXTFIELD.
	 */
	public static final String MATCHFIELD_VALUE_TXTFIELD = "//input[@type='textarea']";

	/**
	 * ADD_NEW_ROW_SECOND_BTN.
	 */
	public static final String ADD_NEW_ROW_SECOND_BTN = "//h4[text()='Event Model Details']/following-sibling::div//div[@id='addNewRowBtn']";

	/**
	 * EVENTCODE_POPUP.
	 */

	public static String EVENTCODE_POPUP = "//span[@onclick='showEventModelPopup({0})']";

	/**
	 * EVENTCODE_SEARCHFIELD.
	 */
	public static String EVENTCODE_SEARCHFIELD = "//input[@placeholder='Search']";

	/**
	 * SELECT_EVENTCODE.
	 */
	public static String SELECT_EVENTCODE = "//a[contains(text(),'{0}')]";

	/**
	 * EVENT_DETAILS_JQXID.
	 */

	public static String EVENT_DETAILS_JQXID = "(//div//div//jqxgrid/div)[2]";

	/**
	 * TRIGGER_DAYS_ROW.
	 */
	public static String TRIGGER_DAYS_ROW = "//*[@id='row{0}{1}']/div[4]";

	/**
	 * TRIGGER_EVENT_ROW.
	 */
	public static String TRIGGER_EVENT_ROW = "//*[@id='row{0}{1}']/div[3]";

	/**
	 * TRIGGERDAYS_TXTFIELD.
	 */
	public static String TRIGGER_TXTFIELD = "//input[@type='textbox']";

	/**
	 * SAVE_BTN.
	 */
	public static String SAVE_BTN = "//i[contains(text(),' save ')]";

	/**
	 * SELECT_OPERATOR.
	 */
	public static String SELECT_OPERATOR = "//select[@Id='modeloperator']";

	/**
	 * SEARCH_MODEL_NAME.
	 */
	public static String SEARCH_MODEL_NAME = "//input[@Id='modelName']";

	/**
	 * SEARCH_BTN.
	 */
	public static String MODEL_SEARCH_BTN = "//button[contains(text(),'Search')]";

	/**
	 * CREATED_MODEL_NAMES.
	 */
	public static String CREATED_MODEL_NAMES = "//div[@class='event-code-alignment' and text() = '{0}']";

	/**
	 * UPDATE_MODEL_ICON.
	 */
	public static String UPDATE_MODEL_ICON = "(//i[@class='fa fa-edit'])[1]";

	/**
	 * UPDATE_MODEL_JQXID.
	 */
	public static String UPDATE_MODEL_JQXID = "//div[@class='table-container']/div/jqxgrid/div";

	/**
	 * TOTAL_ROWS_UPDATE_MODEL_DETAILS_GRID.
	 */
	public static String TOTAL_ROWS_UPDATE_MODEL_DETAILS_GRID = "//*[@id='contenttable{0}']/div[@role='row']/div/div/span/i[@class='fa fa-search']";

	/**
	 * ECODE_DISPLAYED.
	 */
	public static String ECODE_DISPLAYED = "//span[contains(text(),'{0}')]";

	/**
	 * UPDATE_MODEL_ICON.
	 */
	public static String COPY_MODEL_ICON = "(//i[@class='fa fa-copy'])[1]";

	/**
	 * DISABLE_MODEL_CHECK_BOX_STATUS.
	 */
	public static String DISABLE_MODEL_CHECK_BOX_STATUS = "//div[contains(@class,'jqx-widget jqx-widget-white jqx-checkbox jqx-checkbox-white')]//span";

	/**
	 * DISABLE_MODEL_CHECK_BOX.
	 */
	public static String DISABLE_MODEL_CHECK_BOX = "//div[contains(@class,'jqx-checkbox-default jqx-checkbox-default-white')]/div";

	/**
	 * DELETE_EVENT_MODEL_BTN.
	 */
	public static String DELETE_EVENT_MODEL_BTN = "//button[@class='icn-btn']/i[contains(text(),'delete')]";

	/**
	 * TOASTER_MSG.
	 */
	public static String TOASTER_MSG = "//div[@id='toastr-container']/div/div/span";

	/**
	 * DISABLE_EVENT_CHECK_BOX.
	 */
	public static String DISABLE_EVENT_CHECK_BOX = "(//div[contains(@class,'jqx-checkbox-default jqx-checkbox-default-white')]/div)[2]";

	/**
	 * DISABLE_EVENT_CHECK_BOX_STATUS.
	 */
	public static String DISABLE_EVENT_CHECK_BOX_STATUS = "(//div[contains(@class,'jqx-widget jqx-widget-white jqx-checkbox jqx-checkbox-white')]//span)[2]";

	/**
	 * REMOVE_EVENT_ROW.
	 */
	public static String REMOVE_EVENT_ROW = "//*[@id='row{0}{1}']/div[6]";

	/**
	 * EMPTY_GRID.
	 */
	public static String EMPTY_GRID = "//span[text()='No data to display']";

	/**
	 * EVENT_MODEL_DETAILS_JQXID.
	 */

	public static String EVENT_MODEL_DETAILS_JQXID = "//div//div//jqxgrid/div";
}
