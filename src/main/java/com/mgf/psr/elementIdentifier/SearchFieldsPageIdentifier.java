package com.mgf.psr.elementIdentifier;

public interface SearchFieldsPageIdentifier {

	/**
	 * VPO_DROPDOWN.
	 */
	public static String VPO_DROPDOWN = "//select[@id='vpoSelect']";
	
	/**
	 * VPO_INPUT.
	 */
	public static String VPO_INPUT = "//input[@id='vpo']";
	
	
	/**
	 * FACTORY_ID_DROPDOWN.
	 */
	public static String FACTORY_ID_DROPDOWN = "//select[@id='factoryIdSelect']";
	
	/**
	 * FACTORY_ID_INPUT.
	 */
	public static String FACTORY_ID_INPUT = "//input[@id='factoryId']";
	
	/**
	 * STYLE_DROPDOWN.
	 */
	public static String STYLE_DROPDOWN = "//select[@id='styleSelect']";
	
	/**
	 * STYLE_INPUT.
	 */
	public static String STYLE_INPUT = "//input[@id='style']";
	
	/**
	 * NDC_DATE_DROPDOWN.
	 */
	public static String NDC_DATE_DROPDOWN = "//select[@id='ndcdateSelect']";
	
	/**
	 * COLOR_WASH_NAME_DROPDOWN.
	 */
	public static String COLOR_WASH_NAME_DROPDOWN = "//select[@id='colorWashNameSelect']";
	
	/**
	 * COLOR_WASH_NAME_INPUT.
	 */
	public static String COLOR_WASH_NAME_INPUT = "//input[@id='colorWashName']";
	
	/**
	 * VPO_STATUS_DROPDOWN.
	 */
	public static String VPO_STATUS_DROPDOWN = "//select[@id='vpoStatusSelect']";
	
	/**
	 * VPO_STATUS_INPUT.
	 */
	public static String VPO_STATUS_INPUT = "//input[@id='vpoStatus']";
	
	/**
	 * CPO_COLOR_DESC_DROPDOWN.
	 */
	public static String CPO_COLOR_DESC_DROPDOWN = "//select[@id='cpoColorDescSelect']";
	
	/**
	 * CPO_COLOR_DESC_INPUT.
	 */
	public static String CPO_COLOR_DESC_INPUT = "//input[@id='cpoColorDesc']";
	
	/**
	 * VENDOR_ID_DROPDOWN.
	 */
	public static String VENDOR_ID_DROPDOWN = "//select[@id='vendorIdSelect']";
	
	/**
	 * VENDOR_ID_INPUT.
	 */
	public static String VENDOR_ID_INPUT = "//input[@id='vendorId']";
	
	/**
	 * FACTORY_NAME_DROPDOWN.
	 */
	public static String FACTORY_NAME_DROPDOWN = "//select[@id='factoryNameSelect']";
	
	/**
	 * FACTORY_NAME_INPUT.
	 */
	public static String FACTORY_NAME_INPUT = "//input[@id='factoryName']";
	
	/**
	 * STYLE_DESC_DROPDOWN.
	 */
	public static String STYLE_DESC_DROPDOWN = "//select[@id='styleDescSelect']";
	
	/**
	 * STYLE_DESC_INPUT.
	 */
	public static String STYLE_DESC_INPUT = "//input[@id='styleDescription']";
	
	/**
	 * SEASON_DROPDOWN.
	 */
	public static String SEASON_DROPDOWN = "//select[@id='seasonSelect']";
	
	/**
	 * SEASON_INPUT.
	 */
	public static String SEASON_INPUT = "//input[@id='season']";
	
	/**
	 * VPO_NLT_GAC_DATE_DROPDOWN.
	 */
	public static String VPO_NLT_GAC_DATE_DROPDOWN = "//select[@id='vpoNltGacDateSelect']";
	
	/**
	 * COUNTRY_ORIGIN_DROPDOWN.
	 */
	public static String COUNTRY_ORIGIN_DROPDOWN = "//select[@id='countryOriginSelect']";
	
	/**
	 * COUNTRY_ORIGIN_INPUT.
	 */
	public static String COUNTRY_ORIGIN_INPUT = "//input[@id='countryOrigin']";
	
	/**
	 * VPO_LINE_STATUS_DROPDOWN.
	 */
	public static String VPO_LINE_STATUS_DROPDOWN = "//select[@id='vpoLineStatusSelect']";
	
	/**
	 * VPO_LINE_STATUS_INPUT.
	 */
	public static String VPO_LINE_STATUS_INPUT = "//input[@id='vpoLineStatus']";
	
	/**
	 * CPO_DEPT_DROPDOWN.
	 */
	public static String CPO_DEPT_DROPDOWN = "//select[@id='cpoDeptSelect']";
	
	/**
	 * CPO_DEPT_INPUT.
	 */
	public static String CPO_DEPT_INPUT = "//input[@id='cpoDept']";
	
	/**
	 * ORDER_COMMIT_DROPDOWN.
	 */
	public static String ORDER_COMMIT_DROPDOWN = "//select[@id='ocCommitSelect']";
	
	/**
	 * ORDER_COMMIT_INPUT.
	 */
	public static String ORDER_COMMIT_INPUT = "//input[@id='ocCommit']";
	
	/**
	 * BRAND_DROPDOWN.
	 */
	public static String BRAND_DROPDOWN = "//select[@id='brandSelect']";
	
	/**
	 * BRAND_INPUT.
	 */
	public static String BRAND_INPUT = "//input[@id='brand']";
	
	/**
	 * PRODUCTION_OFFICE_DROPDOWN.
	 */
	public static String PRODUCTION_OFFICE_DROPDOWN = "//select[@id='prodOfficeSelect']";
	
	/**
	 * PRODUCTION_OFFICE_INPUT.
	 */
	public static String PRODUCTION_OFFICE_INPUT = "//input[@id='productionOffice']";
	
	/**
	 * DEPT_DROPDOWN.
	 */
	public static String DEPT_DROPDOWN = "//select[@id='deptSelect']";
	
	/**
	 * DEPT_INPUT.
	 */
	public static String DEPT_INPUT = "//input[@id='dept']";
	
	/**
	 * FPO_FTY_GAC_DATE_DROPDOWN.
	 */
	public static String FPO_FTY_GAC_DATE_DROPDOWN = "//select[@id='vpoFtyGacDateSelect']";
	
	/**
	 * NDC_WK_DROPDOWN.
	 */
	public static String NDC_WK_DROPDOWN = "//select[@id='ndcWkSelect']";
	
	/**
	 * NDC_WK_INPUT.
	 */
	public static String NDC_WK_INPUT = "//input[@id='ndcWk']";
	
	/**
	 * CATEGORY_DROPDOWN.
	 */
	public static String CATEGORY_DROPDOWN = "//select[@id='categorySelect']";
	
	/**
	 * CATEGORY_INPUT.
	 */
	public static String CATEGORY_INPUT = "//input[@id='category']";
	
	/**
	 * INIT_FLOW_DROPDOWN.
	 */
	public static String INIT_FLOW_DROPDOWN = "//select[@id='initFlowSelect']";
	
	/**
	 * INIT_FLOW_INPUT.
	 */
	public static String INIT_FLOW_INPUT = "//input[@id='initFlow']";
	
	/**
	 * NDC_DATE_CALENDAR_ICON.
	 */
	public static String NDC_DATE_CALENDAR_ICON = "//my-date-picker[@name='ndcdate']//button[@aria-label='Open Calendar']";
	
	/**
	 * SELECT_DATE.
	 */
	public static String SELECT_DATE = "//td[@aria-label='Select day{0}']";
	
	/**
	 * SEARCH_BUTTON.
	 */
	public static String SEARCH_BUTTON = "//button[text()='Search']";
	
	/**
	 * SEARCHED_VPO.
	 */
	public static String SEARCHED_VPO = "//div[contains(@class, 'vpo') and not(contains(@class, 'Date'))]";
	
	/**
	 * SEARCHED_STYLE.
	 */
	public static String SEARCHED_STYLE = "//div[contains(@class, 'style') and not(contains(@class, 'Desc'))]";
	
	/**
	 * SEARCHED_OC_COMMIT.
	 */
	public static String SEARCHED_OC_COMMIT = "//div[contains(@class, 'ocCommit')]";
	
	/**
	 * SEARCHED_PRODUCTION_OFFICE.
	 */
	public static String SEARCHED_PRODUCTION_OFFICE = "//div[contains(@class, 'prodOffice')]";
	
	/**
	 * SELECT_COLUMN_DROPDOWN.
	 */
	public static String SELECT_COLUMN_DROPDOWN = "//ng-select[@id='dropdownscroll']";
	
	/**
	 * COLUMN_OPTION.
	 */
	public static String COLUMN_OPTION = "//div[text()='{0}']";
	
}