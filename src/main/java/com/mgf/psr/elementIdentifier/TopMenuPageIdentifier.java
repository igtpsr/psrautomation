package com.mgf.psr.elementIdentifier;

public interface TopMenuPageIdentifier {

	/**
	 * LOGOUT_BTN.
	 */
	public static final String LOGOUT_LINK = "//span[text()='Logout']";
}
