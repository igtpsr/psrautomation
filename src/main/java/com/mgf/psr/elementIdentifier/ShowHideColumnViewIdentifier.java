package com.mgf.psr.elementIdentifier;

public interface ShowHideColumnViewIdentifier {
	
	/**
	 * COLUMN_NAMES_LIST_IN_VIEW_FORM
	 */
	public static final String COLUMN_NAMES_LIST_IN_VIEW_FORM = "//*[text()='{0}']";
	/**
	 * VIEW_NAME_IN_DROP_DOWN
	 */
	public static final String VIEW_NAME_IN_DROP_DOWN = "//div[text()='{0}']";
	/**
	 * AVAILABLE_VIEW_DROP_DOWN
	 */
	public static final String AVAILABLE_VIEW_DROP_DOWN = "//div[@class='ui-select-match ng-star-inserted']//Span"; 
	/**
	 * SHOW_HIDE_COLUMN_BUTTON
	 */
	public static final String SHOW_HIDE_COLUMN_BUTTON = "//button[text()='Show Hide Column']";
	/**
	 * LOOKING_FOR_COLUMN_INPUT
	 */
	public static final String LOOKING_FOR_COLUMN_INPUT = "//input[@placeholder='Looking for']";
	/**
	 * VIEW_NAME_INPUT
	 */
	public static final String VIEW_NAME_INPUT = "//*[@id='queryName']";
	/**
	 * DELETE_VIEW_BUTTON
	 */
	public static final String DELETE_VIEW_BUTTON = "//button[text()='Delete']";
	/**
	 * UPDATE_VIEW_BUTTON
	 */
	public static final String UPDATE_VIEW_BUTTON = "//button[text()='Update']";
	/**
	 * SAVE_AS_VIEW_BUTTON
	 */
	public static final String SAVE_AS_VIEW_BUTTON = "//button[text()='Save As']";
}