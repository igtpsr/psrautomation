package com.mgf.psr.elementIdentifier;

public interface RoleProfileIdentifier {
	/**
	 * DASHBOARD_LINK.
	 */
	public static final String DASHBOARD_LINK = "//li[@class='icon-dv']";
	/**
	 * ROLEPROFILE_LINK
	 */
	public static final String ROLE_PROFILE_LINK = "//*[@id='container']/app-sidebar/div/nav/ul/li[5]/a/span[1]/i";
	/**
	 * NEW_BUTTON_IN_ROLEPOFILE
	 */
	public static final String NEW_BUTTON_IN_ROLEPROFILE = "//button[contains(text(),' NEW')]";
	/**
	 * ROLE_NAME_INPUT
	 */
	public static final String ROLE_NAME_INPUT = "//input[@name='roleName']";
	/**
	 * ROLE_DESC_INPUT
	 */
	public static final String ROLE_DESC_INPUT = "//textarea[@name='description']";
	/**
	 * ROLE_APP_INPUT_SELECTION
	 */
	public static final String ROLE_APP_INPUT_SELECTION = "//select[@name='appName']";
	/**
	 * ROLE_APP_INPUT_VALUE
	 */
	public static final String ROLE_APP_INPUT_VALUE = "//div[text()='{0}']";
	/**
	 * ROLE_PROFILE_SAVE_BUTTON
	 */
	public static final String ROLE_PROFILE_SAVE_BUTTON = "//button[contains(text(),'SAVE')]";
	/**
	 * ROLE_NAME_SEARCH_FIELD_INPUT
	 */
	public static final String ROLE_NAME_SEARCH_FIELD_INPUT = "//select[@name='roleOperator']/following-sibling::input";
	/**
	 * ROLE_SEARCH_BUTTON
	 */
	public static final String ROLE_SEARCH_BUTTON = "//button[contains(text(),'Search')]";
	/**
	 * ROLE_DETAILS_ID
	 */
	public static final String ROLE_DETAILS_ID = "//div[@class='jqx-grid-main-container']/jqxgrid/div";
	/**
	 * ALL_AVAILABLE_ROLE_NAMES
	 */
	public static final String ALL_AVAILABLE_ROLE_NAMES = "//div[@id='row0{0}']/div[@columnindex='2']";
	/**
	 * SELECT_ROLE
	 */
	public static final String SELECT_ROLE = "//div[@id='row0{0}']/div/div/div";
	/**
	 * ROLENAME_SELECT_OPERATOR
	 */
	public static final String ROLENAME_SELECT_OPERATOR = "//*[@id='roleOperator']";
	/**
	 * UPDATE_ROLE_BUTTON_APP
	 */
	public static final String UPDATE_ROLE_BUTTON_APP = "//button[contains(text(),' UPDATE')]";
	/**
	 * UPDATE_ROLE_BUTTON
	 */
	public static final String UPDATE_ROLE_BUTTON = "//*[@id='container']/div/app-create-role/div/form/div[1]/button[2]";
	/**
	 * DELETE_ROLE_BUTTON
	 */
	public static final String DELETE_ROLE_BUTTON = "//button[contains(text(),' DELETE')]";
	/**
	 * DISABLE_ROLE
	 */
	public static final String DISABLE_ROLE = "//*[@id='container']/div/app-create-role/div/form/div[2]/div[2]/div[5]/div/label";
	/**
	 * ROLE_NO_DATA_TO_DISPLAY.
	 */
	public static String ROLE_NO_DATA_TO_DISPLAY = "//span[text()='No data to display']";
}
