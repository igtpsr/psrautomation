package com.mgf.psr.elementIdentifier;

public interface LeftPanelPageIdentifier {

	/**
	 * DASHBOARD_LINK.
	 */
	public static final String DASHBOARD_LINK = "//li[@class='icon-dv']";

	/**
	 * PSR_LINK.
	 */
	public static final String PSR_LINK = "//a[@routerlink='/psr']";

	/**
	 * EVENT_MODEL_LINK.
	 */
	public static final String EVENT_MODEL_LINK = "//a[@routerlink='/event/events']";
	
	/**
	 * EVENT_CODE_LINK.
	 */
	public static final String EVENT_CODE_LINK = "//a[@routerlink='/event/eventcode']";

}
