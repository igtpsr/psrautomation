package com.mgf.psr.elementIdentifier;

public interface GridActionsIdentifier {
	/**
	 * GET_VIEW_NAME
	 */
	public static final String GET_VIEW_NAME = "(//div[@class='ui-select-match ng-star-inserted']//span)[2]"; 
	/**
	 * SEARCH_BAR
	 */
	public static final String SEARCH_BAR = "//span[contains(text(),'Search Fields')]";
	/**
	 * GRID_ID
	 */
	public static final String GRID_ID= "//*[@id='container']/div/app-staticpsr/div[3]/jqxgrid/div";
	/**
	 * GRID_SAVE
	 */
	public static final String GRID_SAVE = "//button[@id='save-changes-psr']" ;
	/**
	 * EXCEL_OPERATION
	 */
	public static final String EXCEL_OPERATION = "//button[contains(text(),'Excel Operation')]"; 
	/**
	 * EXPORT_EXCEL_BUTTON
	 */
	public static final String EXPORT_EXCEL_BUTTON ="//a[contains(text(),'Export to Excel')]"; 
	/**
	 * IMPORT_EXCEL_BUTTON
	 */
	public static final String IMPORT_EXCEL_BUTTON = "//a[contains(text(),'Import to PSR')]";
	/**
	 * FILE_TRACK
	 */
	public static final String FILE_TRACK = "//a[contains(text(),'File Track')]";
	/**
	 * FILE_NAME_EXPAND
	 */
	public static final String FILE_NAME_EXPAND =  "(//mat-expansion-panel-header[@role='button'])[1]";
	/**
	 * FILE_TRACK_USER_NAME
	 */
	public static final String FILE_TRACK_USER_NAME = "(//div[@class='mat-expansion-panel-body']//b)[1]";
	/**
	 * FILE_TRACK_VISIBILITY
	 */
	public static final String FILE_TRACK_VISIBILITY ="//div//h3[text()='File Track']";
	/**
	 * IMPORT_CHOOSE_FILE_BUTTON
	 */
	public static final String IMPORT_CHOOSE_FILE_BUTTON = "//button[text()='Choose File']";
	/**
	 * IMPORT_UPLOAD_EXCEL_BUTTON
	 */
	public static final String IMPORT_UPLOAD_EXCEL_BUTTON = "//button[text()='Upload Excel']";
	
	/**
	 * SAVE_CHANGES_BUTTON
	 */
	public static final String SAVE_CHANGES_BUTTON = "//button[@id= 'save-changes-psr']";
	/**
	 * LOADING_ICON
	 */
	public static final String LOADING_ICON ="//span[text()='Loading...']";
	
	/**
	 * ACKNOWLEDGEMENT_BUTTON
	 */
	public static final String ACKNOWLEDGEMENT_BUTTON = "//button[contains(text(),'Acknowledgement')]";
	/**
	 * SUCCESS_MESSAGE
	 */
	public static final String SUCCESS_MESSAGE = "//div[text() = 'Success!']";
}
