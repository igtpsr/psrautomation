package com.mgf.psr.elementIdentifier;

public interface EventCodePageIdentifier {
	
	/**
	 * EVENT_CODE_NEW_BTN.
	 */
	
	public static final String EVENT_CODE_NEW_BTN = "//button[contains(text(),'New')]";
	
	/**
	 * EVENT_CODE_NAME_TXTFIELD.
	 */
	
	public static final String EVENT_CODE_NAME_TXTFIELD = "//input[@name='eventcode']";

	/**
	 * NEW_EVENT_CODE_TXTFIELD.
	 */
	
	public static final String NEW_EVENT_CODE_TXTFIELD = "//input[@name='eventcode']";
	
	/**
	 * NEW_EVENT_CODE_DESCRIPTION.
	 */
	
	public static final String NEW_EVENT_CODE_DESCRIPTION = "//input[@name='desc']";
	
	/**
	 * NEW_EVENT_CODE_CATEGORY.
	 */
	
	public static final String NEW_EVENT_CODE_CATEGORY = "//input[@name='eventcategory']";
	
	/**
	 * EVENT_CODE_SAVE.
	 */
	
	public static final String EVENT_CODE_SAVE = "//button[contains(text(),' Save ')]";
	
	/**
	 * EVENT_CODE_DELETE.
	 */
	
	public static final String EVENT_CODE_DELETE = "//button[contains(text(),' Delete ')]";
	
	/**
	 * EVENT_CODE_SEARCH_BTN.
	 */
	
	public static final String EVENT_CODE_SEARCH_BTN = "//button[contains(text(),'Search')]";
	
	/**
	 * EVENT_CODE_SEARCH_INPUT.
	 */
	
	public static final String EVENT_CODE_SEARCH_INPUT = "//input[@id='eventcode']";
	
	/**
	 * SELECT_OPERATOR.
	 */
	public static String SELECT_OPERATOR = "//select[@Id='modeloperator']";
	
	/**
	 * CREATED_EVENT_CODES.
	 */
	public static String CREATED_EVENT_CODES = "//div[@class='event-code-alignment']/a[text()= '{0}']";
	
	/**
	 * DELETE_EVENT_CODE.
	 */
	public static String DELETE_EVENT_CODE = "//button[contains(text(),' Delete ')]";
	
	/**
	 * UPDATE_CODE_DESCRIPTION_JQXID.
	 */
	public static String UPDATE_CODE_DESCRIPTION_JQXID = "//div[text()= '{0}']";
	
	/**
	 * TOASTER_MSG.
	 */
	public static String TOASTER_MSG = "//div[@id='toastr-container']/div/div/span";
	
}
