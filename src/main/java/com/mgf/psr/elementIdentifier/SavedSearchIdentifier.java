package com.mgf.psr.elementIdentifier;

public interface SavedSearchIdentifier {
	
	/**
	 * SAVE_SEARCH_BUTTON
	 */
	public static final String SAVE_SEARCH_BUTTON = "//button[text()='Save Search']";
	/**
	 * SAVED_SEARCH_BUTTON
	 */
	public static final String SAVED_SEARCH_BUTTON = "//button[text()='Saved Search']";
	/**
	 * REFRESH_BUTTON
	 */
	public static final String REFRESH_BUTTON = "//*[@class='fa fa-refresh']";
	/**
	 * SEARCH_FIELDS_DROP_DOWN
	 */
	public static final String SEARCH_FIELDS_DROP_DOWN = "//*[@id='mat-expansion-panel-header-0']";
}
