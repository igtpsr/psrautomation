package com.mgf.psr.elementIdentifier;

public interface ChangeTrakPageIdentifier {
	
	/**
	 * CHANGE_TRACK_BUTTON
	 */
	public static final String CHANGE_TRACK_BUTTON = "//button[text()='Change Tracking']";
	/**
	 * SINCE_DROP_DOWN
	 */
	public static final String SINCE_DROP_DOWN = "//*[@id='dropdown']/div/div[2]/span";
	/**
	 * SINCE_VALUE_IN_DROP_DOWN
	 */
	public static final String SINCE_VALUE_IN_DROP_DOWN = "//div[text()='{0}']";
	/**
	 * SEARCH_BUTTON_IN_CHANGE_TRACK
	 */
	public static final String SEARCH_BUTTON_IN_CHANGE_TRACK = "//button[text()='Search']";
	/**
	 * CHANGE_TRACK_DETAILS
	 */
	public static final String CHANGE_TRACK_DETAILS = "(//div[@class = 'mat-expansion-panel-body'])[1]/table[1]//td";
}
