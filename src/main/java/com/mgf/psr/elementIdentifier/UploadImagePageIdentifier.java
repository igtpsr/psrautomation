package com.mgf.psr.elementIdentifier;

public interface UploadImagePageIdentifier {
	/**
	 * IMAGE_UPLOAD_ICON
	 */
	public static final String IMAGE_UPLOAD_ICON = "//i[contains(@onclick, 'image{0}')] ";
}
