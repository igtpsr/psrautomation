package com.mgf.psr.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.mgf.psr.constants.TestGroupConstant;
import com.retelzy.utilities.MainUtilities;

public class Utilities {

	/**
	 * This method is used to return the screenshot name.
	 *
	 * @param screenshotName  - Screenshot name
	 * @return String - Screenshot name
	 */
	public String getScreenshotName(String screenshotName) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		return screenshotName + "_" + timestamp.getTime();
	}
	
	/**
	 * This method is used to get Test group.
	 * 
	 * @param groups groups
	 * @return test group
	 */
	public static String getTestGroup(final String[] groups) {
		String testGroup = null;
		for (int i = 0; i < groups.length; i++) {
			if (!(groups[i].matches("[0-9]+") || groups[i].equals(TestGroupConstant.REGRESSION)
					|| groups[i].equals(TestGroupConstant.SMOKE) || groups[i].equals(TestGroupConstant.SANITY)
					)) {
				testGroup = groups[i];
				break;
			}
		}
		return testGroup;
	}
	
	/**
	 * This method in used to generate random password with input length
	 * @param length length
	 * @return password password
	 */
	public static String generateRandomPassword(int length) {
      String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
      String specialCharacters = "!@#$";
      String numbers = "1234567890";
      String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
      Random random = new Random();
      char[] password = new char[length];

      password[0] = lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length()));
      password[1] = capitalCaseLetters.charAt(random.nextInt(capitalCaseLetters.length()));
      password[2] = specialCharacters.charAt(random.nextInt(specialCharacters.length()));
      password[3] = numbers.charAt(random.nextInt(numbers.length()));
   
      for(int i = 4; i< length ; i++) {
         password[i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
      }
      return password.toString().substring(1);
	}
	/**
	 * This method is used to return column names and 1st row values
	 * @param filePath
	 * @param fileName
	 * @param sheetName
	 * @return listOfColumnNameWithValues listOfColumnNameWithValues
	 * @throws IOException
	 */
	public static LinkedHashMap<String, String> readExcel(String filePath, String fileName, String sheetName) throws IOException {	
		MainUtilities.waitFor(10000);
		File file = new File(filePath + "\\" + fileName);
		FileInputStream inputStream = new FileInputStream(file);
		Workbook workBook = null;
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			workBook = new XSSFWorkbook(inputStream);
		} else if (fileExtensionName.equals(".xls")) {
			workBook = new HSSFWorkbook(inputStream);
		}
		Sheet dataSheet = workBook.getSheet(sheetName);
		int rowCount = dataSheet.getLastRowNum() - dataSheet.getFirstRowNum();
		LinkedHashMap<String, String> listOfColumnNameWithValues = new LinkedHashMap<String, String>();
		LinkedHashMap<Integer, String> listOfColumnNames = new LinkedHashMap<Integer, String>();
		for (int i = 0; i < rowCount + 1; i++) {
			Row row = dataSheet.getRow(i);
			for (int j = 0; j < row.getLastCellNum(); j++) {
				
				if (i == 0) {
					//Adding column names into list with keys.
					listOfColumnNames.put(j, row.getCell(j).getStringCellValue());
				} else if (i == 1) {
					//Adding column names along with values
					listOfColumnNameWithValues.put(listOfColumnNames.get(j), row.getCell(j).getStringCellValue());
				}	
			}
		}
		return listOfColumnNameWithValues;
	}
	/**
	 * This method is used to write data to excel
	 * @param filePath
	 * @param fileName
	 * @param sheetName
	 * @throws IOException
	 */
	public static void writeToExcel(String filePath, String fileName, String sheetName) throws IOException {
		File file = new File(filePath + fileName);
		FileInputStream inputStream = new FileInputStream(file);
		MainUtilities.waitFor(2000);
		
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		MainUtilities.waitFor(2000);
		
		HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
		MainUtilities.waitFor(2000);
		HSSFSheet sheet = workbook.getSheetAt(0);
	    Cell cell = null;
	    MainUtilities.waitFor(2000);

	    //Update the value of cell
	    cell = sheet.getRow(1).getCell(10);
	    cell.setCellValue(MainUtilities.generateRandomString(5));
	    
	    inputStream.close();
	    
	    FileOutputStream outFile = new FileOutputStream(file);
	    workbook.write(outFile);
	    outFile.close();
	    
		/*HSSFWorkbook workBook = new HSSFWorkbook(inputStream);
		MainUtilities.waitFor(2000);
		
		HSSFSheet dataSheet = workBook.getSheet(sheetName);
		MainUtilities.waitFor(2000);
		
		Cell cell2Update = dataSheet.getRow(1).getCell(10);
		MainUtilities.waitFor(2000);
		
		Log.info("new cell value : " + cell2Update);
		cell2Update.setCellValue("NDC Test");*/
	}
}
