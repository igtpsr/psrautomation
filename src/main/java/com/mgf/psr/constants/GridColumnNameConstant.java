package com.mgf.psr.constants;

public class GridColumnNameConstant {

	/**
	 * constructor.
	 */
	private GridColumnNameConstant() {
		
	}

	/**
	 * PRODUCTION_OFFICE.
	 */
	public static final String PRODUCTION_OFFICE = "Production Office";
	}