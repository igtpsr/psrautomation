package com.mgf.psr.constants;

import java.io.File;

public class UploadImageConstant {

	/**
	 * constructor.
	 */
	private UploadImageConstant() {
		
	}
	/**
	 * BLUE_TSHIRT_PATH
	 */
	public static final String BLUE_TSHIRT_PATH = System.getProperty("user.dir") + File.separator + "Images" + File.separator + "Blue_Tshirt.jpg";
	/**
	 * SUCCESS_MESSAGE
	 */
	public static final String SUCCESS_MESSAGE = "Success!";
}
