package com.mgf.psr.constants;

import java.util.Arrays;
import java.util.List;

public class EventModelConstant {

	/**
	 * constructor.
	 */
	private EventModelConstant() {

	}

	/**
	 * MODEL_NAME.
	 */
	public static final String MODEL_NAME = "ANF_SEL_DE-DEN";

	/**
	 * MATCH_CASE_LIST.
	 */
	public static final List<String> MATCH_CASE_LIST = Arrays.asList("prod_office", "brand", "category");

	/**
	 * MATCH_CASE_FIELD_VALUES.
	 */
	public static final List<String> MATCH_CASE_FIELD_VALUES = Arrays.asList("KOREA", "ANF", "DE-DEN");

	/**
	 * E000_CODE.
	 */
	public static final String E000_CODE = "E000";

	/**
	 * E001_CODE.
	 */
	public static final String E001_CODE = "E001";

	/**
	 * E002_CODE.
	 */
	public static final String E002_CODE = "E002";

	/**
	 * E003_CODE.
	 */
	public static final String E003_CODE = "E003";

	/**
	 * E004_CODE.
	 */
	public static final String E004_CODE = "E004";

	/**
	 * E005_CODE.
	 */
	public static final String E005_CODE = "E005";

	/**
	 * E006_CODE.
	 */
	public static final String E006_CODE = "E006";

	/**
	 * E007_CODE.
	 */
	public static final String E007_CODE = "E007";

	/**
	 * E008_CODE.
	 */
	public static final String E008_CODE = "E008";

	/**
	 * E011_CODE.
	 */
	public static final String E011_CODE = "E011";

	/**
	 * E012_CODE.
	 */
	public static final String E012_CODE = "E012";

	/**
	 * E013_CODE.
	 */
	public static final String E013_CODE = "E013";
	/**
	 * E014_CODE.
	 */
	public static final String E014_CODE = "E014";

	/**
	 * E015_CODE.
	 */
	public static final String E015_CODE = "E015";

	/**
	 * E016_CODE.
	 */
	public static final String E016_CODE = "E016";

	/**
	 * E017_CODE.
	 */
	public static final String E017_CODE = "E017";

	/**
	 * E018_CODE.
	 */
	public static final String E018_CODE = "E018";

	/**
	 * E020_CODE.
	 */
	public static final String E020_CODE = "E020";

	/**
	 * E021_CODE.
	 */
	public static final String E021_CODE = "E021";

	/**
	 * E022_CODE.
	 */
	public static final String E022_CODE = "E022";

	/**
	 * E023_CODE.
	 */
	public static final String E023_CODE = "E023";

	/**
	 * E024_CODE.
	 */
	public static final String E024_CODE = "E024";

	/**
	 * E025_CODE.
	 */
	public static final String E025_CODE = "E025";

	/**
	 * E026_CODE.
	 */
	public static final String E026_CODE = "E026";

	/**
	 * E027_CODE.
	 */
	public static final String E027_CODE = "E027";

	/**
	 * E028_CODE.
	 */
	public static final String E028_CODE = "E028";
	/**
	 * E030_CODE.
	 */
	public static final String E030_CODE = "E030";

	/**
	 * E031_CODE.
	 */
	public static final String E031_CODE = "E031";

	/**
	 * E032_CODE.
	 */
	public static final String E032_CODE = "E032";

	/**
	 * E033_CODE.
	 */
	public static final String E033_CODE = "E033";
	/**
	 * E034_CODE.
	 */
	public static final String E034_CODE = "E034";

	/**
	 * E040_CODE.
	 */
	public static final String E040_CODE = "E040";

	/**
	 * E041_CODE.
	 */
	public static final String E041_CODE = "E041";

	/**
	 * E042_CODE.
	 */
	public static final String E042_CODE = "E042";
	/**
	 * E043_CODE.
	 */
	public static final String E043_CODE = "E043";

	/**
	 * E044_CODE.
	 */
	public static final String E044_CODE = "E044";

	/**
	 * E045_CODE.
	 */
	public static final String E045_CODE = "E045";

	/**
	 * E049_CODE.
	 */
	public static final String E049_CODE = "E049";

	/**
	 * E050_CODE.
	 */
	public static final String E050_CODE = "E050";

	/**
	 * E051_CODE.
	 */
	public static final String E051_CODE = "E051";

	/**
	 * E052_CODE.
	 */
	public static final String E052_CODE = "E052";

	/**
	 * E053_CODE.
	 */
	public static final String E053_CODE = "E053";

	/**
	 * E056_CODE.
	 */
	public static final String E056_CODE = "E056";

	/**
	 * E057_CODE.
	 */
	public static final String E057_CODE = "E057";

	/**
	 * E058_CODE.
	 */
	public static final String E058_CODE = "E058";

	/**
	 * EVENT_CODE_LIST.
	 */
	public static final List<String> EVENT_CODE_LIST = Arrays.asList(E001_CODE, E002_CODE);
	
	/**
	 * EVENT_CODE_UPDATE_LIST.
	 */
	public static final List<String> EVENT_CODE_UPDATE_LIST = Arrays.asList(E003_CODE, E004_CODE);

	/**
	 * GAC_BY_VENDOR.
	 */
	public static final String GAC_BY_VENDOR = "Gac By Vendor";

	/**
	 * OC_DATE_OR_COMMIT_DATE.
	 */
	public static final String OC_DATE_OR_COMMIT_DATE = "OC Date / Commit Date";

	/**
	 * CPO_DATE.
	 */
	public static final String CPO_DATE = "CPO Date";

	/**
	 * SIZE_BREAKDOWN_APPROVAL.
	 */
	public static final String SIZE_BREAKDOWN_APPROVAL = "Size Breakdown Approval";

	/**
	 * FABRIC_YARN_APPROVAL.
	 */
	public static final String FABRIC_YARN_APPROVAL = "Fabric / Yarn Approval";

	/**
	 * FABRIC_BOOKING_DATE.
	 */
	public static final String FABRIC_BOOKING_DATE = "Fabric Booking Date";

	/**
	 * TRIM_APPROVAL.
	 */
	public static final String TRIM_APPROVAL = "Trim Approval";

	/**
	 * LAB_DIP_STRIKE_OFF_KNITDOWN_APPROVAL.
	 */
	public static final String LAB_DIP_STRIKE_OFF_KNITDOWN_APPROVAL = "Lab Dip/ Strike Off/ Knitdown Approval";

	/**
	 * 1ST_BULK_LOT_APPROVAL.
	 */
	public static final String FIRST_BULK_LOT_APPROVAL = "1st Bulk Lot Approval";

	/**
	 * COLOR_SHADE_BAND_APPROVAL.
	 */
	public static final String COLOR_SHADE_BAND_APPROVAL = "Color Shade Band Approval";

	/**
	 * DYEING_STANDARD.
	 */
	public static final String DYEING_STANDARD = "Dyeing Standard";

	/**
	 * WASHING_STANDARD.
	 */
	public static final String WASHING_STANDARD = "Washing Standard";

	/**
	 * FABRIC_TEST_DATE.
	 */
	public static final String FABRIC_TEST_DATE = "Fabric Test Date";

	/**
	 * WASHING_START_DATE.
	 */
	public static final String WASHING_START_DATE = "Washing Start Date";

	/**
	 * WASHING_END_DATE.
	 */
	public static final String WASHING_END_DATE = "Washing End Date";

	/**
	 * COLOR_CALL.
	 */
	public static final String COLOR_CALL = "Color Call";

	/**
	 * FABRIC_YARN_EX-MILL_DATE.
	 */
	public static final String FABRIC_YARN_EX_MILL_DATE = "Fabric / Yarn Ex-Mill Date";

	/**
	 * FABRIC_YARN_IN_FACTORY_DATE.
	 */
	public static final String FABRIC_YARN_IN_FACTORY_DATE = "Fabric / Yarn in Factory Date";

	/**
	 * FIT_SAMPLE_SENDING_DATE.
	 */
	public static final String FIT_SAMPLE_SENDING_DATE = "Fit Sample Sending Date";

	/**
	 * FIT_APPROVAL.
	 */
	public static final String FIT_APPROVAL = "Fit Approval";

	/**
	 * PP_APPROVAL.
	 */
	public static final String PP_APPROVAL = "PP Approval";

	/**
	 * TRIM_IN_FACTORY_DATE.
	 */
	public static final String TRIM_IN_FACTORY_DATE = "Trim in Factory Date";

	/**
	 * CUT_APPROVAL.
	 */
	public static final String CUT_APPROVAL = "Cut Approval";

	/**
	 * PP_SAMPLE_SENDING_DATE.
	 */
	public static final String PP_SAMPLE_SENDING_DATE = "PP Sample Sending Date";

	/**
	 * PP_MEETING_DATE.
	 */
	public static final String PP_MEETING_DATE = "PP Meeting Date";

	/**
	 * CUT_DATE_START_KNIT.
	 */
	public static final String CUT_DATE_START_KNIT = "Cut Date / Start Knit";

	/**
	 * SEW_LINK_DATE.
	 */
	public static final String SEW_LINK_DATE = "Sew / Link Date";

	/**
	 * PILOT_DATE.
	 */
	public static final String PILOT_DATE = "Pilot Date";

	/**
	 * SIZE_SET_APPROVAL.
	 */
	public static final String SIZE_SET_APPROVAL = "Size Set Approval";

	/**
	 * PRODUCTION_START.
	 */
	public static final String PRODUCTION_START = "Production Start";

	/**
	 * INLINE_INSPECTION.
	 */
	public static final String INLINE_INSPECTION = "Inline Inspection";

	/**
	 * TOP_SAMPLE_SUBMISSION.
	 */
	public static final String TOP_SAMPLE_SUBMISSION = "TOP Sample Submission";

	/**
	 * FINAL_INSPECTION.
	 */
	public static final String FINAL_INSPECTION = "Final Inspection";

	/**
	 * GARMENT_TEST.
	 */
	public static final String GARMENT_TEST = "Garment Test";

	/**
	 * START_PACKING_DATE.
	 */
	public static final String START_PACKING_DATE = "Start Packing Date";

	/**
	 * FINISH_PACKING_DATE.
	 */
	public static final String FINISH_PACKING_DATE = "Finish Packing Date";

	/**
	 * GARMENT_FINISHED.
	 */
	public static final String GARMENT_FINISHED = "Garment Finished";

	/**
	 * EX-FTY_DATE.
	 */
	public static final String EX_FTY_DATE = "Ex-Fty Date";

	/**
	 * ARTWORK_RECEIVED_DATE.
	 */
	public static final String ARTWORK_RECEIVED_DATE = "Artwork Received Date";

	/**
	 * LAB_DIP_STRIKE_OFF_KNITDOWN_SUBMIT_DATE.
	 */
	public static final String LAB_DIP_STRIKE_OFF_KNITDOWN_SUBMIT_DATE = "Lab Dip/ Strike Off / Knitdown Submit Date";

	/**
	 * BULK_FABRIC_SUBMIT_DATE.
	 */
	public static final String BULK_FABRIC_SUBMIT_DATE = "Bulk Fabric Submit Date";

	/**
	 * SIZESET_SUBMIT_DATE.
	 */
	public static final String SIZESET_SUBMIT_DATE = "Sizeset Submit Date";

	/**
	 * PHOTOSHOOT_SAMPLE_SEND_DATE.
	 */
	public static final String PHOTOSHOOT_SAMPLE_SEND_DATE = "Photoshoot Sample Send Date";

	/**
	 * ETA.
	 */
	public static final String ETA = "ETA";

}
