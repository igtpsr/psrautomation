package com.mgf.psr.constants;

public class TestGroupConstant {

	/**
	 * constructor.
	 */
	private TestGroupConstant() {

	}
	/**
	 * REGRESSION.
	 */
	public static final String REGRESSION = "Regression";
	/**
	 * SMOKE.
	 */
	public static final String SMOKE = "Smoke";
	/**
	 * SANITY.
	 */
	public static final String SANITY = "Sanity";
	/**
	 * LOGIN.
	 */
	public static final String LOGIN = "Login";
	/**
	 * DATAPROVIDER.
	 */
	public static final String DATAPROVIDER = "Data Provider";
	
	/**
	 * USERPROFILE.
	 */
	public static final String USERPROFILE = "User Profile";
	
	/**
	 * EVENTMODEL.
	 */
	public static final String EVENTMODEL = "Event Model";

	/**
	 * ROLEPROFILE
	 */
	public static final String ROLEPROFILE = "Role Profile";
	
	/**
	 * EVENTCODE.
	 */
	public static final String EVENTCODE = "Event Code";
	
	/**
	 * EXPORT_EXCEL
	 */
	public static final String EXPORT_EXCEL = "Export Excel";
	
	/**
	 * IMPORT_EXCEL
	 */
	public static final String IMPORT_EXCEL = "Import Excel";
	
	/**
	 * FILE_TRACK
	 */
	public static final String FILE_TRACK = "File Track";
	
	/**
	 * SAVE_GRID_CHANGES
	 */
	public static final String SAVE_GRID_CHANGES = "Save Grid Changes";
	
	/**
	 * ACKNOWLEDGEMENT
	 */
	public static final String ACKNOWLEDGEMENT = "Acknowledgement";	
	
	/**
	 * SEARCHFIELDS
	 */
	public static final String SEARCH_FIELDS = "Search Fields";
	
	/**
	 * UPLOAD_IMAGE
	 */
	public static final String UPLOAD_IMAGE = "Upload Image";
	/**
	 * SHOW_HIDE_COLUMN_VIEW
	 */
	public static final String SHOW_HIDE_COLUMN_VIEW = "Show Hide Column View";
	/**
	 * CHANGE_TRACK
	 */
	public static final String CHANGE_TRACK = "Change Track";
	/**
	 * AUTO_RESIZE_COLUMNS
	 */
	public static final String AUTO_RESIZE_COLUMNS = "Auto ReSize Columns";
	/**
	 * SAVED_SEARCH
	 */
	public static final String SAVED_SEARCH = "Saved Search";
}
