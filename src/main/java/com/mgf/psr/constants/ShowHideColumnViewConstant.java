package com.mgf.psr.constants;

public class ShowHideColumnViewConstant {

	/**
	 * constructor.
	 */
	private ShowHideColumnViewConstant() {
		
	}
	/**
	 * ORDER_QTY
	 */
	public static final String ORDER_QTY = "Order Qty";
}
