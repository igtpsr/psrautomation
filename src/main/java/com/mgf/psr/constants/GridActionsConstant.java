package com.mgf.psr.constants;

public class GridActionsConstant {
	/**
	 * constructor.
	 */
	private GridActionsConstant() {
		
	}
	/**
	 * VPO_NO_FOR_EXCEL_OPERATION
	 */
	public static final String VPO_NO_FOR_EXCEL_OPERATION = "2154";
	/**
	 * NDC_WK
	 */
	public static final String NDC_WK = "Ndc Wk";
	/**
	 * STYLE
	 */
	public static final String STYLE = "204";
}
