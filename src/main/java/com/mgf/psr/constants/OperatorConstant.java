package com.mgf.psr.constants;

public class OperatorConstant {

	/**
	 * constructor.
	 */
	private OperatorConstant() {
		
	}

	/**
	 * LIKE.
	 */
	public static final String LIKE = "Like";
	
	/**
	 * NOT_LIKE.
	 */
	public static final String NOT_LIKE = "Not like";
	
	/**
	 * EQUAL_TO.
	 */
	public static final String EQUAL_TO = "Equal to";

	/**
	 * NOT_EQUAL_TO.
	 */
	public static final String NOT_EQUAL_TO = "Not equal to";

	/**
	 * IN_THE_LIST.
	 */
	public static final String IN_THE_LIST = "In the list";

	/**
	 * NOT_IN_THE_LIST.
	 */
	public static final String NOT_IN_THE_LIST = "Not in the list";

	/**
	 * STARTS_WITH.
	 */
	public static final String STARTS_WITH = "Starts with";

	/**
	 * ENDS_WITH.
	 */
	public static final String ENDS_WITH = "Ends with";

	/**
	 * EQUAL_TO_NULL.
	 */
	public static final String EQUAL_TO_NULL = "Equal to null";

	/**
	 * IS_NOT_NULL.
	 */
	public static final String IS_NOT_NULL = "Is not null";
}
