package com.mgf.psr.constants;

public class EventCodeConstant {

	/**
	 * constructor.
	 */
	private EventCodeConstant() {
		
	}
	
	/**
	 * EVENT_CODE.
	 */
	public static final String EVENT_CODE = "E0120";

	/**
	 * EVENT_CODE_DESCRIPTION.
	 */
	public static final String EVENT_CODE_DESCRIPTION = "TEST DESCRIPTION";
	
	/**
	 * EVENT_CODE_CATEGORY.
	 */
	public static final String EVENT_CODE_CATEGORY = "TEST CATEGORY";
	
}
