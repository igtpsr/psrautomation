package com.mgf.psr.constants;

public class UserProfileConstant {
	
	/**
	 * constructor.
	 */
	private UserProfileConstant() {
		
	}
	/**
	 * ROLE.
	 */
	public static final String ROLE = "PTL_ADMIN";
	/**
	 * COUNTRY.
	 */
	public static final String COUNTRY = "INDIA";
	/**
	 * QUERY_NAME.
	 */
	public static final String QUERY_NAME = "EXP_HKG_DEN";
}
