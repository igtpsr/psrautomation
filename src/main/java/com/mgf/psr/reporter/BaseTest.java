package com.mgf.psr.reporter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.mgf.psr.utilities.Utilities;
import com.retelzy.constants.ApplicationConstant;
import com.retelzy.core.helper.LogHelper;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.core.testng.TestTemplate;
import com.retelzy.dataprovider.ExcelDataProviderUtils;
import com.retelzy.dataprovider.ExcelReader;
import com.retelzy.reporter.ExtentManager;


public class BaseTest extends TestTemplate {

	protected static String baseUrl;
	static final String buildName;

	static {
		if (StringUtils.isBlank(System.getProperty("baseUrl"))) {
			baseUrl = ParameterHelper.getParameterDefaultValue("baseUrl");
		} else {
			baseUrl = System.getProperty("baseUrl");
		}
	}

	static {
		String browser = StringUtils.isBlank(System.getProperty("browser"))
				? ParameterHelper.getParameterDefaultValue("browser")
				: System.getProperty("browser");
		buildName = String.valueOf(java.time.LocalDate.now()) + browser;
	}

	public com.aventstack.extentreports.ExtentTest test;
	static final File fileReport = getReportPath();
	static int totalTcCount = 0;
	static int passedTcCount = 0;
	static int failedTcCount = 0;
	static int skippedTcCount = 0;

	@BeforeMethod(alwaysRun = true)
	public void beforeMethod(Method method) {
		test = ExtentManager.startTest(method.getName());
		log.info("test execution started for.." + method.getName());
	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod(Method method, ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			Utilities utilities = new Utilities();
			String screenshotName = utilities.getScreenshotName(result.getMethod().getMethodName());
			test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " FAILED ", ExtentColor.RED));
			failedTcCount++;
			takeScreenShot(screenshotName);
			test.fail(result.getThrowable());
			test.addScreenCaptureFromPath("./FailedTcScreenshot" + File.separator + screenshotName + ".png");
			log.info("test execution ended for.." + method.getName() + " with FAIL status");
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " SKIPPED ", ExtentColor.ORANGE));
			skippedTcCount++;
			test.skip(result.getThrowable());
			log.info("test execution ended for.." + method.getName() + " with SKIP status");
		} else {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " PASSED ", ExtentColor.GREEN));
			passedTcCount++;
			log.info("test execution ended for.." + method.getName() + " with PASS status");
		}
		totalTcCount++;
		test.assignCategory(Utilities.getTestGroup(result.getMethod().getGroups()));
		test = null;
		driver.quit();
	}

	@BeforeSuite(alwaysRun = true)
	public void beforeSuite() {
		ExtentManager.getReporter(fileReport.getAbsolutePath());
	}

	@AfterSuite(alwaysRun = true)
	protected void afterSuite(final ITestContext iTestContext) {
		createSummaryReport(iTestContext);
		ExtentManager.getReporter(fileReport.getAbsolutePath()).flush();
	}

	@BeforeTest(alwaysRun = true)
	public void beforeTest() {

	}

	protected static File getReportPath() {
		return new File(System.getProperty("user.dir") + File.separator + "reports",
				ApplicationConstant.AUTOMATION_REPORT);
	}

	/**
	 * Tests that would like to run based on excel sheet can use this as their data
	 * provider. <br>
	 * Use in the @ExcelDataProviderArgs annotation on test methods.
	 *
	 * @param testMethod
	 *            testMethod
	 * @return Object[][]
	 */
	@DataProvider(name = "Excel", parallel = false)
	public Object[][] provideExcelData(final Method testMethod) {
		final String excelFile = ExcelDataProviderUtils.resolveExcelDataProviderFileName(testMethod);
		final String worksheet = ExcelDataProviderUtils.resolveExcelDataProviderWorksheet(testMethod);
		final ExcelReader excelReader = new ExcelReader(excelFile, worksheet);
		int rows = excelReader.getWorkbookSheet().getLastRowNum();
		int cols = excelReader.getWorkbookSheet().getRow(0).getLastCellNum();
		Object[][] data = new Object[rows][1];
		for (int i = 0; i < rows; i++) {
			Map<Object, Object> datamap = new HashMap<>();
			for (int j = 0; j < cols; j++) {
				datamap.put(excelReader.getWorkbookSheet().getRow(0).getCell(j).toString(),
						excelReader.getWorkbookSheet().getRow(i + 1).getCell(j).toString());

			}
			data[i][0] = datamap;
		}
		return data;
	}

	/**
	 * This method used to create summary report.
	 *
	 * @param iTestContext
	 *            iTestContext
	 */
	public void createSummaryReport(final ITestContext iTestContext) {
		try {
			final StringBuilder htmlStringBuilder = new StringBuilder();

			// Need to handle runtime
			final String browser = getBrowser();
			final String environment = getEnvironment();

			htmlStringBuilder.append("<html><head><title>PSR Automation Report Summary</title></head>");
			htmlStringBuilder.append("<body bgcolor=\"#DFE6E6\">");
			htmlStringBuilder.append("<h4>Automation Suite Execution Summary:</h4>");
			htmlStringBuilder.append("<table border=\"1\" bordercolor=\"#000000\">");
			htmlStringBuilder.append(
					"<tr bgcolor=\"#0d6c73\"><td align='center'><font color=\"#FFFFFF\">Suite</font></td><td align='center'><font color=\"#FFFFFF\">#Executed</font></td><td align='center'><font color=\"#FFFFFF\">#Passed</font></td><td align='center'><font color=\"#FFFFFF\">#Failed</font></td><td align='center'><font color=\"#FFFFFF\">#Skipped</font></td><td align='center'><font color=\"#FFFFFF\">Browser</font></td><td align='center'><font color=\"#FFFFFF\">Environment</font></td></tr>");
			htmlStringBuilder.append("<tr><td align='center'>" + iTestContext.getCurrentXmlTest().getSuite().getName()
					+ "</td><td align='center'>" + totalTcCount + "</td><td align='center'>" + passedTcCount
					+ "</td><td align='center'>" + failedTcCount + "</td><td align='center'>" + skippedTcCount
					+ "</td><td align='center'>" + browser + "</td><td align='center'>" + environment + "</td></tr>");
			htmlStringBuilder.append("</table></body></html>");
			// write html string content to a file
			writeToFile(htmlStringBuilder.toString(), ApplicationConstant.AUTOMATION_SUMMARY_REPORT);
		} catch (IOException e) {
			LogHelper.getLogger().info("Exception Caught " + e.getMessage());
		}
	}

	/**
	 * This method used to write file content to html file.
	 *
	 * @param fileContent
	 *            fileContent
	 * @param fileName
	 * @throws IOException
	 *             IOException
	 */
	public static void writeToFile(final String fileContent, final String fileName) throws IOException {
		final File file = new File(System.getProperty("user.dir") + File.separator + "reports", fileName);
		try (OutputStream outputStream = new FileOutputStream(file.getAbsoluteFile());
				Writer writer = new OutputStreamWriter(outputStream)) {
			writer.write(fileContent);
		} catch (Exception exception) {
			LogHelper.getLogger().error("Unable to write to summary report..");
		}
	}

	/**
	 * This method is used to capture screenshots on failure of test script
	 * 
	 * @param screenshotName
	 *            - Method name
	 */
	public void takeScreenShot(String screenshotName) { 
		try {
			String path = System.getProperty("user.dir") + File.separator + "reports" + File.separator
					+ "FailedTcScreenshot";
			File file = new File(path);
			if (!file.exists()) {
				file.mkdir();
			}
			File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			String destination = path + File.separator + screenshotName + ".png";
			File finalDestination = new File(destination);
			FileUtils.copyFile(source, finalDestination);
		} catch (Exception e) {
			LogHelper.getLogger().info("Exception Caught " + e.getMessage());
		}
	}

	/**
	 * This method used to get browser name on which test cases executed.
	 * 
	 * @return browser
	 */
	private String getBrowser() {
		String browser;
		if (StringUtils.isBlank(System.getProperty("browser"))) {
			browser = ParameterHelper.getParameterDefaultValue("browser");
		} else {
			browser = System.getProperty("browser");
		}
		return browser;
	}

	/**
	 * This method used to get environment name on which test cases executed.
	 * 
	 * @return environment
	 */
	private String getEnvironment() {
		String environment;
		if (StringUtils.isBlank(System.getProperty("environment"))) {
			environment = ParameterHelper.getParameterDefaultValue("environment");
		} else {
			environment = System.getProperty("environment");
		}
		return environment;
	}

}
