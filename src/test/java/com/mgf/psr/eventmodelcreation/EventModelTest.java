package com.mgf.psr.eventmodelcreation;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.mgf.psr.constants.EventModelConstant;
import com.mgf.psr.constants.OperatorConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.EventModelPage;
import com.mgf.psr.pages.LeftPanelPage;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.TopMenuPage;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class EventModelTest extends BaseLoginTest {

	private LoginPage loginPage = new LoginPage(baseUrl);
	private TopMenuPage topMenuPage;
	private LeftPanelPage leftPanelPage;
	private EventModelPage EventModelPage;
	private BaseAssertions baseAssertions = new BaseAssertions();

	@Test(groups = { TestGroupConstant.EVENTMODEL, TestGroupConstant.REGRESSION })
	public void testEventModelCreation(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event model link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventModelLink();

		MainUtilities.logInfoToReport(test, "Step 5 : Click New Button");
		EventModelPage.clickOnNewBtn();

		MainUtilities.logInfoToReport(test, "Step 6 : Enter Model Name");
		String modelName = EventModelConstant.MODEL_NAME + MainUtilities.generateRandomDigits(4);
		context.setAttribute("ModelName", modelName);
		EventModelPage.enterModelName(modelName);

		MainUtilities.logInfoToReport(test, "Step 7 : Enter Match Criteria Values");
		EventModelPage.selectMatchCase(EventModelConstant.MATCH_CASE_LIST, EventModelConstant.MATCH_CASE_FIELD_VALUES);

		MainUtilities.logInfoToReport(test, "Step 8 : enter event model details");
		EventModelPage.enterModelDetails(EventModelConstant.EVENT_CODE_LIST);

		MainUtilities.logInfoToReport(test, "Step 9 : Go to event model page by clicking on event model link");
		leftPanelPage.clickOnEventModelLink();

		MainUtilities.logInfoToReport(test, "Step 10 : Verify event model is created or no");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isModelCreated(modelName),
				"Model is created Successfully", "Model is not created");
	}

	@Test(dependsOnMethods = { "testEventModelCreation" }, groups = { TestGroupConstant.EVENTMODEL,
			TestGroupConstant.REGRESSION })
	public void testEventModelUpdate(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		final String modelName = String.valueOf(context.getAttribute("ModelName"));
		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event model link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventModelLink();

		MainUtilities.logInfoToReport(test, "Step 5 : Verify event model is available for update or no");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isModelCreated(modelName),
				"Model is available for update", "Model is not available for update");

		MainUtilities.logInfoToReport(test, "Step 6 : Update event model details");
		EventModelPage.updateModelDetails(modelName, EventModelConstant.EVENT_CODE_UPDATE_LIST);

		MainUtilities.logInfoToReport(test, "Step 7 : Verify whether event model is updated or no");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isModelUpdated(), "Model is updated",
				"Model is not updated");
	}

	@Test(dependsOnMethods = { "testEventModelUpdate" }, groups = { TestGroupConstant.EVENTMODEL,
			TestGroupConstant.REGRESSION })
	public void testEventModelCopy(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		final String modelName = String.valueOf(context.getAttribute("ModelName"));
		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event model link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventModelLink();

		MainUtilities.logInfoToReport(test, "Step 5 : Verify event model is available or no for Copy");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isModelCreated(modelName),
				"Model is available for update", "Model is not available for update");

		MainUtilities.logInfoToReport(test, "Step 6 : Enter new model name");
		EventModelPage.enterModelNameCopy(modelName);

		MainUtilities.logInfoToReport(test, "Step 7 : click on dashboard, PSR and Event Model Link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventModelLink();

		MainUtilities.logInfoToReport(test, "Step 8 : Verify event model copy is created or no");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isModelNameCopyCreated(modelName + "Copy"),
				"Copy Model is created", "Copy Model is not created");
	}

	@Test(dependsOnMethods = { "testEventModelCopy" }, groups = { TestGroupConstant.EVENTMODEL,
			TestGroupConstant.REGRESSION })
	public void testEventModelDisable(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		final String modelName = String.valueOf(context.getAttribute("ModelName"));
		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event model link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventModelLink();

		MainUtilities.logInfoToReport(test, "Step 5 : Disable Model");
		boolean status = EventModelPage.disableModel(modelName);

		MainUtilities.logInfoToReport(test, "Step 6 : verify model is disabled or no");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isModelDisabled(modelName) && status,
				"User is bale to disable the model", "Model is already disabled");
	}

	@Test(dependsOnMethods = { "testRemoveEventRow" }, groups = { TestGroupConstant.EVENTMODEL,
			TestGroupConstant.REGRESSION })
	public void testEventModelDelete(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		final String modelName = String.valueOf(context.getAttribute("ModelName"));

		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event model link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventModelLink();

		MainUtilities.logInfoToReport(test, "Step 5 : Verify event model is available or no for Delete");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isModelCreated(modelName),
				"Model is available for update", "Model is not available for update");

		MainUtilities.logInfoToReport(test, "Step 6 : Verify event model is deleted or no");
		boolean isModelDeleted = EventModelPage.isModelDeleted(modelName);

		if (isModelDeleted) {
			leftPanelPage.clickOnDashboardLink();
			leftPanelPage.clickOnPsrLink();
			leftPanelPage.clickOnEventModelLink();
			EventModelPage.selectOpeartor(OperatorConstant.EQUAL_TO);
			EventModelPage.searchModelName(modelName);
			EventModelPage.clickOnModelSearchBtn();
			MainUtilities.waitFor(2000);
			baseAssertions.booleanTrueAssertion(test, EventModelPage.isEmptyGridDisplayed(),
					"event model " + modelName + " deleted successfully",
					"Event Model " + modelName + " is not deleted");
		} else {
			baseAssertions.booleanTrueAssertion(test, true, "Model " + modelName + " is participated in autoattachment",
					"Model  " + modelName + " is participated in autoattachment but it got deleted");
		}
	}

	@Test(dependsOnMethods = { "testEventModelUpdate" }, groups = { TestGroupConstant.EVENTMODEL,
			TestGroupConstant.REGRESSION })
	public void testDisableEvent(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		final String modelName = String.valueOf(context.getAttribute("ModelName"));
		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event model link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventModelLink();

		MainUtilities.logInfoToReport(test, "Step 5 : Verify event model is available or no for Delete");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isModelCreated(modelName),
				"Model is available for disabling event", "Model is not available for disabling event");

		MainUtilities.logInfoToReport(test, "Step 5 : Verify event model is deleted or no");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isEventDisabled(), "Event is disabled",
				"Event is not disabled");
	}

	@Test(dependsOnMethods = { "testEventModelUpdate" }, groups = { TestGroupConstant.EVENTMODEL,
			TestGroupConstant.REGRESSION })
	public void testRemoveEventRow(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		final String modelName = String.valueOf(context.getAttribute("ModelName"));
		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event model link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventModelLink();

		MainUtilities.logInfoToReport(test, "Step 5 : Verify event model is available or no for Delete");
		baseAssertions.booleanTrueAssertion(test, EventModelPage.isModelCreated(modelName),
				"Model is available for update", "Model is not available for update");

		MainUtilities.logInfoToReport(test, "Step 6 : Verify event is removed or no");
		boolean isEventRemoved = EventModelPage.isEventRowRemoved(modelName);

		if (isEventRemoved) {
			baseAssertions.booleanTrueAssertion(test, EventModelPage.isEventCodeVisible(EventModelConstant.E002_CODE),
					"event is removed successfully", "Event is not removed");
		} else {
			baseAssertions.booleanTrueAssertion(test, true, "Model E002 is participated in autoattachment",
					"Event is participated in autoattachment but it got deleted");
		}
	}
}