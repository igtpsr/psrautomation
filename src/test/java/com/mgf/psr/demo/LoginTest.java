package com.mgf.psr.demo;

import org.testng.annotations.Test;

import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.TopMenuPage;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class LoginTest extends BaseLoginTest {

	private LoginPage loginPage = new LoginPage(baseUrl);
	private TopMenuPage topMenuPage;
	private BaseAssertions baseAssertions = new BaseAssertions();

	@Test(groups = { TestGroupConstant.LOGIN, TestGroupConstant.REGRESSION })
	public void testLogin() {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();
		verifyLoginPage(loginPage);

		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");
	}
}
