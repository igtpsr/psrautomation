package com.mgf.psr.demo;

import java.util.Map;
import org.testng.annotations.Test;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.reporter.BaseTest;
import com.retelzy.dataprovider.ExcelDataProviderArgs;

public class DataProviderTest extends BaseTest {

	@Test(groups = { TestGroupConstant.DATAPROVIDER,
			TestGroupConstant.REGRESSION }, dataProvider = "Excel", dataProviderClass = BaseTest.class)
	@ExcelDataProviderArgs(excelFile = "testdata/UserDetails.xlsx", worksheet = "Sheet1")
	public void testDataProvider(Map<Object, Object> input) {
		System.out.println("Input Data column 1 ==> " + input.get("UserName"));
		System.out.println("Input Data column 2 ==> " + input.get("Password"));
	}
}
