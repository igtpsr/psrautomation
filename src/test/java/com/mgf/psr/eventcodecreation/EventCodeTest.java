package com.mgf.psr.eventcodecreation;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.mgf.psr.constants.EventCodeConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.EventCodePage;
import com.mgf.psr.pages.LeftPanelPage;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.TopMenuPage;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class EventCodeTest extends BaseLoginTest  {
	
	private LoginPage loginPage = new LoginPage(baseUrl);
	private TopMenuPage topMenuPage;
	private LeftPanelPage leftPanelPage;
	private EventCodePage eventCodePage;
	private BaseAssertions baseAssertions = new BaseAssertions();

	@Test(groups = { TestGroupConstant.EVENTCODE, TestGroupConstant.REGRESSION })
	public void testEventCodeCreation(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");
		
		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event code link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventCodeLink();
		
		MainUtilities.logInfoToReport(test, "Step 5 : Click New Button");
		eventCodePage.clickOnNewBtn();
		
		MainUtilities.logInfoToReport(test, "Step 6 : Enter Event Code Name");
		String eventCodeName = EventCodeConstant.EVENT_CODE+ MainUtilities.generateRandomString(4);
		context.setAttribute("EventCodeName", eventCodeName);
		eventCodePage.enterCodeName(eventCodeName);
		
		MainUtilities.logInfoToReport(test, "Step 7 : Enter Event Code Description");
		String eventCodeDescription = EventCodeConstant.EVENT_CODE_DESCRIPTION+ MainUtilities.generateRandomString(5);
		context.setAttribute("EventCodeDescription", eventCodeDescription);
		eventCodePage.enterEventCodeDescription(eventCodeDescription);
		
		MainUtilities.logInfoToReport(test, "Step 8 : Enter Event Code Category");
		String eventCodeCategory = EventCodeConstant.EVENT_CODE_CATEGORY+ MainUtilities.generateRandomString(4);
		context.setAttribute("EventCodeCategory", eventCodeCategory);
		eventCodePage.enterEventCodeCategory(eventCodeCategory);
		
		MainUtilities.logInfoToReport(test, "Step 9 : Click Save Button");
		eventCodePage.clickOnSave();
		
		MainUtilities.logInfoToReport(test, "Step 10 : Verify event code is created or not");
		baseAssertions.booleanTrueAssertion(test, eventCodePage.isEventCodeCreated(eventCodeName),
				"Event Code is created Successfully", "Event Code is not created");
	
}
	@Test(dependsOnMethods = { "testEventCodeCreation" }, groups = { TestGroupConstant.EVENTCODE,
			TestGroupConstant.REGRESSION })
	public void testEventCodeUpdate(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		final String eventCodeName = String.valueOf(context.getAttribute("EventCodeName"));
		
		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event code link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventCodeLink();
		
		MainUtilities.logInfoToReport(test, "Step 5 : Verify event code is available for update or no");
		baseAssertions.booleanTrueAssertion(test, eventCodePage.isEventCodeCreated(eventCodeName),
				"Event Code is available for update", "Event Code is not available for update");

		MainUtilities.logInfoToReport(test, "Step 6 : Click On Created Event Code");
		eventCodePage.clickOnCreatedEventCode(eventCodeName);
		
		MainUtilities.logInfoToReport(test, "Step 7 : Enter Event Code Description");
		String eventCodeDescription = EventCodeConstant.EVENT_CODE_DESCRIPTION+ MainUtilities.generateRandomString(5);
		context.setAttribute("EventCodeDescription", eventCodeDescription);
		eventCodePage.enterEventCodeDescription(eventCodeDescription);
		
		MainUtilities.logInfoToReport(test, "Step 8 : Click Save Button");
		eventCodePage.clickOnSave();
		
		MainUtilities.logInfoToReport(test, "Step 9 : Verify event code is updated or not");
		baseAssertions.booleanTrueAssertion(test, eventCodePage.isEventCodeUpdated(eventCodeName, eventCodeDescription),
				"Event Code is updated Successfully", "Event Code is not updated");
		
	}
	
	@Test(dependsOnMethods = { "testEventCodeUpdate" }, groups = { TestGroupConstant.EVENTCODE,
			TestGroupConstant.REGRESSION })
	public void testEventCodeDelete(ITestContext context) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application");
		loginPage.openApplication();

		final String eventCodeName = String.valueOf(context.getAttribute("EventCodeName"));
		
		MainUtilities.logInfoToReport(test, "Step 2 : Login to PSR application");
		loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 3 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		MainUtilities.logInfoToReport(test, "Step 4 : Click on Dashboard Link, Psr Link and event code link");
		leftPanelPage.clickOnDashboardLink();
		leftPanelPage.clickOnPsrLink();
		leftPanelPage.clickOnEventCodeLink();
		
		MainUtilities.logInfoToReport(test, "Step 5 : Verify event code is available to delete or no");
		baseAssertions.booleanTrueAssertion(test, eventCodePage.isEventCodeCreated(eventCodeName),
				"Event Code is available to delete", "Event Code is not available to delete");

		MainUtilities.logInfoToReport(test, "Step 6 : Click On Created Event Code");
		eventCodePage.clickOnCreatedEventCode(eventCodeName);
		
		MainUtilities.logInfoToReport(test, "Step 7 : Click Delete Button");
		eventCodePage.clickOnEventCodeDeleteBtn();
		
		MainUtilities.logInfoToReport(test, "Step 8 : Click Confirm Delete Button");
		eventCodePage.confirmDelete();
		
		MainUtilities.logInfoToReport(test, "Step 9 : Verify event code is deleted or not");
		baseAssertions.booleanTrueAssertion(test, eventCodePage.isEventCodeDeleted(eventCodeName),
				"Event Code is deleted Successfully", "Event Code is not deleted");
	
     }
}
 

	
