package com.mgf.psr.savedSearch;

import org.testng.annotations.Test;

import com.mgf.psr.constants.GridActionsConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.elementIdentifier.GridActionsIdentifier;
import com.mgf.psr.elementIdentifier.SearchFieldsPageIdentifier;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.GridActionsPage;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.SavedSearchPage;
import com.mgf.psr.pages.SearchFieldsPage;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class SavedSearchTest extends BaseLoginTest {
	
	private LoginPage loginPage = new LoginPage(baseUrl);
	private SearchFieldsPage searchFieldPage;
	private SavedSearchPage savedSearchPage;
	private GridActionsPage gridActionsPage;
	
	private BaseAssertions baseAssertions = new BaseAssertions();
	
	@Test(groups = { TestGroupConstant.SAVED_SEARCH, TestGroupConstant.REGRESSION })
	public void testSavedSearch() {
		MainUtilities.logInfoToReport(test, "Step 1 : Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 : Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 : Enter Style Number");
		searchFieldPage.enterStyle(GridActionsConstant.STYLE);
		
		MainUtilities.logInfoToReport(test, "Step 3 : Click on Search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 : Click on Search Fields drop down.");
		savedSearchPage.clickOnSearchFieldsDropDown();
		
		MainUtilities.logInfoToReport(test, "Step 5 : Click on Save Search Button");
		savedSearchPage.clickOnSaveSearchButton();
				
		boolean isTestPassed = false;
		String toasterMsg = gridActionsPage.getMessageFromToaster();
		log.info("toasterMsg : " + toasterMsg);
		if (savedSearchPage.isSearchSaved(toasterMsg)) {
			
			MainUtilities.logInfoToReport(test, "Step 6 : Click on Refresh Button");
			savedSearchPage.clickOnRefreshButton();
			
			MainUtilities.logInfoToReport(test, "Step 7 : Click on Saved Search Button");
			savedSearchPage.clickOnSavedSearchButton();
			
			MainUtilities.logInfoToReport(test, "Step 8 : Verifing whether the saved search is correct or not.");
			if (savedSearchPage.isSavedSearchCorrect(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION, GridActionsConstant.STYLE, SearchFieldsPageIdentifier.VPO_INPUT, SearchFieldsPageIdentifier.STYLE_INPUT)) {
				isTestPassed = true;
			}
		}
		
		MainUtilities.logInfoToReport(test, "Step 9 : Verify whether the Saved Search is updated or not.");
		baseAssertions.booleanTrueAssertion(test, isTestPassed,
					"Saved Search updated Successfully", "Saved Search not Updated");
	}
}

