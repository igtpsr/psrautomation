package com.mgf.psr.autoResizeColumns;

import org.testng.annotations.Test;

import com.mgf.psr.constants.GridActionsConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.elementIdentifier.GridActionsIdentifier;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.AutoResizeColumnsPage;
import com.mgf.psr.pages.GridActionsPage;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.SearchFieldsPage;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class AutoResizeColumnsTest extends BaseLoginTest {
	
	private LoginPage loginPage = new LoginPage(baseUrl);
	private SearchFieldsPage searchFieldPage;
	private AutoResizeColumnsPage autoReSizeColumnsPage;
	private GridActionsPage gridActionsPage;
	
	private BaseAssertions baseAssertions = new BaseAssertions();
	
	@Test(groups = { TestGroupConstant.AUTO_RESIZE_COLUMNS, TestGroupConstant.REGRESSION })
	public void testAutoResizeColumns() {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 :  Click on Auto Resize Columns Button");
		autoReSizeColumnsPage.clickOnAutoResizeColumnsButton();
		
		MainUtilities.logInfoToReport(test, "Step 8 : Verify whether the Auto Resize Columns is updated or not.");
		baseAssertions.booleanTrueAssertion(test, autoReSizeColumnsPage.isAutoResizeColumnsDone(gridActionsPage.getMessageFromToaster()),
					"Auto Resize Columns Successfully", "Auto Resize Columns not Updated");
	}
}
