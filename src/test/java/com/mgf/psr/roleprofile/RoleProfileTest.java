package com.mgf.psr.roleprofile;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.mgf.psr.constants.RoleProfileConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.RoleProfilePage;
import com.mgf.psr.pages.TopMenuPage;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class RoleProfileTest extends BaseRoleProfileTest {
	private LoginPage loginPage = new LoginPage(baseUrl);
	private TopMenuPage topMenuPage;
	private RoleProfilePage roleProfile;

	private BaseAssertions baseAssertions = new BaseAssertions();
	
	@Test(groups = { TestGroupConstant.ROLEPROFILE, TestGroupConstant.REGRESSION })
	public void testRoleProfileCreation(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application and navigate to Role profile");
		loginToApplicationAndNavigateToRoleProfile(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"), loginPage, topMenuPage, roleProfile);
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Creation of Role profile");
		String roleName = MainUtilities.generateRandomString(8) + MainUtilities.generateRandomDigits(4);
		String roleDesc = MainUtilities.generateRandomString(8) + MainUtilities.generateRandomDigits(4);
		String roleApp = RoleProfileConstant.ROLE_APP_NAME;
		iTestContext.setAttribute("roleName", roleName);
		roleProfile.createRoleProfile(roleName, roleDesc, roleApp);
		
		MainUtilities.logInfoToReport(test, "Step 3 : Verify Role Profile is created or not");
		baseAssertions.booleanTrueAssertion(test, roleProfile.isRoleCreated(roleName),
				"Role is created Successfully", "Role is not created");
	}

	@Test(dependsOnMethods = "testRoleProfileCreation", groups = { TestGroupConstant.ROLEPROFILE, TestGroupConstant.REGRESSION })
	public void testRoleProfileUpdation(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application and navigate to Role profile");
		loginToApplicationAndNavigateToRoleProfile(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"), loginPage, topMenuPage, roleProfile);
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Updation of Role profile");
		String roleName = iTestContext.getAttribute("roleName").toString();
		roleProfile.updateRoleProfile(roleName);
		
		MainUtilities.logInfoToReport(test, "Step 3 : Verify Role Profile is updated or not");
		log.info("before verifing updation");
		baseAssertions.booleanTrueAssertion(test, roleProfile.isRoleUpdated(roleName),
				"Role is updated Successfully", "Role is not updated");
	}
	
	@Test( dependsOnMethods = "testRoleProfileCreation", groups = { TestGroupConstant.USERPROFILE, TestGroupConstant.REGRESSION })
	public void testRoleProfileDeletion(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application and navigate to User profile");
		loginToApplicationAndNavigateToRoleProfile(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"), loginPage, topMenuPage, roleProfile);
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Deletion of Role profile");
		String roleName = iTestContext.getAttribute("roleName").toString();
		roleProfile.deleteRoleProfile(roleName);
		
		MainUtilities.logInfoToReport(test, "Step 3 : Verify Role profile is deleted or not");
		baseAssertions.booleanTrueAssertion(test, roleProfile.isRoleDeleted(roleName),
				"Role is Deleted Successfully", "Role is not deleted");
	}
}
