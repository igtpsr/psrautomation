package com.mgf.psr.roleprofile;

import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.RoleProfilePage;
import com.mgf.psr.pages.TopMenuPage;

public class BaseRoleProfileTest extends BaseLoginTest {
	/**
	 * This method is used to login to application and navigate to Role profile
	 * @param userName
	 * @param password
	 * @param loginPage
	 * @param topMenuPage
	 * @param userProfile
	 */
	protected void loginToApplicationAndNavigateToRoleProfile(String userName, String password , LoginPage loginPage, TopMenuPage topMenuPage, RoleProfilePage roleProfile) {
		loginPage.openApplication();
		loginPage.loginToApplication(userName,password);
		roleProfile.clickOnDashboardLink();
		roleProfile.clickOnRoleProfileLink();
	}
}
