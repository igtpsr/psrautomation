package com.mgf.psr.showHideColumn;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.mgf.psr.constants.GridActionsConstant;
import com.mgf.psr.constants.ShowHideColumnViewConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.elementIdentifier.GridActionsIdentifier;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.GridActionsPage;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.SearchFieldsPage;
import com.mgf.psr.pages.ShowHideColumnViewPage;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class ShowHideColumnTest extends BaseLoginTest {

	private LoginPage loginPage = new LoginPage(baseUrl);
	private SearchFieldsPage searchFieldPage;
	private ShowHideColumnViewPage showHideColumnViewPage;	
	private GridActionsPage gridActionsPage;
	
	private BaseAssertions baseAssertions = new BaseAssertions();
	
	@Test(groups = { TestGroupConstant.SHOW_HIDE_COLUMN_VIEW, TestGroupConstant.REGRESSION })
	public void testShowHideColumnSaveView(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 :  Save new View");
		String gridViewName = MainUtilities.generateRandomString(10) + MainUtilities.generateRandomDigits(2);
		iTestContext.setAttribute("gridViewName", gridViewName);
		showHideColumnViewPage.saveNewView(gridViewName, ShowHideColumnViewConstant.ORDER_QTY);
		
		MainUtilities.logInfoToReport(test, "Step 5 : Verify whether the Grid View is saved or not.");
		baseAssertions.booleanTrueAssertion(test, showHideColumnViewPage.isGridViewSaved(gridActionsPage.getMessageFromToaster()),
					"View saved Successfully", "View not saved");
	}
	
	@Test(dependsOnMethods = "testShowHideColumnSaveView", groups = { TestGroupConstant.SHOW_HIDE_COLUMN_VIEW, TestGroupConstant.REGRESSION })
	public void testShowHideColumnUpdateView(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 :  Select the require grid view");
		String gridViewName = iTestContext.getAttribute("gridViewName").toString();
		showHideColumnViewPage.selectGridView(gridViewName);
		MainUtilities.waitFor(4000);
	
		MainUtilities.logInfoToReport(test, "Step 5 :  Update the existing View");
		showHideColumnViewPage.updateView(ShowHideColumnViewConstant.ORDER_QTY,gridViewName);
		
		MainUtilities.logInfoToReport(test, "Step 6 : Verify whether the Grid View is updated or not.");
		baseAssertions.booleanTrueAssertion(test, showHideColumnViewPage.isGirdViewUpdated(gridActionsPage.getMessageFromToaster()),
					"View Update Successfully", "View not Updated");
	}
	
	@Test(dependsOnMethods = "testShowHideColumnSaveView", groups = { TestGroupConstant.SHOW_HIDE_COLUMN_VIEW, TestGroupConstant.REGRESSION })
	public void testShowHideColumnDeleteView(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 :  Select the required grid view");
		String gridViewName = iTestContext.getAttribute("gridViewName").toString();
		MainUtilities.waitFor(4000);
		showHideColumnViewPage.selectGridView(gridViewName);
		MainUtilities.waitFor(4000);
		
		MainUtilities.logInfoToReport(test, "Step 5 :  Delete View");
		showHideColumnViewPage.deleteGridView();
		
		MainUtilities.logInfoToReport(test, "Step 6 : Verify whether the Grid View is deleted or not.");
		baseAssertions.booleanTrueAssertion(test, !showHideColumnViewPage.isGridViewDeleted(gridViewName),
					"View deleted Successfully", "View not deleted");
	}
}
