package com.mgf.psr.userprofile;

import org.testng.ITestContext;
import org.testng.annotations.Test;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.constants.UserProfileConstant;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.TopMenuPage;
import com.mgf.psr.pages.UserProfilePage;
import com.mgf.psr.utilities.Utilities;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class UserProfileTest extends BaseUserProfileTest {

	private LoginPage loginPage = new LoginPage(baseUrl);
	private TopMenuPage topMenuPage;
	private UserProfilePage userProfile;

	private BaseAssertions baseAssertions = new BaseAssertions();

	@Test(groups = { TestGroupConstant.USERPROFILE, TestGroupConstant.REGRESSION })
	public void testUserProfileCreation(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application and navigate to User profile");
		loginToApplicationAndNavigateToUserProfile(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"), loginPage, topMenuPage, userProfile);
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Creation of User profile");
		String userID = MainUtilities.generateRandomString(8) + MainUtilities.generateRandomDigits(4);
		String userName = MainUtilities.generateRandomString(8) + MainUtilities.generateRandomDigits(4);
		String password = Utilities.generateRandomPassword(8);
		String role = UserProfileConstant.ROLE;
		String country = UserProfileConstant.COUNTRY;
		String queryName = UserProfileConstant.QUERY_NAME;
		String brUserID = MainUtilities.generateRandomString(8) + MainUtilities.generateRandomDigits(4);
		iTestContext.setAttribute("userID", userID);
		userProfile.createUserProfile(userID,userName,password,brUserID,role,country,queryName);

		MainUtilities.logInfoToReport(test, "Step 3 : Verify User Profile is created or not");
		baseAssertions.booleanTrueAssertion(test, userProfile.isUserCreated(userID),
				"User is created Successfully", "User is not created");

	}

	@Test(dependsOnMethods = "testUserProfileCreation", groups = { TestGroupConstant.USERPROFILE, TestGroupConstant.REGRESSION })
	public void testUserProfileUpdation(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application and navigate to User profile");
		loginToApplicationAndNavigateToUserProfile(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"), loginPage, topMenuPage, userProfile);
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Updating User profile");
		String userID = iTestContext.getAttribute("userID").toString();
		userProfile.updateUserProfile(userID);
		
		MainUtilities.logInfoToReport(test, "Step 3 : Verify User model is updated or not");
		baseAssertions.booleanTrueAssertion(test, userProfile.isUserUpdated(userID),
				"User is updated Successfully", "User is not updated");
	}

	@Test( dependsOnMethods = "testUserProfileCreation", groups = { TestGroupConstant.USERPROFILE, TestGroupConstant.REGRESSION })
	public void testUserProfileDeletion(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application and navigate to User profile");
		loginToApplicationAndNavigateToUserProfile(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"), loginPage, topMenuPage, userProfile);
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Deletion of User profile");
		String userID = iTestContext.getAttribute("userID").toString();
		userProfile.deleteUserProfile(userID);
		
		MainUtilities.logInfoToReport(test, "Step 3 : Verify User model is deleted or not");
		baseAssertions.booleanTrueAssertion(test, userProfile.isUserDeleted(userID),
				"User is Deleted Successfully", "User is not deleted");
	}
}
