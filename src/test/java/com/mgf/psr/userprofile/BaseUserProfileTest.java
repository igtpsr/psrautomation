package com.mgf.psr.userprofile;

import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.TopMenuPage;
import com.mgf.psr.pages.UserProfilePage;

public class BaseUserProfileTest extends BaseLoginTest {
	
	/**
	 * This method is used to login to application and navigate to User Profile
	 * @param userName
	 * @param password
	 * @param loginPage
	 * @param topMenuPage
	 * @param userProfile
	 */
	protected void loginToApplicationAndNavigateToUserProfile(String userName, String password , LoginPage loginPage, TopMenuPage topMenuPage, UserProfilePage userProfile) {
		loginPage.openApplication();
		loginPage.loginToApplication(userName,password);
		userProfile.clickOnDashboardLink();
		userProfile.clickOnUserProfileLink();
	}
}
