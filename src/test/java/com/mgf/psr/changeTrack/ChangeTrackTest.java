package com.mgf.psr.changeTrack;

import org.testng.annotations.Test;

import com.mgf.psr.constants.GridActionsConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.ChangeTrackPage;
import com.mgf.psr.pages.GridActionsPage;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.SearchFieldsPage;

import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class ChangeTrackTest extends BaseLoginTest{
	
	private LoginPage loginPage = new LoginPage(baseUrl);
	private ChangeTrackPage changeTrackPage;
	private SearchFieldsPage searchFieldPage;
	private GridActionsPage gridActionsPage;
	
	private BaseAssertions baseAssertions = new BaseAssertions();
	
	@Test(groups = { TestGroupConstant.CHANGE_TRACK, TestGroupConstant.REGRESSION })
	public void testChangeTrack() {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		MainUtilities.waitFor(10000);
		
		MainUtilities.logInfoToReport(test, "Step 4 : Select column to edit");
		String columnName = GridActionsConstant.NDC_WK;
		searchFieldPage.selectColumn(columnName);
		MainUtilities.waitFor(5000);
		
		MainUtilities.logInfoToReport(test, "Step 4 :  Enter Ndc Wk value");
		String updatedColumnValue = MainUtilities.generateRandomString(7);
		gridActionsPage.enterNdcWkNewValue(updatedColumnValue);
		
		MainUtilities.logInfoToReport(test, "Step 5 :  Click on save grid changed button");
		MainUtilities.waitFor(5000);
		gridActionsPage.clickOnSaveGridChangesButton();
		
		MainUtilities.logInfoToReport(test, "Step 6 :  Select row in the grid");
		MainUtilities.waitFor(20000);
		gridActionsPage.clickOnSelectCheckBox();
		MainUtilities.waitFor(4000);
		
		MainUtilities.logInfoToReport(test, "Step 7 :  Check Change Track");
		changeTrackPage.navigateChangeTrack();	
				
		MainUtilities.logInfoToReport(test, "Step 8 : Verify whether the Change Track is updated or not.");
		baseAssertions.booleanTrueAssertion(test, changeTrackPage.isChangeTrackUpdated(columnName,updatedColumnValue),
					"Change Track updated Successfully", "Change Track not Updated");
	}
}
