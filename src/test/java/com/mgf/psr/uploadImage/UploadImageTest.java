package com.mgf.psr.uploadImage;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.testng.annotations.Test;

import com.mgf.psr.constants.GridActionsConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.elementIdentifier.GridActionsIdentifier;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.GridActionsPage;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.SearchFieldsPage;
import com.mgf.psr.pages.UploadImagePage;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class UploadImageTest extends BaseLoginTest{
	
	private LoginPage loginPage = new LoginPage(baseUrl);
	private UploadImagePage uploadImagePage;
	private SearchFieldsPage searchFieldPage;
	private GridActionsPage gridActionsPage;
	
	private BaseAssertions baseAssertions = new BaseAssertions();
	
	@Test(groups = { TestGroupConstant.UPLOAD_IMAGE, TestGroupConstant.REGRESSION })
	public void testUploadImage() throws IOException, FindFailed, AWTException {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 :  Do Upload Image");
		uploadImagePage.uploadImage();
		
		MainUtilities.logInfoToReport(test, "Step 5 : Verify whether the image is uploaded or not.");
		baseAssertions.booleanTrueAssertion(test, uploadImagePage.isImageUploaded(gridActionsPage.getMessageFromToaster()),
					"Image uploaded Successfully", "Image not Uploaded");
	}
}
