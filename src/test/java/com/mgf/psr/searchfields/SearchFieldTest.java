package com.mgf.psr.searchfields;

import java.util.Map;

import org.testng.annotations.Test;

import com.mgf.psr.constants.GridColumnNameConstant;
import com.mgf.psr.constants.OperatorConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.SearchFieldsPage;
import com.mgf.psr.pages.TopMenuPage;
import com.mgf.psr.reporter.BaseTest;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.dataprovider.ExcelDataProviderArgs;
import com.retelzy.utilities.MainUtilities;

public class SearchFieldTest extends BaseLoginTest {

	private LoginPage loginPage = new LoginPage(baseUrl);
	private TopMenuPage topMenuPage;
	private SearchFieldsPage searchFieldsPage;
	private BaseAssertions baseAssertions = new BaseAssertions();

	@Test(groups = { TestGroupConstant.SEARCH_FIELDS,
			TestGroupConstant.REGRESSION }, dataProvider = "Excel", dataProviderClass = BaseTest.class)
	@ExcelDataProviderArgs(excelFile = "testdata/SearchFields.xlsx", worksheet = "Sheet1")
	public void testVPOSearchField(Map<Object, Object> input) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application and login with valid credential");
		loginToApplication(loginPage, ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 2 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		final String fieldName = String.valueOf(input.get("Field Name"));
		final String operator = String.valueOf(input.get("Operator"));
		final String value = String.valueOf(input.get("Value"));

		MainUtilities.logInfoToReport(test, "Step 3 : Search VPO#");
		searchFieldsPage.selectVpoOpeartor(operator);
		searchFieldsPage.enterVPO(value);
		searchFieldsPage.clickOnSearchButton();

		MainUtilities.logInfoToReport(test, "Step 4 : Verify searched VPO in PSR grid");
		if (operator.equals(OperatorConstant.NOT_EQUAL_TO) || operator.equals(OperatorConstant.NOT_LIKE)
				|| operator.equals(OperatorConstant.NOT_IN_THE_LIST)) {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isVpoNotSearched(value),
					"VPO# " + value + " is not searched using " + operator,
					"VPO# " + value + " is searched using " + operator);
		} else {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isVpoSearched(value),
					"VPO# " + value + " searched using " + operator,
					"VPO# " + value + " is not searched using " + operator);
		}
	}
	
	@Test(groups = { TestGroupConstant.SEARCH_FIELDS,
			TestGroupConstant.REGRESSION }, dataProvider = "Excel", dataProviderClass = BaseTest.class)
	@ExcelDataProviderArgs(excelFile = "testdata/SearchFields.xlsx", worksheet = "Sheet2")
	public void testStyleSearchField(Map<Object, Object> input) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application and login with valid credential");
		loginToApplication(loginPage, ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 2 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		final String fieldName = String.valueOf(input.get("Field Name"));
		final String operator = String.valueOf(input.get("Operator"));
		final String value = String.valueOf(input.get("Value"));

		MainUtilities.logInfoToReport(test, "Step 3 : Search Style#");
		searchFieldsPage.selectStyleOpeartor(operator);
		searchFieldsPage.enterStyle(value);
		searchFieldsPage.clickOnSearchButton();

		MainUtilities.logInfoToReport(test, "Step 4 : Verify searched Style# in PSR grid");
		if (operator.equals(OperatorConstant.NOT_EQUAL_TO) || operator.equals(OperatorConstant.NOT_LIKE)
				|| operator.equals(OperatorConstant.NOT_IN_THE_LIST)) {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isVpoNotSearched(value),
					"Style# " + value + " is not searched using " + operator,
					"Style# " + value + " is searched using " + operator);
		} else {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isVpoSearched(value),
					"Style# " + value + " searched using " + operator,
					"Style# " + value + " is not searched using " + operator);
		}
	}
	
	@Test(groups = { TestGroupConstant.SEARCH_FIELDS,
			TestGroupConstant.REGRESSION }, dataProvider = "Excel", dataProviderClass = BaseTest.class)
	@ExcelDataProviderArgs(excelFile = "testdata/SearchFields.xlsx", worksheet = "Sheet3")
	public void testOrderCommitSearchField(Map<Object, Object> input) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application and login with valid credential");
		loginToApplication(loginPage, ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 2 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		final String fieldName = String.valueOf(input.get("Field Name"));
		final String operator = String.valueOf(input.get("Operator"));
		final String value = String.valueOf(input.get("Value"));

		MainUtilities.logInfoToReport(test, "Step 3 : Search Order Commit #");
		searchFieldsPage.selectOrderCommitOpeartor(operator);
		searchFieldsPage.enterOrderCommit(value);
		searchFieldsPage.clickOnSearchButton();

		MainUtilities.logInfoToReport(test, "Step 4 : Verify searched Order Commit # in PSR grid");
		if (operator.equals(OperatorConstant.NOT_EQUAL_TO) || operator.equals(OperatorConstant.NOT_LIKE)
				|| operator.equals(OperatorConstant.NOT_IN_THE_LIST)) {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isOrderCommitNotSearched(value),
					"Order Commit # " + value + " is not searched using " + operator,
					"Order Commit # " + value + " is searched using " + operator);
		} else {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isOrderCommitSearched(value),
					"Order Commit # " + value + " searched using " + operator,
					"Order Commit # " + value + " is not searched using " + operator);
		}
	}
	
	@Test(groups = { TestGroupConstant.SEARCH_FIELDS,
			TestGroupConstant.REGRESSION }, dataProvider = "Excel", dataProviderClass = BaseTest.class)
	@ExcelDataProviderArgs(excelFile = "testdata/SearchFields.xlsx", worksheet = "Sheet4")
	public void testProductionOfficeSearchField(Map<Object, Object> input) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application and login with valid credential");
		loginToApplication(loginPage, ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 2 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		final String fieldName = String.valueOf(input.get("Field Name"));
		final String operator = String.valueOf(input.get("Operator"));
		final String value = String.valueOf(input.get("Value"));

		MainUtilities.logInfoToReport(test, "Step 3 : Search Production Office");
		searchFieldsPage.selectProductionOfficeOpeartor(operator);
		searchFieldsPage.enterProductionOffice(value);
		searchFieldsPage.clickOnSearchButton();
		searchFieldsPage.selectColumn(GridColumnNameConstant.PRODUCTION_OFFICE);

		MainUtilities.logInfoToReport(test, "Step 4 : Verify searched Production Office in PSR grid");
		if (operator.equals(OperatorConstant.NOT_EQUAL_TO) || operator.equals(OperatorConstant.NOT_LIKE)
				|| operator.equals(OperatorConstant.NOT_IN_THE_LIST)) {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isOrderCommitNotSearched(value),
					"Production Office " + value + " is not searched using " + operator,
					"Production Office " + value + " is searched using " + operator);
		} else {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isOrderCommitSearched(value),
					"Production Office " + value + " searched using " + operator,
					"Production Office " + value + " is not searched using " + operator);
		}
	}
	
	@Test(groups = { TestGroupConstant.SEARCH_FIELDS,
			TestGroupConstant.REGRESSION }, dataProvider = "Excel", dataProviderClass = BaseTest.class)
	@ExcelDataProviderArgs(excelFile = "testdata/SearchFields.xlsx", worksheet = "Sheet5")
	public void testBrandSearchField(Map<Object, Object> input) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application and login with valid credential");
		loginToApplication(loginPage, ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 2 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		final String fieldName = String.valueOf(input.get("Field Name"));
		final String operator = String.valueOf(input.get("Operator"));
		final String value = String.valueOf(input.get("Value"));

		MainUtilities.logInfoToReport(test, "Step 3 : Search Brand");
		searchFieldsPage.selectBrandOpeartor(operator);
		searchFieldsPage.enterBrand(value);
		searchFieldsPage.clickOnSearchButton();

		MainUtilities.logInfoToReport(test, "Step 4 : Verify searched Brand in PSR grid");
		if (operator.equals(OperatorConstant.NOT_EQUAL_TO) || operator.equals(OperatorConstant.NOT_LIKE)
				|| operator.equals(OperatorConstant.NOT_IN_THE_LIST)) {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isBrandNotSearched(value),
					"Brand " + value + " is not searched using " + operator,
					"Brand " + value + " is searched using " + operator);
		} else {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isBrandSearched(value),
					"Brand " + value + " searched using " + operator,
					"Brand " + value + " is not searched using " + operator);
		}
	}
	
	@Test(groups = { TestGroupConstant.SEARCH_FIELDS,
			TestGroupConstant.REGRESSION }, dataProvider = "Excel", dataProviderClass = BaseTest.class)
	@ExcelDataProviderArgs(excelFile = "testdata/SearchFields.xlsx", worksheet = "Sheet6")
	public void testCategorySearchField(Map<Object, Object> input) throws InterruptedException {
		MainUtilities.logInfoToReport(test, "Step 1 : Open PSR application and login with valid credential");
		loginToApplication(loginPage, ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));

		MainUtilities.logInfoToReport(test, "Step 2 : Verify user login");
		baseAssertions.booleanTrueAssertion(test, topMenuPage.isLogoutLinkDisplayed(),
				"User is able to login to application", "User is not able to login to application");

		final String fieldName = String.valueOf(input.get("Field Name"));
		final String operator = String.valueOf(input.get("Operator"));
		final String value = String.valueOf(input.get("Value"));

		MainUtilities.logInfoToReport(test, "Step 3 : Search Category");
		searchFieldsPage.selectCategoryOpeartor(operator);
		searchFieldsPage.enterCategory(value);
		searchFieldsPage.clickOnSearchButton();

		MainUtilities.logInfoToReport(test, "Step 4 : Verify searched Category in PSR grid");
		if (operator.equals(OperatorConstant.NOT_EQUAL_TO) || operator.equals(OperatorConstant.NOT_LIKE)
				|| operator.equals(OperatorConstant.NOT_IN_THE_LIST)) {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isCategoryNotSearched(value),
					"Category " + value + " is not searched using " + operator,
					"Category " + value + " is searched using " + operator);
		} else {
			baseAssertions.booleanTrueAssertion(test, searchFieldsPage.isCategorySearched(value),
					"Category " + value + " searched using " + operator,
					"Category " + value + " is not searched using " + operator);
		}
	}
}
