package com.mgf.psr.gridActions;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.mgf.psr.constants.GridActionsConstant;
import com.mgf.psr.constants.TestGroupConstant;
import com.mgf.psr.login.BaseLoginTest;
import com.mgf.psr.pages.GridActionsPage;
import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.pages.SearchFieldsPage;
import com.mgf.psr.pages.TopMenuPage;
import com.retelzy.assertions.BaseAssertions;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.utilities.MainUtilities;

public class GridActionsTest extends BaseLoginTest {
	private LoginPage loginPage = new LoginPage(baseUrl);
	private GridActionsPage gridActionsPage;
	private SearchFieldsPage searchFieldPage;
	
	private BaseAssertions baseAssertions = new BaseAssertions();
	
	@Test(groups = { TestGroupConstant.EXPORT_EXCEL, TestGroupConstant.REGRESSION })
	public void testExportExcel(ITestContext iTestContext) throws IOException, FindFailed {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 :  Export to Excel");
		gridActionsPage.exportExcel();
		
		MainUtilities.logInfoToReport(test, "Step 5 : Verify Export excel is correct or not.");
		baseAssertions.booleanTrueAssertion(test, gridActionsPage.isExcelExported(),
					"Excel exported Successfully", "Excel not exported");
	}
	
	@Test(groups = { TestGroupConstant.IMPORT_EXCEL, TestGroupConstant.REGRESSION })
	public void testImportExcel(ITestContext iTestContext) throws IOException, AWTException, FindFailed {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 :  Import from Excel");
		gridActionsPage.importExcel();
				
		MainUtilities.logInfoToReport(test, "Step 5 : Verify Imported excel is correct or not.");
		baseAssertions.booleanTrueAssertion(test, gridActionsPage.isExcelImported(),
				"Excel imported Successfully", "Excel not imported");
	}
	
	@Test(groups = { TestGroupConstant.FILE_TRACK, TestGroupConstant.REGRESSION })
	public void testFileTrack(ITestContext iTestContext) {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Checking File track");
		gridActionsPage.checkFileTrack();
				
		MainUtilities.logInfoToReport(test, "Step 3 : Verify File track is correct or not.");
		baseAssertions.booleanTrueAssertion(test, gridActionsPage.isFileTrackCorrect(),
				"File Track is Correct", "File track is not correct");	
	}
	
	@Test(groups = { TestGroupConstant.ACKNOWLEDGEMENT, TestGroupConstant.REGRESSION })
	public void testAcknowledgement(ITestContext iTestContext) throws IOException {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 :  click on Acknowledgement");
		gridActionsPage.acknowledgeRow();
		
		MainUtilities.logInfoToReport(test, "Step 5 : Verify whether the row is acknowledged or not");
		baseAssertions.booleanTrueAssertion(test, gridActionsPage.isRowAcknowledged(),
				"Row acknowledged Successfully", "Row not acknowledged");
	}
	
	@Test(groups = { TestGroupConstant.SAVE_GRID_CHANGES, TestGroupConstant.REGRESSION })
	public void testSaveGridChanges(ITestContext iTestContext) throws IOException {
		MainUtilities.logInfoToReport(test, "Step 1 :  Login to application");
		loginPage.openApplication();loginPage.loginToApplication(ParameterHelper.getParameterDefaultValue("userName"),
				ParameterHelper.getParameterDefaultValue("password"));
		
		MainUtilities.logInfoToReport(test, "Step 2 :  Enter VPO Number");
		searchFieldPage.enterVPO(GridActionsConstant.VPO_NO_FOR_EXCEL_OPERATION);
		
		MainUtilities.logInfoToReport(test, "Step 3 :  Click on search Button");
		searchFieldPage.clickOnSearchButton();
		
		MainUtilities.logInfoToReport(test, "Step 4 :  Save Grid Changes");
		gridActionsPage.saveGridChanges();
		
		MainUtilities.logInfoToReport(test, "Step 5 : Verify whether the grid is saved or not.");
		baseAssertions.booleanTrueAssertion(test, gridActionsPage.isGridSaved(),
				"Saved Grid Changes Successfully", "Grid not saved");
	}
}
