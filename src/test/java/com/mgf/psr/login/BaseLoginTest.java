package com.mgf.psr.login;

import com.mgf.psr.pages.LoginPage;
import com.mgf.psr.reporter.BaseTest;
import com.retelzy.assertions.BaseAssertions;

public class BaseLoginTest extends BaseTest{

	private BaseAssertions baseAssertions = new BaseAssertions();

	/**
	 * This method used to verify login page.
	 */
	protected void verifyLoginPage(final LoginPage loginPage) {
		baseAssertions.booleanTrueAssertion(test, loginPage.isLoginPageDisplayed(), "Login Page is dispayed",
				"Login Page is not dispayed");
	}

	/**
	 * This method used to login to application with given username and password.
	 * 
	 * @param loginPage
	 *            loginPage
	 * @param userName
	 *            userName
	 * @param password
	 *            password
	 */
	protected void loginToApplication(final LoginPage loginPage, final String userName, final String password) {
		loginPage.openApplication();
		verifyLoginPage(loginPage);
		loginPage.loginToApplication(userName, password);
	}
}
